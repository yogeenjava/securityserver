package com.eos.component;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.eos.repository.IPAddressRepository;

@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {

	private final Logger log = LoggerFactory.getLogger(RequestInterceptor.class);

	@Autowired
	private IPAddressRepository ipAddressRepository;

	@Autowired
	private ServletContext servletContext;

	/**
	 * This is not a good practice to use sysout. Always integrate any logger
	 * with your application. We will discuss about integrating logger with
	 * spring boot application in some later article
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		String requestURI = request.getRequestURI();

		if (requestURI.contains("api/admin") || requestURI.contains("sound/isValidSound")) {
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}

			if (servletContext.getAttribute("iplist") == null) {
				List<String> ipList = this.ipAddressRepository.getAll();
				servletContext.setAttribute("iplist", ipList);
			}

			if (servletContext.getAttribute("iplist") != null) {

				List<String> ipList = (List<String>) servletContext.getAttribute("iplist");

				if (ipList.contains(ipAddress)) {
					return true;
				} else {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST);
					return false;
				}
			} else {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return false;
			}

		}
		return true;
	}

}
