package com.eos.component;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eos.repository.IPAddressRepository;

@Component
public class PostConstruct {

	@Autowired
	private IPAddressRepository ipAddressRepository;

	@Autowired
	private ServletContext servletContext;

	@javax.annotation.PostConstruct
	public void init() {
		if (servletContext.getAttribute("iplist") == null) {
			List<String> ipList = this.ipAddressRepository.getAll();
			servletContext.setAttribute("iplist", ipList);
		}
	}
}
