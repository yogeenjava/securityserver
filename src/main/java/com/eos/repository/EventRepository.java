package com.eos.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.eos.domain.Event;
import com.eos.domain.SoundCode;

/**
 * Spring Data JPA repository for the Event entity.
 */
@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
	@Query("select distinct event from Event event left join fetch event.eventFloors")
	List<Event> findAllWithEagerRelationships();

	@Query("select event from Event event left join fetch event.eventFloors where event.id =:id")
	Event findOneWithEagerRelationships(@Param("id") Long id);

	Event findOneByIdAndIsDeleted(Long id, Boolean isDeleted);

	Page<Event> findAllByIsDeletedAndToDateAfterOrderByFromDateAsc(Pageable pageable, Boolean isDeleted,
			ZonedDateTime dateTime);

	Event findOneByIdAndIsDeletedAndEventGuestListEmail(Long id, Boolean isDeleted, String email);

	List<Event> findAllByIsDeletedFalseAndActivatedTrueAndToDateBefore(ZonedDateTime currentDateTime);

	Page<Event> findAllByIsDeletedFalse(Pageable pageable);

	Event findOneBySoundCodeAndEventFloorsFloorNo(SoundCode sc, String floorNo);

	Event findOneBySoundCode(SoundCode sc);

}
