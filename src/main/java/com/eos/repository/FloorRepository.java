package com.eos.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eos.domain.Floor;

/**
 * Spring Data JPA repository for the Floor entity.
 */
@Repository
public interface FloorRepository extends JpaRepository<Floor, Long> {

	public Collection<Floor> findAllByIsDeleted(Boolean isDeleted);

	public Floor findOneByIdAndIsDeleted(Long id, Boolean isDeleted);

	public Floor findOneByFloorNo(String floorNo);

}
