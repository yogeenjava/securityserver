package com.eos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.eos.domain.IPAddress;

/**
 * Spring Data JPA repository for the IPAddress entity.
 */
@Repository
public interface IPAddressRepository extends JpaRepository<IPAddress, Long> {

	@Query("Select ipAddress from IPAddress")
	public List<String> getAll();

}
