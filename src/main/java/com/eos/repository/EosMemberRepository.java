package com.eos.repository;

import com.eos.domain.EosMember;
import com.eos.domain.SoundCode;
import com.eos.domain.User;

import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data JPA repository for the EosMember entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EosMemberRepository extends JpaRepository<EosMember, Long> {
	@Query("select distinct eos_member from EosMember eos_member left join fetch eos_member.floors")
	List<EosMember> findAllWithEagerRelationships();

	@Query("select eos_member from EosMember eos_member, User user left join fetch eos_member.floors where eos_member.id =:id and user.id =:id and user.activated = true")
	EosMember findOneWithEagerRelationships(@Param("id") Long id);

	EosMember findOneById(Long id);

	Page<EosMember> findAllByUserLoginNot(Pageable pageable, String login);
	
	List<EosMember> findAllByUserActivatedTrueAndToDateNotNullAndToDateBefore(ZonedDateTime currentDateTime);

	EosMember findOneBySoundCodeAndFloorsFloorNo(SoundCode sc, String floorNo);

	EosMember findOneBySoundCode(SoundCode sc);

//	List<EosMember> findAllByUserActivatedTrueAndToDateAndToDateBefore(ZonedDateTime currentDateTime);

}
