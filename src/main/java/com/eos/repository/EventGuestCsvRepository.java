package com.eos.repository;

import com.eos.domain.EventGuestCsv;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the EventGuestCsv entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EventGuestCsvRepository extends JpaRepository<EventGuestCsv, Long> {

}
