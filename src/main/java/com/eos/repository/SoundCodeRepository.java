package com.eos.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eos.domain.SoundCode;

@Repository
public interface SoundCodeRepository extends JpaRepository<SoundCode, Serializable>{

	public SoundCode findOneByLicenseNoAndUid(String licenceKey, String uid);

}
