package com.eos.repository;

import com.eos.domain.EventGuest;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the EventGuest entity.
 */
@Repository
public interface EventGuestRepository extends JpaRepository<EventGuest, Long> {

	public EventGuest findOneByEmail(String email);

	public List<EventGuest> findAllByEventId(Long id);

}
