package com.eos.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AppUtils {
	
	private final Logger log = LoggerFactory.getLogger(AppUtils.class);

	public String getLicenseno(){
		String licenseNo = null;
		try{
			Integer index = new Random().nextInt(9);
			List<String> list = new ArrayList<>();
			list.add("0xFD615D61,0xB0854BAB,0x88E33764,0xA3B7C57A");
			list.add("0x6C14A6C4,0xCF7A426A,0x8C7E7DBA,0xE4CFE0E8");
			list.add("0xCB7FC5E3,0xFEAA4507,0x91F373B5,0x902DA2EB");
			list.add("0xC6B12B18,0x7C954EEF,0xB3C991F4,0xA68251A3");
			list.add("0x007178AE,0xB6FE4EE7,0x9F335B50,0x074DB0E3");
			list.add("0x7832603D,0x8A38490F,0xA2D7892C,0x5197D23E");
			list.add("0x96097E62,0xB23C43F8,0x9BB5CBF3,0x5CFA72A2");
			list.add("0x5F3DB222,0x8BB44FEF,0xB2E50049,0xFABADC32");
			list.add("0x9A1BFF80,0xA9B34F7D,0xBB0F77A2,0x8963E572");
			list.add("0x47E5932F,0xB55042C5,0xBAAF39C8,0x75A030A2");
			licenseNo = list.get(index);
		}catch(Exception e){
			log.error(e.getMessage());
		}
		return licenseNo;
	}
	
	public String getSoundCodeUid(){
		String uid = null;
		try{
			Integer intUid = new Random().nextInt(65535);
			uid = intUid.toString();
		}catch(Exception e){
			log.error(e.getMessage());
		}
		return uid;
	}
}
