package com.eos.utils;

import java.time.LocalTime;
import java.time.ZonedDateTime;

import org.springframework.stereotype.Service;

@Service
public class DateTimeUtil {

	public boolean isTimeBetween(LocalTime fromTime, LocalTime toTime) {

		LocalTime currentTime = ZonedDateTime.now().toLocalTime();

		if (fromTime.isBefore(toTime)) {
			if (currentTime.isBefore(toTime) && currentTime.isAfter(fromTime)) {
				return true;
			}
			return false;
		} else {
			if (currentTime.isBefore(fromTime) && currentTime.isAfter(toTime)) {
				return false;
			}
			return true;
		}

	}

}
