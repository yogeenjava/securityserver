package com.eos.utils;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonView;

public class ApiResponse<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private T data;

	private Boolean status;

	private String message;

	private int statusCode;

	public ApiResponse() {
		super();
	}

	public ApiResponse(Boolean status, String message) {
		this(null, status, message);
	}

	public ApiResponse(T data, Boolean status, String message) {
		super();
		this.data = data;
		this.status = status;
		this.message = message;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
