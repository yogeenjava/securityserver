package com.eos.config;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Eos.
 * <p>
 * Properties are configured in the application.yml file. See
 * {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

	@Value("${soundUploadPath}")
	private String soundUploadPath;

	@Value("${url}")
	private String url;

	@Value("${decoderUserName}")
	private String decoderUserName;

	@Value("${decoderPassword}")
	private String decoderPassword;

	@SuppressWarnings("unused")
	private String authenticationToken;

	public String getAuthenticationToken() {
		String credentials = decoderUserName +":"+ decoderPassword;
		byte[] encodedHeader = Base64.getEncoder().encode(credentials.getBytes());
		return new String(encodedHeader);
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public String getDecoderUserName() {
		return decoderUserName;
	}

	public void setDecoderUserName(String decoderUserName) {
		this.decoderUserName = decoderUserName;
	}

	public String getDecoderPassword() {
		return decoderPassword;
	}

	public void setDecoderPassword(String decoderPassword) {
		this.decoderPassword = decoderPassword;
	}

	public String getSoundUploadPath() {
		return soundUploadPath;
	}

	public void setSoundUploadPath(String soundUploadPath) {
		this.soundUploadPath = soundUploadPath;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
