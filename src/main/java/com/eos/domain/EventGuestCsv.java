package com.eos.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A EventGuestCsv.
 */
@Entity
@Table(name = "event_guest_csv")
public class EventGuestCsv implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "file_name", nullable = false)
	private String fileName;

	@NotNull
	@Lob
	@Column(name = "file_data", nullable = false)
	private String fileData;

	@JoinColumn(name = "id")
	@OneToOne(fetch = FetchType.EAGER)
	@MapsId
	private Event event;

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public EventGuestCsv fileName(String fileName) {
		this.fileName = fileName;
		return this;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileData() {
		return fileData;
	}

	public EventGuestCsv fileData(String fileData) {
		this.fileData = fileData;
		return this;
	}

	public void setFileData(String fileData) {
		this.fileData = fileData;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		EventGuestCsv eventGuestCsv = (EventGuestCsv) o;
		if (eventGuestCsv.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), eventGuestCsv.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "EventGuestCsv{" + "id=" + getId() + ", fileName='" + getFileName() + "'" + ", fileData='"
				+ getFileData() + "'" + "}";
	}
}
