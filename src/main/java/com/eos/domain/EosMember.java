package com.eos.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A EosMember.
 */
@Entity
@Table(name = "eos_member")
public class EosMember implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name = "from_date")
	private ZonedDateTime fromDate;

	@Column(name = "to_date")
	private ZonedDateTime toDate;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "eos_member_floor", joinColumns = @JoinColumn(name = "eos_members_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "floors_id", referencedColumnName = "id"))
	private Set<Floor> floors = new HashSet<>();

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "sound_code_id")
	private SoundCode soundCode;

	@JoinColumn(name = "id")
	@OneToOne(fetch = FetchType.EAGER)
	@MapsId
	private User user;

	@Column(name = "start_time")
	private ZonedDateTime startTime;

	@Column(name = "end_time")
	private ZonedDateTime endTime;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ZonedDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(ZonedDateTime endTime) {
		this.endTime = endTime;
	}

	public ZonedDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(ZonedDateTime startTime) {
		this.startTime = startTime;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getFromDate() {
		return fromDate;
	}

	public EosMember fromDate(ZonedDateTime fromDate) {
		this.fromDate = fromDate;
		return this;
	}

	public void setFromDate(ZonedDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public ZonedDateTime getToDate() {
		return toDate;
	}

	public EosMember toDate(ZonedDateTime toDate) {
		this.toDate = toDate;
		return this;
	}

	public void setToDate(ZonedDateTime toDate) {
		this.toDate = toDate;
	}

	public Set<Floor> getFloors() {
		return floors;
	}

	public EosMember floors(Set<Floor> floors) {
		this.floors = floors;
		return this;
	}

	public EosMember addFloor(Floor floor) {
		this.floors.add(floor);
		return this;
	}

	public EosMember removeFloor(Floor floor) {
		this.floors.remove(floor);
		return this;
	}

	public void setFloors(Set<Floor> floors) {
		this.floors = floors;
	}

	@JsonIgnore
	public SoundCode getSoundCode() {
		return soundCode;
	}

	public EosMember soundCode(SoundCode soundCode) {
		this.soundCode = soundCode;
		return this;
	}

	@JsonProperty
	public void setSoundCode(SoundCode soundCode) {
		this.soundCode = soundCode;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		EosMember eosMember = (EosMember) o;
		if (eosMember.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), eosMember.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "EosMember{" + "id=" + getId() + ", fromDate='" + getFromDate() + "'" + ", toDate='" + getToDate() + "'"
				+ "}";
	}
}
