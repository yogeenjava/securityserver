package com.eos.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SoundCode.
 */
@Entity
@Table(name = "sound_code")
public class SoundCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "license_no")
    private String licenseNo;

    @Column(name = "jhi_uid")
    private String uid;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public SoundCode licenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
        return this;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getUid() {
        return uid;
    }

    public SoundCode uid(String uid) {
        this.uid = uid;
        return this;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SoundCode soundCode = (SoundCode) o;
        if (soundCode.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), soundCode.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SoundCode{" +
            "id=" + getId() +
            ", licenseNo='" + getLicenseNo() + "'" +
            ", uid='" + getUid() + "'" +
            "}";
    }
}
