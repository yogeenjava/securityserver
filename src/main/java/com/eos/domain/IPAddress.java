package com.eos.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.web.context.annotation.ApplicationScope;

import java.io.Serializable;
import java.util.Objects;

/**
 * A IPAddress.
 */
@ApplicationScope
@Entity
@Table(name = "ipaddress")
public class IPAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ip_address", nullable = false)
    private String ipAddress;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public IPAddress ipAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IPAddress iPAddress = (IPAddress) o;
        if (iPAddress.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), iPAddress.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IPAddress{" +
            "id=" + getId() +
            ", ipAddress='" + getIpAddress() + "'" +
            "}";
    }
}
