package com.eos.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Objects;

/**
 * A Event.
 */
@Entity
@Table(name = "event")
public class Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Size(min = 5)
	@Column(name = "event_name", nullable = false)
	private String eventName;

	@Column(name = "description")
	private String description;

	@Column(name = "from_date", nullable = false)
	private ZonedDateTime fromDate;

	@Column(name = "to_date")
	private ZonedDateTime toDate;

	@Column(name = "web_url")
	private String webUrl;

	@Column(name = "is_deleted")
	private Boolean isDeleted = false;

	@ManyToMany
	@JoinTable(name = "event_event_floors", joinColumns = @JoinColumn(name = "events_id"), inverseJoinColumns = @JoinColumn(name = "event_floors_id"))
	private Set<Floor> eventFloors = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "event_id")
	private List<EventGuest> eventGuestList = new ArrayList<>();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "sound_code_id")
	private SoundCode soundCode;

	@Column(name = "activated")
	private Boolean activated = true;

	@Column(name = "file_name")
	private String fileName;

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public Event eventName(String eventName) {
		this.eventName = eventName;
		return this;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getDescription() {
		return description;
	}

	public Event description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getFromDate() {
		return fromDate;
	}

	public Event fromDate(ZonedDateTime fromDate) {
		this.fromDate = fromDate;
		return this;
	}

	public void setFromDate(ZonedDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public ZonedDateTime getToDate() {
		return toDate;
	}

	public Event toDate(ZonedDateTime toDate) {
		this.toDate = toDate;
		return this;
	}

	public void setToDate(ZonedDateTime toDate) {
		this.toDate = toDate;
	}

	public Set<Floor> getEventFloors() {
		return eventFloors;
	}

	public Event eventFloors(Set<Floor> floors) {
		this.eventFloors = floors;
		return this;
	}

	public Event addEventFloors(Floor floor) {
		this.eventFloors.add(floor);
		return this;
	}

	public Event removeEventFloors(Floor floor) {
		this.eventFloors.remove(floor);
		return this;
	}

	public void setEventFloors(Set<Floor> floors) {
		this.eventFloors = floors;
	}

	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getWebUrl() {
		return webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

	public List<EventGuest> getEventGuestList() {
		return eventGuestList;
	}

	public void setEventGuestList(List<EventGuest> eventGuestList) {
		if (this.eventGuestList != null) {
			this.eventGuestList.clear();
		}
		this.eventGuestList.addAll(eventGuestList);
	}

	public SoundCode getSoundCode() {
		return soundCode;
	}

	public void setSoundCode(SoundCode soundCode) {
		this.soundCode = soundCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Event event = (Event) o;
		if (event.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), event.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", eventName=" + eventName + ", description=" + description + ", fromDate="
				+ fromDate + ", toDate=" + toDate + ", webUrl=" + webUrl + ", isDeleted=" + isDeleted + ", eventFloors="
				+ eventFloors + ", eventGuestList=" + eventGuestList + ", soundCode=" + soundCode + "]";
	}

}
