package com.eos.service;

import java.io.File;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eos.config.Constants;
import com.eos.domain.Authority;
import com.eos.domain.EosMember;
import com.eos.domain.SoundCode;
import com.eos.domain.User;
import com.eos.repository.AuthorityRepository;
import com.eos.repository.EosMemberRepository;
import com.eos.repository.UserRepository;
import com.eos.security.AuthoritiesConstants;
import com.eos.security.SecurityUtils;
import com.eos.service.dto.SoundCodeDTO;
import com.eos.service.dto.UserDTO;
import com.eos.service.util.RandomUtil;
import com.eos.web.rest.vm.ManagedUserVM;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

	@Inject
	private EosMemberRepository eosMemberRepository;

	private final Logger log = LoggerFactory.getLogger(UserService.class);

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	private final AuthorityRepository authorityRepository;

	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder,
			AuthorityRepository authorityRepository) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.authorityRepository = authorityRepository;
	}

	public Optional<User> activateRegistration(String key) {
		log.debug("Activating user for activation key {}", key);
		return userRepository.findOneByActivationKey(key).map(user -> {
			// activate given user for the registration key.
			user.setActivated(true);
			user.setActivationKey(null);
			log.debug("Activated user: {}", user);
			return user;
		});
	}

	public Optional<User> completePasswordReset(String newPassword, String key) {
		log.debug("Reset user password for reset key {}", key);

		return userRepository.findOneByResetKey(key)
				.filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400))).map(user -> {
					user.setPassword(passwordEncoder.encode(newPassword));
					user.setResetKey(null);
					user.setResetDate(null);
					return user;
				});
	}

	public Optional<User> requestPasswordReset(String mail) {
		return userRepository.findOneByEmailIgnoreCase(mail).filter(User::getActivated).map(user -> {
			user.setResetKey(RandomUtil.generateResetKey());
			user.setResetDate(Instant.now());
			return user;
		});
	}

	public User registerUser(ManagedUserVM userDTO) {

		User newUser = new User();
		Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
		Set<Authority> authorities = new HashSet<>();
		newUser.setLogin(userDTO.getLogin());
		// new user gets initially a generated password
		newUser.setPassword(userDTO.getPassword());
		newUser.setFirstName(userDTO.getFirstName());
		newUser.setLastName(userDTO.getLastName());
		newUser.setEmail(userDTO.getEmail());
		newUser.setImageUrl(userDTO.getImageUrl());
		newUser.setLangKey("en");
		// new user is not active
		newUser.setActivated(false);
		// new user gets registration key
		newUser.setActivationKey(RandomUtil.generateActivationKey());
		authorities.add(authority);
		newUser.setAuthorities(authorities);
		userRepository.save(newUser);
		log.debug("Created Information for User: {}", newUser);

		EosMember eosMember = new EosMember();
		eosMember.setUser(newUser);
		eosMember.setFromDate(userDTO.getFromDate());
		eosMember.setToDate(userDTO.getToDate());
		eosMember.setFloors(userDTO.getFloors());
		eosMember.setStartTime(userDTO.getStartTime());
		eosMember.setEndTime(userDTO.getEndTime());
		SoundCodeDTO soundCode = userDTO.getSoundCode();
		if (soundCode != null) {
			SoundCode sound = new SoundCode();
			sound.setLicenseNo(soundCode.getLicenseKey());
			sound.setUid(soundCode.getUid());
			eosMember.setSoundCode(sound);
		}

		eosMemberRepository.save(eosMember);

		return newUser;
	}

	public User createUser(UserDTO userDTO) {
		User user = new User();
		user.setLogin(userDTO.getLogin());
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setEmail(userDTO.getEmail());
		user.setImageUrl(userDTO.getImageUrl());
		if (userDTO.getLangKey() == null) {
			user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
		} else {
			user.setLangKey(userDTO.getLangKey());
		}
		if (userDTO.getAuthorities() != null) {
			Set<Authority> authorities = userDTO.getAuthorities().stream().map(authorityRepository::findOne)
					.collect(Collectors.toSet());
			user.setAuthorities(authorities);
		}
		String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
		user.setPassword(encryptedPassword);
		user.setResetKey(RandomUtil.generateResetKey());
		user.setResetDate(Instant.now());
		user.setActivated(true);
		userRepository.save(user);
		log.debug("Created Information for User: {}", user);
		return user;
	}

	/**
	 * Update basic information (first name, last name, email, language) for the
	 * current user.
	 *
	 * @param firstName
	 *            first name of user
	 * @param lastName
	 *            last name of user
	 * @param email
	 *            email id of user
	 * @param langKey
	 *            language key
	 * @param imageUrl
	 *            image URL of user
	 */
	public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
		userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(email);
			user.setLangKey(langKey);
			user.setImageUrl(imageUrl);
			log.debug("Changed Information for User: {}", user);
		});
	}

	/**
	 * Update all information for a specific user, and return the modified user.
	 *
	 * @param userDTO
	 *            user to update
	 * @return updated user
	 */
	public Optional<UserDTO> updateUser(UserDTO userDTO) {
		return Optional.of(userRepository.findOne(userDTO.getId())).map(user -> {
			user.setLogin(userDTO.getLogin());
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setEmail(userDTO.getEmail());
			user.setImageUrl(userDTO.getImageUrl());
			user.setActivated(userDTO.isActivated());
			user.setLangKey(userDTO.getLangKey());
			Set<Authority> managedAuthorities = user.getAuthorities();
			managedAuthorities.clear();
			userDTO.getAuthorities().stream().map(authorityRepository::findOne).forEach(managedAuthorities::add);
			log.debug("Changed Information for User: {}", user);
			return user;
		}).map(UserDTO::new);
	}

	public Optional<UserDTO> updateUser(User userDTO) {
		return Optional.of(userRepository.findOne(userDTO.getId())).map(user -> {
			user.setLogin(userDTO.getLogin());
			user.setActivated(userDTO.getActivated());
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setEmail(userDTO.getEmail());
			user.setImageUrl(userDTO.getImageUrl());
			user.setLangKey(userDTO.getLangKey());
			log.debug("Changed Information for User: {}", user);
			return user;
		}).map(UserDTO::new);
	}

	public void deleteUser(String login) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
			userRepository.delete(user);
			log.debug("Deleted User: {}", user);
		});
	}

	public void delete(Long id) {
		userRepository.findOneById(id).ifPresent(user -> {
			userRepository.delete(user);
			log.debug("Deleted User: {}", user);
		});
	}

	public Optional<User> memberRegistration(String password, String key) throws Exception {
		log.debug("Register member for activation key {}", key);
		return userRepository.findOneByActivationKey(key).map(user -> {
			// activate given user for the registration key.
			user.setActivated(true);
			user.setPassword(passwordEncoder.encode(password));
			user.setActivationKey(null);
			log.debug("Activated user: {}", user);
			return user;
		});

	}

	public void changePassword(String password, String oldPassword) throws Exception {
		Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());

		if (passwordEncoder.matches(oldPassword, user.get().getPassword())) {
			String encryptedPassword = passwordEncoder.encode(password);
			user.get().setPassword(encryptedPassword);
			userRepository.save(user.get());
			log.debug("Changed password for User: {}", user);

		} else {
			throw new Exception("Invalid old Password");
		}

	}

	@Transactional(readOnly = true)
	public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
		return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthoritiesByLogin(String login) {
		return userRepository.findOneWithAuthoritiesByLogin(login);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserAuthorities(String login) {
		Authority authority = new Authority();
		authority.setName("ROLE_ADMIN");
		return userRepository.findOneByLoginAndAuthorities(login, authority);
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities(Long id) {
		return userRepository.findOneWithAuthoritiesById(id);
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities() {
		return userRepository.findOneWithAuthoritiesByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null);
	}

	/**
	 * Not activated users should be automatically deleted after 3 days.
	 * <p>
	 * This is scheduled to get fired everyday, at 01:00 (am).
	 */
	@Scheduled(cron = ("${cron}"))
	public void removeNotActivatedUsers() {
		ZonedDateTime currentDateTime = ZonedDateTime.now();
		List<EosMember> eosMembers = eosMemberRepository
				.findAllByUserActivatedTrueAndToDateNotNullAndToDateBefore(currentDateTime);
		for (EosMember eosMember : eosMembers) {
			User user = userRepository.findOne(eosMember.getId());
			log.debug("Deleting not activated user {}");
			String path = System.getProperty("user.dir") + "\\src\\main\\resources\\static\\members" + user.getId();
			File file = new File(path);
			if (file.delete()) {
				log.debug("Wave file deleted successfully");
			} else {
				log.debug("File nor found");
			}
			user.setActivated(false);
			userRepository.save(user);
		}
	}

	/**
	 * @return a list of all the authorities
	 */
	public List<String> getAuthorities() {
		return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
	}

	public User trim(User user) {

		user.setFirstName(StringUtils.trim(user.getFirstName()));
		user.setLastName(StringUtils.trim(user.getLastName()));

		return user;

	}
}
