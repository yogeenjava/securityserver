package com.eos.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the EventGuest entity.
 */
public class EventGuestDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }	

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventGuestDTO eventGuestDTO = (EventGuestDTO) o;
        if(eventGuestDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eventGuestDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EventGuestDTO{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            "}";
    }
}
