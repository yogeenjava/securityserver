package com.eos.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

import com.eos.domain.EventGuestCsv;

/**
 * A DTO for the EventGuestCsv entity.
 */
public class EventGuestCsvDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull
	private String fileName;

	@NotNull
	@Lob
	private String fileData;

	public EventGuestCsvDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EventGuestCsvDTO(EventGuestCsv eventGuestCsv) {
		// TODO Auto-generated constructor stub
		this.id = eventGuestCsv.getId();
		this.fileName = eventGuestCsv.getFileName();
		this.fileData = eventGuestCsv.getFileData();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileData() {
		return fileData;
	}

	public void setFileData(String fileData) {
		this.fileData = fileData;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		EventGuestCsvDTO eventGuestCsvDTO = (EventGuestCsvDTO) o;
		if (eventGuestCsvDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), eventGuestCsvDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "EventGuestCsvDTO{" + "id=" + getId() + ", fileName='" + getFileName() + "'" + ", fileData='"
				+ getFileData() + "'" + "}";
	}
}
