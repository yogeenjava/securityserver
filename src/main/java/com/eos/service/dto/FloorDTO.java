package com.eos.service.dto;


import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.eos.domain.Floor;

/**
 * A DTO for the Floor entity.
 */
public class FloorDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull
    @Size(min = 60, max = 60)
	@NotEmpty
    private String floorNo;

    public FloorDTO(){
    		
    }
    
    public FloorDTO(Floor floor) {
		// TODO Auto-generated constructor stub
    	this.id = floor.getId();
    	this.floorNo = floor.getFloorNo();
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FloorDTO floorDTO = (FloorDTO) o;
        if(floorDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), floorDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FloorDTO{" +
            "id=" + getId() +
            ", floorNo='" + getFloorNo() + "'" +
            "}";
    }
}
