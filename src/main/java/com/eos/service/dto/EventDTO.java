package com.eos.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.URL;

import com.eos.domain.Event;
import com.eos.domain.Floor;
import com.eos.domain.SoundCode;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A DTO for the Event entity.
 */
public class EventDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4335121068987065239L;

	private Long id;

	@NotNull
	@Size(min = 5)
	private String eventName;

	private String description;

	@URL
	private String webUrl;

	private ZonedDateTime fromDate;

	private ZonedDateTime toDate;

	@JsonIgnore
	private String licenceKey;

	private Set<FloorDTO> eventFloors = new HashSet<>();

	@JsonIgnore
	private List<EventGuestDTO> eventGuest = new ArrayList<>();

	@JsonIgnore
	private SoundCodeDTO soundCode;

	private String file;

	private String fileName;

	public EventDTO() {

	}

	public EventDTO(Event event) {
		// TODO Auto-generated constructor stub
		if (event != null) {
			this.id = event.getId();
			this.eventName = event.getEventName();
			this.description = event.getDescription();
			this.fromDate = event.getFromDate();
			this.toDate = event.getToDate();
			this.webUrl = event.getWebUrl();
			this.fileName = event.getFileName();
			Set<Floor> eventFloorList = event.getEventFloors();
			FloorDTO floor = null;
			for (Floor f : eventFloorList) {
				floor = new FloorDTO(f);
				this.eventFloors.add(floor);
			}
			SoundCode sc = event.getSoundCode();
			SoundCodeDTO scDTO = null;
			if (sc != null) {
				scDTO = new SoundCodeDTO();
				scDTO.setId(sc.getId());
				scDTO.setLicenseKey(sc.getLicenseNo());
				scDTO.setUid(sc.getUid());
				this.soundCode = scDTO;
			}
			/*
			 * List<EventGuest> eventUserList = event.getEventGuestList();
			 * EventGuestDTO guest = null; for(EventGuest guest:eventUserList){
			 * guest = new EventGuestDTO(u); this.eventGuest.add(guest); }
			 */
		}

	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(ZonedDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public ZonedDateTime getToDate() {
		return toDate;
	}

	public void setToDate(ZonedDateTime toDate) {
		this.toDate = toDate;
	}

	public String getLicenceKey() {
		return licenceKey;
	}

	public void setLicenceKey(String licenceKey) {
		this.licenceKey = licenceKey;
	}

	public Set<FloorDTO> getEventFloors() {
		return eventFloors;
	}

	public void setEventFloors(Set<FloorDTO> floors) {
		this.eventFloors = floors;
	}

	public List<EventGuestDTO> getEventGuest() {
		return eventGuest;
	}

	public void setEventGuest(List<EventGuestDTO> eventGuest) {
		this.eventGuest = eventGuest;
	}

	public String getWebUrl() {
		return webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

	public SoundCodeDTO getSoundCode() {
		return soundCode;
	}

	public void setSoundCode(SoundCodeDTO soundCode) {
		this.soundCode = soundCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		EventDTO eventDTO = (EventDTO) o;
		if (eventDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), eventDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "EventDTO [id=" + id + ", eventName=" + eventName + ", description=" + description + ", webUrl=" + webUrl
				+ ", fromDate=" + fromDate + ", toDate=" + toDate + ", licenceKey=" + licenceKey + ", eventFloors="
				+ eventFloors + ", eventGuest=" + eventGuest + ", soundCode=" + soundCode + "]";
	}

}
