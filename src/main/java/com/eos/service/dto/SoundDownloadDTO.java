package com.eos.service.dto;

public class SoundDownloadDTO {

	private String soundFor;
	private Long id;
	public String getSoundFor() {
		return soundFor;
	}
	public void setSoundFor(String soundFor) {
		this.soundFor = soundFor;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
}
