package com.eos.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.eos.domain.EosMember;
import com.eos.domain.Floor;
import com.eos.domain.SoundCode;
import com.eos.domain.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A DTO for the EosMember entity.
 */
public class EosMemberDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private ZonedDateTime fromDate;

	private ZonedDateTime toDate;

	private Set<Floor> floors = new HashSet<>();

	private User user;

	private SoundCodeDTO soundCode;

	private ZonedDateTime startTime;
	private ZonedDateTime endTime;

	public EosMemberDTO() {
		super();
	}

	public EosMemberDTO(EosMember eosMember) {

		this.id = eosMember.getId();
		this.fromDate = eosMember.getFromDate();
		this.toDate = eosMember.getToDate();
		this.floors = eosMember.getFloors();
		this.user = eosMember.getUser();
		this.startTime = eosMember.getStartTime();
		this.endTime = eosMember.getEndTime();

		SoundCode sc = eosMember.getSoundCode();
		SoundCodeDTO scDTO = null;
		if (sc != null) {
			scDTO = new SoundCodeDTO();
			scDTO.setId(sc.getId());
			scDTO.setLicenseKey(sc.getLicenseNo());
			scDTO.setUid(sc.getUid());
			this.soundCode = scDTO;
		}
	}

	public ZonedDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(ZonedDateTime startTime) {
		this.startTime = startTime;
	}

	public ZonedDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(ZonedDateTime endTime) {
		this.endTime = endTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(ZonedDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public ZonedDateTime getToDate() {
		return toDate;
	}

	public void setToDate(ZonedDateTime toDate) {
		this.toDate = toDate;
	}

	public Set<Floor> getFloors() {
		return floors;
	}

	public void setFloors(Set<Floor> floors) {
		this.floors = floors;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@JsonIgnore
	public SoundCodeDTO getSoundCode() {
		return soundCode;
	}

	@JsonProperty
	public void setSoundCode(SoundCodeDTO soundCode) {
		this.soundCode = soundCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		EosMemberDTO eosMemberDTO = (EosMemberDTO) o;
		if (eosMemberDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), eosMemberDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "EosMemberDTO [id=" + id + ", fromDate=" + fromDate + ", toDate=" + toDate + ", floors=" + floors
				+ ", user=" + user + ", soundCode=" + soundCode + "]";
	}

}
