package com.eos.service.dto;

import java.io.Serializable;
import java.util.Base64;

import com.eos.domain.IPAddress;

public class IPAddressDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private byte[] ipAddress;

	public IPAddressDTO(IPAddress ipAddress) {
		super();
		this.id = ipAddress.getId();
		this.ipAddress = Base64.getEncoder().encode(ipAddress.getIpAddress().getBytes());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(byte[] ipAddress) {
		this.ipAddress = ipAddress;
	}

}
