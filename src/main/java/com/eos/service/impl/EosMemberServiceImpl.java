package com.eos.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eos.domain.EosMember;
import com.eos.domain.SoundCode;
import com.eos.repository.EosMemberRepository;
import com.eos.security.SecurityUtils;
import com.eos.service.EosMemberService;
import com.eos.service.dto.EosMemberDTO;
import com.eos.service.mapper.EosMemberMapper;

/**
 * Service Implementation for managing EosMember.
 */
@Service
@Transactional
public class EosMemberServiceImpl implements EosMemberService {

	private final Logger log = LoggerFactory.getLogger(EosMemberServiceImpl.class);

	private final EosMemberRepository eosMemberRepository;

	private final EosMemberMapper eosMemberMapper;

	public EosMemberServiceImpl(EosMemberRepository eosMemberRepository, EosMemberMapper eosMemberMapper) {
		this.eosMemberRepository = eosMemberRepository;
		this.eosMemberMapper = eosMemberMapper;
	}

	/**
	 * Save a eosMember.
	 *
	 * @param eosMemberDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public EosMemberDTO save(EosMemberDTO eosMemberDTO) {
		log.debug("Request to save EosMember : {}", eosMemberDTO);
		EosMember eosMember = eosMemberMapper.toEntity(eosMemberDTO);
		eosMember = eosMemberRepository.save(eosMember);
		return eosMemberMapper.toDto(eosMember);
	}

	@Override
	public Optional<EosMemberDTO> updateMember(EosMemberDTO eosMemberDTO) {
		return Optional.of(eosMemberRepository.findOne(eosMemberDTO.getId())).map(user -> {
			user.setFromDate(eosMemberDTO.getFromDate());
			user.setToDate(eosMemberDTO.getToDate());
			user.setFloors(eosMemberDTO.getFloors());
			user.setStartTime(eosMemberDTO.getStartTime());
			user.setEndTime(eosMemberDTO.getEndTime());
			log.debug("Changed Information for User: {}", user);
			return user;
		}).map(EosMemberDTO::new);
	}

	/**
	 * Get all the eosMembers.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<EosMemberDTO> findAll(Pageable pageable) {
		log.debug("Request to get all EosMembers");
		return eosMemberRepository.findAllByUserLoginNot(pageable, SecurityUtils.getCurrentUserLogin())
				.map(eosMemberMapper::toDto);
	}

	/**
	 * Get one eosMember by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public EosMemberDTO findOne(Long id) {
		log.debug("Request to get EosMember : {}", id);
		EosMember eosMember = eosMemberRepository.findOneById(id);
		if (eosMember != null) {
			return eosMemberMapper.toDto(eosMember);
		}
		return null;
	}

	/**
	 * Delete the eosMember by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete EosMember : {}", id);
		eosMemberRepository.delete(id);
	}

	@Override
	public EosMemberDTO findOneByLicenceKeyAndFloorNo(String licenceKey, String floorNo) {
		// TODO Auto-generated method stub
		// EosMember eosMember =
		// eosMemberRepository.findOneByLicenceKeyAndFloorsFloorNo(licenceKey,floorNo);
		// return eosMemberMapper.toDto(eosMember);
		return null;
	}

	@Override
	public EosMemberDTO findOneBySoundCodeAndFloor(SoundCode sc, String floorNo) {

		EosMember eosMember = eosMemberRepository.findOneBySoundCodeAndFloorsFloorNo(sc, floorNo);
		if (eosMember != null) {
			return eosMemberMapper.toDto(eosMember);
		}
		return null;
	}

	@Override
	public EosMemberDTO findOneBySoundCode(SoundCode sc) {

		EosMember eosMember = eosMemberRepository.findOneBySoundCode(sc);

		if (eosMember != null) {
			return eosMemberMapper.toDto(eosMember);
		}

		return null;
	}

}
