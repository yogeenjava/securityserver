package com.eos.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eos.domain.SoundCode;
import com.eos.repository.SoundCodeRepository;
import com.eos.service.SoundCodeService;

@Service
@Transactional
public class SoundCodeServiceImpl implements SoundCodeService{
	
	@Autowired
	private SoundCodeRepository soundCodeRepository;

	@Override
	public SoundCode findOneByLicenseNoAndUid(String licenceKey, String uid) {
		// TODO Auto-generated method stub
		SoundCode sc = soundCodeRepository.findOneByLicenseNoAndUid(licenceKey,uid);
		return sc;
	}

}
