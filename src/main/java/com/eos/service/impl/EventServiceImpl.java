package com.eos.service.impl;

import java.io.File;
import java.time.ZonedDateTime;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eos.domain.Event;
import com.eos.domain.EventGuest;
import com.eos.domain.EventGuestCsv;
import com.eos.domain.SoundCode;
import com.eos.repository.EventGuestCsvRepository;
import com.eos.repository.EventGuestRepository;
import com.eos.repository.EventRepository;
import com.eos.service.EventService;
import com.eos.service.dto.EventDTO;
import com.eos.service.dto.EventGuestDTO;
import com.eos.service.mapper.EventMapper;

/**
 * Service Implementation for managing Event.
 */
@Service
@Transactional
public class EventServiceImpl implements EventService {

	private final Logger log = LoggerFactory.getLogger(EventServiceImpl.class);

	@Inject
	private EventGuestCsvRepository eventGuestCsvRepository;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private EventMapper eventMapper;

	@Autowired
	private EventGuestRepository egRepository;

	private Boolean isDeleted = false;

	/**
	 * Save a event.
	 *
	 * @param eventDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public EventDTO save(EventDTO eventDTO) {
		log.debug("Request to save Event : {}", eventDTO);
		Event event = eventMapper.toEntity(eventDTO);
		event = eventRepository.save(event);
		return eventMapper.toDto(event);
	}

	/**
	 * Get all the events.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<EventDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Events");
		return eventRepository
				.findAllByIsDeletedAndToDateAfterOrderByFromDateAsc(pageable, isDeleted, ZonedDateTime.now())
				.map(eventMapper::toDto);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<EventDTO> findAllEvent(Pageable pageable) {
		log.debug("Request to get all Events");
		return eventRepository.findAllByIsDeletedFalse(pageable).map(eventMapper::toDto);
	}

	/**
	 * Get one event by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public EventDTO findOne(Long id) {
		log.debug("Request to get Event : {}", id);
		Event event = eventRepository.findOneByIdAndIsDeleted(id, isDeleted);
		log.info("Event = " + event);
		return eventMapper.toDto(event);
	}

	/**
	 * Delete the event by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @throws Exception
	 */
	@Override
	public void delete(Long id) throws Exception {
		log.debug("Request to delete Event : {}", id);
		Event event = eventRepository.findOneByIdAndIsDeleted(id, isDeleted);
		if (event != null) {
			event.setIsDeleted(true);
			eventRepository.save(event);
		} else {
			throw new Exception("event not found");
		}
	}

	@Override
	public EventDTO update(EventDTO eventDto) {
		// TODO Auto-generated method stub
		log.debug("Request to save Event : {}", eventDto);
		Event event = eventMapper.toEntity(eventDto);
		Event temp = eventRepository.findOneByIdAndIsDeleted(event.getId(), isDeleted);
		if (temp != null) {

			temp.setEventName(event.getEventName());
			temp.setWebUrl(event.getWebUrl());
			temp.setFileName(event.getFileName());
			temp.setFromDate(event.getFromDate());
			temp.setToDate(event.getToDate());
			temp.setDescription(event.getDescription());
			temp.setEventFloors(event.getEventFloors());

			if (eventDto.getFile() == null && eventDto.getFileName() != null) {
				List<EventGuest> egList = egRepository.findAllByEventId(eventDto.getId());
				temp.setEventGuestList(egList);
			} else {
				temp.setEventGuestList(event.getEventGuestList());
			}
			event = eventRepository.save(temp);
		}

		if (eventDto.getFile() == null && eventDto.getFileName() == null) {
			if (eventGuestCsvRepository.findOne(event.getId()) != null) {
				eventGuestCsvRepository.delete(event.getId());
			}
		}

		if (eventDto.getFile() != null) {
			if (eventGuestCsvRepository.findOne(event.getId()) != null) {
				eventGuestCsvRepository.delete(event.getId());
			}
			EventGuestCsv eventGuestCsv = new EventGuestCsv();
			eventGuestCsv.setEvent(event);
			eventGuestCsv.setFileName(eventDto.getFileName());
			eventGuestCsv.setFileData(eventDto.getFile());

			eventGuestCsvRepository.save(eventGuestCsv);
		}
		return eventMapper.toDto(event);
	}

	@Override
	public Boolean isValidEventGuest(EventGuestDTO eventGuestDto) {
		// TODO Auto-generated method stub
		Event event = eventRepository.findOneByIdAndIsDeletedAndEventGuestListEmail(eventGuestDto.getId(), isDeleted,
				eventGuestDto.getEmail());
		if (event != null) {
			return true;
		}
		return false;
	}

	@Override
	@Scheduled(cron = ("${cron}"))
	public void inActivateEvent() {

		ZonedDateTime currentDateTime = ZonedDateTime.now();

		List<Event> events = eventRepository.findAllByIsDeletedFalseAndActivatedTrueAndToDateBefore(currentDateTime);
		for (Event event : events) {
			log.debug("Deleting not activated user {}", event);
			String path = System.getProperty("user.dir") + "\\src\\main\\resources\\static\\events" + event.getId();
			File file = new File(path);
			if (file.delete()) {
				log.debug("Wave file deleted successfully");
			} else {
				log.debug("File nor found");
			}
			event.setActivated(false);
			eventRepository.save(event);
		}
	}

	@Override
	public EventDTO findOneBySoundCodeAndFloor(SoundCode sc, String floorNo) {

		Event event = eventRepository.findOneBySoundCodeAndEventFloorsFloorNo(sc, floorNo);
		if (event != null) {
			return eventMapper.toDto(event);
		}
		return null;
	}

	@Override
	public EventDTO findOneBySoundCode(SoundCode sc) {

		Event event = eventRepository.findOneBySoundCode(sc);
		if (event != null) {
			return eventMapper.toDto(event);
		}
		return null;
	}

}
