package com.eos.service.impl;

import com.eos.service.EventGuestCsvService;
import com.eos.domain.EventGuestCsv;
import com.eos.repository.EventGuestCsvRepository;
import com.eos.service.dto.EventGuestCsvDTO;
import com.eos.service.mapper.EventGuestCsvMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing EventGuestCsv.
 */
@Service
@Transactional
public class EventGuestCsvServiceImpl implements EventGuestCsvService{

    private final Logger log = LoggerFactory.getLogger(EventGuestCsvServiceImpl.class);

    private final EventGuestCsvRepository eventGuestCsvRepository;

    private final EventGuestCsvMapper eventGuestCsvMapper;

    public EventGuestCsvServiceImpl(EventGuestCsvRepository eventGuestCsvRepository, EventGuestCsvMapper eventGuestCsvMapper) {
        this.eventGuestCsvRepository = eventGuestCsvRepository;
        this.eventGuestCsvMapper = eventGuestCsvMapper;
    }

    /**
     * Save a eventGuestCsv.
     *
     * @param eventGuestCsvDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EventGuestCsvDTO save(EventGuestCsvDTO eventGuestCsvDTO) {
        log.debug("Request to save EventGuestCsv : {}", eventGuestCsvDTO);
        EventGuestCsv eventGuestCsv = eventGuestCsvMapper.toEntity(eventGuestCsvDTO);
        eventGuestCsv = eventGuestCsvRepository.save(eventGuestCsv);
        return eventGuestCsvMapper.toDto(eventGuestCsv);
    }

    /**
     *  Get all the eventGuestCsvs.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<EventGuestCsvDTO> findAll() {
        log.debug("Request to get all EventGuestCsvs");
        return eventGuestCsvRepository.findAll().stream()
            .map(eventGuestCsvMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one eventGuestCsv by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EventGuestCsvDTO findOne(Long id) {
        log.debug("Request to get EventGuestCsv : {}", id);
        EventGuestCsv eventGuestCsv = eventGuestCsvRepository.findOne(id);
        return eventGuestCsvMapper.toDto(eventGuestCsv);
    }

    /**
     *  Delete the  eventGuestCsv by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EventGuestCsv : {}", id);
        eventGuestCsvRepository.delete(id);
    }
}
