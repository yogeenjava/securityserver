package com.eos.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eos.domain.Floor;
import com.eos.repository.FloorRepository;
import com.eos.service.FloorService;
import com.eos.service.dto.FloorDTO;
import com.eos.service.mapper.FloorMapper;

/**
 * Service Implementation for managing Floor.
 */
@Service
@Transactional
public class FloorServiceImpl implements FloorService {

	private final Logger log = LoggerFactory.getLogger(FloorServiceImpl.class);

	@Autowired
	private FloorRepository floorRepository;

	@Autowired
	private FloorMapper floorMapper;

	private Boolean isDeleted = false;

	/*
	 * public FloorServiceImpl(FloorRepository floorRepository, FloorMapper
	 * floorMapper) { this.floorRepository = floorRepository; this.floorMapper =
	 * floorMapper; }
	 */
	/**
	 * Save a floor.
	 *
	 * @param floorDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public FloorDTO save(FloorDTO floorDTO) {
		log.debug("Request to save Floor : {}", floorDTO);
		Floor floor = floorMapper.toEntity(floorDTO);
		floor = floorRepository.save(floor);
		return floorMapper.toDto(floor);
	}

	/**
	 * Get all the floors.
	 *
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public List<FloorDTO> findAll() {
		log.debug("Request to get all Floors");
		return floorRepository.findAllByIsDeleted(isDeleted).stream().map(floorMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Get one floor by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public FloorDTO findOne(Long id) {
		log.info("Request to get Floor : {}", id);
		Floor floor = floorRepository.findOneByIdAndIsDeleted(id, isDeleted);

		return floorMapper.toDto(floor);
	}

	/**
	 * Delete the floor by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public Boolean delete(Long id) {
		log.info("Request to delete Floor : {}", id);
		Floor floor = floorRepository.findOneByIdAndIsDeleted(id, isDeleted);
		if (floor != null) {
			floor.setIsDeleted(true);
			floorRepository.save(floor);
			return true;
		}
		return false;
	}

	@Override
	public FloorDTO findOneWithNo(String floorNo) {
		Floor floor = floorRepository.findOneByFloorNo(floorNo);
		if(floor != null) {
			return floorMapper.toDto(floor);
		}
		return null;
	}
}
