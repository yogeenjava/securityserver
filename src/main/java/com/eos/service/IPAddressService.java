package com.eos.service;

import com.eos.domain.IPAddress;
import com.eos.repository.IPAddressRepository;
import com.eos.service.dto.IPAddressDTO;
import com.eos.service.mapper.IpAddressMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing IPAddress.
 */
@Service
@Transactional
public class IPAddressService {

	private final Logger log = LoggerFactory.getLogger(IPAddressService.class);

	private final IPAddressRepository iPAddressRepository;

	@Autowired
	private IpAddressMapper addressMapper;

	public IPAddressService(IPAddressRepository iPAddressRepository) {
		this.iPAddressRepository = iPAddressRepository;
	}

	/**
	 * Save a iPAddress.
	 *
	 * @param iPAddress
	 *            the entity to save
	 * @return the persisted entity
	 */
	public IPAddress save(IPAddress iPAddress) {
		log.debug("Request to save IPAddress : {}", iPAddress);
		return iPAddressRepository.save(iPAddress);
	}

	/**
	 * Get all the iPAddresses.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<IPAddressDTO> findAll() {
		log.debug("Request to get all IPAddresses");
		return addressMapper.toDTOs(iPAddressRepository.findAll());
	}

	/**
	 * Get one iPAddress by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public IPAddress findOne(Long id) {
		log.debug("Request to get IPAddress : {}", id);
		return iPAddressRepository.findOne(id);
	}

	/**
	 * Delete the iPAddress by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete IPAddress : {}", id);
		iPAddressRepository.delete(id);
	}
}
