package com.eos.service;

import com.eos.service.dto.EventGuestDTO;
import java.util.List;

/**
 * Service Interface for managing EventGuest.
 */
public interface EventGuestService {

    /**
     * Save a eventGuest.
     *
     * @param eventGuestDTO the entity to save
     * @return the persisted entity
     */
    EventGuestDTO save(EventGuestDTO eventGuestDTO);

    /**
     *  Get all the eventGuests.
     *
     *  @return the list of entities
     */
    List<EventGuestDTO> findAll();

    /**
     *  Get the "id" eventGuest.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EventGuestDTO findOne(Long id);

    /**
     *  Delete the "id" eventGuest.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
