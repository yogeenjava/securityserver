package com.eos.service.mapper;

import org.springframework.stereotype.Service;

import com.eos.domain.Floor;
import com.eos.service.dto.FloorDTO;

/**
 * Mapper for the entity Floor and its DTO FloorDTO.
 */

@Service
public class FloorMapper {

	public Floor fromId(Long id) {
		if (id == null) {
			return null;
		}
		Floor floor = new Floor();
		floor.setId(id);
		return floor;
	}

	public Floor toEntity(FloorDTO floorDTO) {
		if (floorDTO == null) {
			return null;
		} else {
			Floor floor = new Floor();
			floor.setId(floorDTO.getId());
			floor.setFloorNo(floorDTO.getFloorNo());
			return floor;
		}
	}

	public FloorDTO toDto(Floor floor) {
		return new FloorDTO(floor);
	}
}
