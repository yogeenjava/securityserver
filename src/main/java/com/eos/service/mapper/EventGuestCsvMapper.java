package com.eos.service.mapper;

import org.springframework.stereotype.Service;

import com.eos.domain.EventGuestCsv;
import com.eos.service.dto.EventGuestCsvDTO;

/**
 * Mapper for the entity EventGuestCsv and its DTO EventGuestCsvDTO.
 */
@Service
public class EventGuestCsvMapper {

	public EventGuestCsv fromId(Long id) {
		if (id == null) {
			return null;
		}
		EventGuestCsv eventGuestCsv = new EventGuestCsv();
		eventGuestCsv.setId(id);
		return eventGuestCsv;
	}

	public EventGuestCsv toEntity(EventGuestCsvDTO eventGuestCsvDTO) {
		if (eventGuestCsvDTO == null) {
			return null;
		} else {
			EventGuestCsv eventGuestCsv = new EventGuestCsv();
			eventGuestCsv.setId(eventGuestCsvDTO.getId());
			return eventGuestCsv;
		}
	}

	public EventGuestCsvDTO toDto(EventGuestCsv eventGuestCsv) {
		return new EventGuestCsvDTO(eventGuestCsv);
	}

}
