package com.eos.service.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.eos.domain.Event;
import com.eos.domain.EventGuest;
import com.eos.domain.Floor;
import com.eos.domain.SoundCode;
import com.eos.service.dto.EventDTO;
import com.eos.service.dto.EventGuestDTO;
import com.eos.service.dto.FloorDTO;
import com.eos.service.dto.SoundCodeDTO;

/**
 * Mapper for the entity Event and its DTO EventDTO.
 */
@Service
public class EventMapper {

	public Event toEntity(EventDTO eventDTO) {
		if (eventDTO == null) {
			return null;
		} else {
			Event event = new Event();
			event.setId(eventDTO.getId());
			event.setEventName(eventDTO.getEventName());
			event.setDescription(eventDTO.getDescription());
			event.setFromDate(eventDTO.getFromDate());
			event.setToDate(eventDTO.getToDate());
			event.setWebUrl(eventDTO.getWebUrl());
			event.setFileName(eventDTO.getFileName());
			Set<FloorDTO> eventFloorList = eventDTO.getEventFloors();
			Floor floor = null;
			Set<Floor> floorList = new HashSet<>();
			for (FloorDTO f : eventFloorList) {
				floor = new Floor();
				floor.setId(f.getId());
				floorList.add(floor);
			}
			event.setEventFloors(floorList);
			List<EventGuestDTO> eventUserList = eventDTO.getEventGuest();
			EventGuest eg = null;
			List<EventGuest> guestList = new ArrayList<>();
			for (EventGuestDTO guest : eventUserList) {
				eg = new EventGuest();
				eg.setEmail(guest.getEmail());
				guestList.add(eg);
			}
			// event.setEventUsers(userList);
			event.setEventGuestList(guestList);

			SoundCodeDTO soundCode = eventDTO.getSoundCode();
			if (soundCode != null) {
				SoundCode sound = new SoundCode();
				sound.setLicenseNo(soundCode.getLicenseKey());
				sound.setUid(soundCode.getUid());
				event.setSoundCode(sound);
			}

			return event;
		}
	}

	public EventDTO toDto(Event event) {
		// TODO Auto-generated method stub
		return new EventDTO(event);
	}

	public Event fromId(Long id) {
		if (id == null) {
			return null;
		}
		Event event = new Event();
		event.setId(id);
		return event;
	}

}
