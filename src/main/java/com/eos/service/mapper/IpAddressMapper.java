package com.eos.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.eos.domain.IPAddress;
import com.eos.service.dto.IPAddressDTO;

@Service
public class IpAddressMapper {

	public IPAddressDTO toDTO(IPAddress address) {
		return new IPAddressDTO(address);
	}
	
	public List<IPAddressDTO> toDTOs(List<IPAddress> ips) {
        return ips.stream()
            .filter(Objects::nonNull)
            .map(this::toDTO)
            .collect(Collectors.toList());
    }
}
