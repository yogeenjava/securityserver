package com.eos.service.mapper;

import org.springframework.stereotype.Service;

import com.eos.domain.EosMember;
import com.eos.domain.SoundCode;
import com.eos.service.dto.EosMemberDTO;
import com.eos.service.dto.SoundCodeDTO;

/**
 * Mapper for the entity EosMember and its DTO EosMemberDTO.
 */
@Service
public class EosMemberMapper {

	public EosMember fromId(Long id) {
		if (id == null) {
			return null;
		}
		EosMember eosMember = new EosMember();
		eosMember.setId(id);
		return eosMember;
	}

	public EosMember toEntity(EosMemberDTO eosMemberDTO) {
		if (eosMemberDTO == null) {
			return null;
		} else {
			EosMember eosMember = new EosMember();
			eosMember.setId(eosMemberDTO.getId());
			eosMember.setFromDate(eosMemberDTO.getFromDate());
			eosMember.setToDate(eosMemberDTO.getToDate());
			eosMember.setFloors(eosMemberDTO.getFloors());
			eosMember.setUser(eosMemberDTO.getUser());
			eosMember.setStartTime(eosMemberDTO.getStartTime());
			eosMember.setEndTime(eosMemberDTO.getEndTime());
			SoundCodeDTO soundCode = eosMemberDTO.getSoundCode();
			if (soundCode != null) {
				SoundCode sound = new SoundCode();
				sound.setLicenseNo(soundCode.getLicenseKey());
				sound.setUid(soundCode.getUid());
				eosMember.setSoundCode(sound);
			}
			return eosMember;
		}
	}

	public EosMemberDTO toDto(EosMember eosMember) {
		return new EosMemberDTO(eosMember);
	}
}
