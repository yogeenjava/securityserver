package com.eos.service;

import com.eos.domain.SoundCode;

public interface SoundCodeService {

	public SoundCode findOneByLicenseNoAndUid(String licenceKey, String uid);
}
