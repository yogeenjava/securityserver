package com.eos.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.eos.domain.SoundCode;
import com.eos.service.dto.EventDTO;
import com.eos.service.dto.EventGuestDTO;

/**
 * Service Interface for managing Event.
 */
public interface EventService {

	/**
	 * Save a event.
	 *
	 * @param eventDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	EventDTO save(EventDTO eventDTO);

	/**
	 * Get all the events.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	Page<EventDTO> findAll(Pageable pageable);

	/**
	 * Get the "id" event.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	EventDTO findOne(Long id);

	/**
	 * Delete the "id" event.
	 *
	 * @param id
	 *            the id of the entity
	 * @throws Exception
	 */
	void delete(Long id) throws Exception;

	EventDTO update(EventDTO eventDto);

	Boolean isValidEventGuest(EventGuestDTO eventGuestDto);

	void inActivateEvent();

	Page<EventDTO> findAllEvent(Pageable pageable);

	EventDTO findOneBySoundCodeAndFloor(SoundCode sc, String floorNo);

	EventDTO findOneBySoundCode(SoundCode sc);

}
