package com.eos.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.eos.domain.SoundCode;
import com.eos.service.dto.EosMemberDTO;

/**
 * Service Interface for managing EosMember.
 */
public interface EosMemberService {

    /**
     * Save a eosMember.
     *
     * @param eosMemberDTO the entity to save
     * @return the persisted entity
     */
    EosMemberDTO save(EosMemberDTO eosMemberDTO);

    /**
     *  Get all the eosMembers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<EosMemberDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" eosMember.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EosMemberDTO findOne(Long id);

    /**
     *  Delete the "id" eosMember.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

	Optional<EosMemberDTO> updateMember(EosMemberDTO eosMember);

	EosMemberDTO findOneByLicenceKeyAndFloorNo(String licenceKey, String floorNo);

	EosMemberDTO findOneBySoundCodeAndFloor(SoundCode sc, String floorNo);

	EosMemberDTO findOneBySoundCode(SoundCode sc);
}
