package com.eos.service;

import com.eos.service.dto.EventGuestCsvDTO;
import java.util.List;

/**
 * Service Interface for managing EventGuestCsv.
 */
public interface EventGuestCsvService {

    /**
     * Save a eventGuestCsv.
     *
     * @param eventGuestCsvDTO the entity to save
     * @return the persisted entity
     */
    EventGuestCsvDTO save(EventGuestCsvDTO eventGuestCsvDTO);

    /**
     *  Get all the eventGuestCsvs.
     *
     *  @return the list of entities
     */
    List<EventGuestCsvDTO> findAll();

    /**
     *  Get the "id" eventGuestCsv.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EventGuestCsvDTO findOne(Long id);

    /**
     *  Delete the "id" eventGuestCsv.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
