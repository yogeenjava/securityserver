package com.eos.web.rest.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.eos.config.ApplicationProperties;
import com.eos.service.EosMemberService;
import com.eos.service.EventService;
import com.eos.service.dto.EosMemberDTO;
import com.eos.service.dto.EventDTO;

import net.spy.memcached.MemcachedClient;

@Service
public class SoundCodeUtil {

	private final Logger log = LoggerFactory.getLogger(SoundCodeUtil.class);

	@Autowired
	private EventService eventService;

	@Autowired
	private EosMemberService eosMemberService;

	@Autowired
	private ApplicationProperties appProperties;

	private String licenseKey = null;
	private String uid = null;
	private Long id = null;
	private String downloadLink = null;

	public String generateSoundFile(String soundFor, Long id) {
		log.info("id = " + id);
		this.id = id;
		try {
			String contextPath = System.getProperty("user.dir");
			String soundPath = appProperties.getSoundUploadPath();
			log.info("contextPath = " + contextPath);
			if (soundFor.equals("member")) {
				EosMemberDTO eosMemberDTO = eosMemberService.findOne(id);
				if (eosMemberDTO != null && eosMemberDTO.getSoundCode() != null) {
					licenseKey = eosMemberDTO.getSoundCode().getLicenseKey();
					uid = eosMemberDTO.getSoundCode().getUid();
					soundPath += "/members/";
					log.info("licenseKey = " + licenseKey);
					log.info("uid = " + uid);
					String command = generateCommand(contextPath, soundPath);
					this.generateWavFile(command);
					this.cacheSoundFile(soundPath + id + ".wav");
					log.info("Member created found");

				} else {
					log.info("Member details not found");
				}

			} else if (soundFor.equals("guest")) {
				EventDTO eventDTO = eventService.findOne(id);
				log.debug(eventDTO.toString());
				if (eventDTO != null && eventDTO.getSoundCode() != null) {
					licenseKey = eventDTO.getSoundCode().getLicenseKey();
					uid = eventDTO.getSoundCode().getUid();
					log.info("licenseKey = " + licenseKey);
					log.info("uid = " + uid);
					soundPath += "/events/";
					String command = generateCommand(contextPath, soundPath);
					this.generateWavFile(command);
					this.cacheSoundFile(soundPath + id + ".wav");
					// this.sendFile(response, fileName);
				} else {
					log.info("event details not found");
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return downloadLink;
	}

	public String generateCommand(String contextPath, String soundPath) {
		log.info("soundPath = " + soundPath);
		File dir = new File(soundPath);
		if (!dir.exists()) {
			dir.mkdirs();
			log.info("path is created");
		}
		String scriptPath = contextPath + "/SoundProgram/";
		log.info("scriptPath = " + scriptPath);
		String fileName = soundPath + id + ".wav";
		String command = "sh " + scriptPath + "generateSound.sh " + scriptPath + " " + uid + " " + fileName + " "
				+ licenseKey;

		log.info("command = " + command);
		return command;
	}

	private void generateWavFile(String command) throws IOException {
		Runtime rt = Runtime.getRuntime();
		Process p = rt.exec(command);
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		StringBuffer output = new StringBuffer();
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}

		log.info("output = " + output);

	}

	@Async
	public void deleteSoundFile(String soundFilePath) {

		File file = new File(soundFilePath);
		if (file.exists()) {

			file.delete();
			log.info("Sound file deleted successfully");
		}

	}

	@Async
	private void cacheSoundFile(String soundFilePath) {

		File file = new File(soundFilePath);
		byte[] bytesArray = new byte[(int) file.length()];

		MemcachedClient c;
		try {
			FileInputStream fis = new FileInputStream(file);
			fis.read(bytesArray); // read file into bytes[]
			fis.close();
			c = new MemcachedClient(new InetSocketAddress("edgeof.4vkjsx.cfg.apne1.cache.amazonaws.com", 11211));
			if (file.exists()) {
				c.set(soundFilePath, 600, bytesArray);
			}
			log.info(" Memched Object >>>>" + c.get(soundFilePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	}
}
