package com.eos.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.eos.service.EventGuestCsvService;
import com.eos.web.rest.util.HeaderUtil;
import com.eos.service.dto.EventGuestCsvDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EventGuestCsv.
 */
@RestController
@RequestMapping("/api/admin")
public class EventGuestCsvResource {

    private final Logger log = LoggerFactory.getLogger(EventGuestCsvResource.class);

    private static final String ENTITY_NAME = "eventGuestCsv";

    private final EventGuestCsvService eventGuestCsvService;

    public EventGuestCsvResource(EventGuestCsvService eventGuestCsvService) {
        this.eventGuestCsvService = eventGuestCsvService;
    }

    /**
     * POST  /event-guest-csvs : Create a new eventGuestCsv.
     *
     * @param eventGuestCsvDTO the eventGuestCsvDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new eventGuestCsvDTO, or with status 400 (Bad Request) if the eventGuestCsv has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/event-guest-csvs")
    @Timed
    public ResponseEntity<EventGuestCsvDTO> createEventGuestCsv(@Valid @RequestBody EventGuestCsvDTO eventGuestCsvDTO) throws URISyntaxException {
        log.debug("REST request to save EventGuestCsv : {}", eventGuestCsvDTO);
        if (eventGuestCsvDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new eventGuestCsv cannot already have an ID")).body(null);
        }
        EventGuestCsvDTO result = eventGuestCsvService.save(eventGuestCsvDTO);
        return ResponseEntity.created(new URI("/api/event-guest-csvs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /event-guest-csvs : Updates an existing eventGuestCsv.
     *
     * @param eventGuestCsvDTO the eventGuestCsvDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated eventGuestCsvDTO,
     * or with status 400 (Bad Request) if the eventGuestCsvDTO is not valid,
     * or with status 500 (Internal Server Error) if the eventGuestCsvDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/event-guest-csvs")
    @Timed
    public ResponseEntity<EventGuestCsvDTO> updateEventGuestCsv(@Valid @RequestBody EventGuestCsvDTO eventGuestCsvDTO) throws URISyntaxException {
        log.debug("REST request to update EventGuestCsv : {}", eventGuestCsvDTO);
        if (eventGuestCsvDTO.getId() == null) {
            return createEventGuestCsv(eventGuestCsvDTO);
        }
        EventGuestCsvDTO result = eventGuestCsvService.save(eventGuestCsvDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, eventGuestCsvDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /event-guest-csvs : get all the eventGuestCsvs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of eventGuestCsvs in body
     */
    @GetMapping("/event-guest-csvs")
    @Timed
    public List<EventGuestCsvDTO> getAllEventGuestCsvs() {
        log.debug("REST request to get all EventGuestCsvs");
        return eventGuestCsvService.findAll();
        }

    /**
     * GET  /event-guest-csvs/:id : get the "id" eventGuestCsv.
     *
     * @param id the id of the eventGuestCsvDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the eventGuestCsvDTO, or with status 404 (Not Found)
     */
    @GetMapping("/event-guest-csvs/{id}")
    @Timed
    public ResponseEntity<EventGuestCsvDTO> getEventGuestCsv(@PathVariable Long id) {
        log.debug("REST request to get EventGuestCsv : {}", id);
        EventGuestCsvDTO eventGuestCsvDTO = eventGuestCsvService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(eventGuestCsvDTO));
    }

    /**
     * DELETE  /event-guest-csvs/:id : delete the "id" eventGuestCsv.
     *
     * @param id the id of the eventGuestCsvDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/event-guest-csvs/{id}")
    @Timed
    public ResponseEntity<Void> deleteEventGuestCsv(@PathVariable Long id) {
        log.debug("REST request to delete EventGuestCsv : {}", id);
        eventGuestCsvService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
