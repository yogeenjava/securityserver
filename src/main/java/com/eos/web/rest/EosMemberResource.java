package com.eos.web.rest;

import java.net.URISyntaxException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.eos.domain.Authority;
import com.eos.domain.SoundCode;
import com.eos.domain.User;
import com.eos.repository.UserRepository;
import com.eos.security.AuthoritiesConstants;
import com.eos.service.EosMemberService;
import com.eos.service.MailService;
import com.eos.service.SoundCodeService;
import com.eos.service.UserService;
import com.eos.service.dto.EosMemberDTO;
import com.eos.service.dto.SoundCodeDTO;
import com.eos.utils.ApiResponse;
import com.eos.utils.AppUtils;
import com.eos.web.rest.errors.EmailAlreadyUsedException;
import com.eos.web.rest.errors.InternalServerErrorException;
import com.eos.web.rest.errors.InvalidPasswordException;
import com.eos.web.rest.errors.LoginAlreadyUsedException;
import com.eos.web.rest.util.PaginationUtil;
import com.eos.web.rest.vm.KeyAndPasswordVM;
import com.eos.web.rest.vm.ManagedUserVM;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing EosMember.
 */
@RestController
@RequestMapping("/api")
public class EosMemberResource {

	private final Logger log = LoggerFactory.getLogger(EosMemberResource.class);

	@SuppressWarnings("unused")
	private static final String ENTITY_NAME = "eosMember";

	private final UserService userService;

	private final UserRepository userRepository;

	private final EosMemberService eosMemberService;

	private final AppUtils appUtilsService;

	private final MailService mailService;

	private final SoundCodeService soundCodeService;

	public EosMemberResource(EosMemberService eosMemberService, UserService userService, UserRepository userRepository,
			MailService mailService, AppUtils appUtilsService, SoundCodeService soundCodeService) {
		this.eosMemberService = eosMemberService;
		this.userService = userService;
		this.userRepository = userRepository;
		this.mailService = mailService;
		this.appUtilsService = appUtilsService;
		this.soundCodeService = soundCodeService;
	}

	/**
	 * POST /eos-members : Create a new eosMember.
	 *
	 * @param eosMemberDTO
	 *            the eosMemberDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new eosMemberDTO, or with status 400 (Bad Request) if the
	 *         eosMember has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/admin/eos-members")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<ApiResponse<EosMemberDTO>> createEosMember(@Valid @RequestBody EosMemberDTO eosMemberDTO)
			throws URISyntaxException {
		log.debug("REST request to save EosMember : {}", eosMemberDTO);

		ApiResponse<EosMemberDTO> apiResponse = new ApiResponse<>();
		try {
			if (eosMemberDTO.getId() != null) {
				apiResponse.setStatus(false);
				apiResponse.setMessage("A new member cannot already have an ID");
				return new ResponseEntity<ApiResponse<EosMemberDTO>>(apiResponse, HttpStatus.BAD_REQUEST);
			}

			User user = eosMemberDTO.getUser();

			user = userService.trim(user);

			if (StringUtils.isEmpty(user.getFirstName()) || StringUtils.isEmpty(user.getLastName())
					|| StringUtils.isEmpty(user.getLogin())) {
				apiResponse.setStatus(false);
				apiResponse.setMessage("Please fill required fields");
				return new ResponseEntity<ApiResponse<EosMemberDTO>>(apiResponse, HttpStatus.BAD_REQUEST);

			}

			String licenseNo = appUtilsService.getLicenseno();
			String uid = appUtilsService.getSoundCodeUid();
			SoundCode sc = soundCodeService.findOneByLicenseNoAndUid(licenseNo, uid);
			while (sc != null) {
				licenseNo = appUtilsService.getLicenseno();
				uid = appUtilsService.getSoundCodeUid();
				sc = soundCodeService.findOneByLicenseNoAndUid(licenseNo, uid);
			}
			SoundCodeDTO soundDto = new SoundCodeDTO();
			soundDto.setLicenseKey(licenseNo);
			soundDto.setUid(uid);

			eosMemberDTO.setSoundCode(soundDto);
			user.setEmail(user.getLogin());
			userRepository.findOneByLogin(user.getLogin().toLowerCase()).ifPresent(u -> {
				throw new LoginAlreadyUsedException();
			});
			userRepository.findOneByEmailIgnoreCase(user.getEmail()).ifPresent(u -> {
				throw new EmailAlreadyUsedException();
			});
			ManagedUserVM managedUserVM = new ManagedUserVM(user.getId(), user.getLogin(), user.getPassword(),
					user.getFirstName(), user.getLastName(), user.getEmail(), user.getActivated(), user.getImageUrl(),
					user.getLangKey(), user.getCreatedBy(), user.getCreatedDate(), user.getLastModifiedBy(),
					user.getLastModifiedDate(),
					user.getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet()),
					eosMemberDTO.getFromDate(), eosMemberDTO.getToDate(), eosMemberDTO.getFloors(),
					eosMemberDTO.getSoundCode(), eosMemberDTO.getStartTime(), eosMemberDTO.getEndTime());
			user = userService.registerUser(managedUserVM);

			mailService.sendActivationEmail(user);
			eosMemberDTO = eosMemberService.findOne(user.getId());

			apiResponse.setData(eosMemberDTO);
			apiResponse.setStatus(true);
			return new ResponseEntity<ApiResponse<EosMemberDTO>>(apiResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			log.error(e.getMessage());
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			return new ResponseEntity<ApiResponse<EosMemberDTO>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * PUT /eos-members : Updates an existing eosMember.
	 *
	 * @param eosMemberDTO
	 *            the eosMemberDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         eosMemberDTO, or with status 400 (Bad Request) if the
	 *         eosMemberDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the eosMemberDTO couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/admin/eos-members")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<ApiResponse<EosMemberDTO>> updateEosMember(@Valid @RequestBody EosMemberDTO eosMemberDTO)
			throws URISyntaxException {
		log.debug("REST request to update EosMember : {}", eosMemberDTO);

		ApiResponse<EosMemberDTO> apiResponse = new ApiResponse<>();
		try {
			if (eosMemberDTO.getId() == null) {
				apiResponse.setStatus(false);
				apiResponse.setMessage("Member not registered");
			}

			User user = eosMemberDTO.getUser();

			user = userService.trim(user);

			if (StringUtils.isEmpty(user.getFirstName()) || StringUtils.isEmpty(user.getLastName())) {
				apiResponse.setStatus(false);
				apiResponse.setMessage("Please fill required fields");
				return new ResponseEntity<ApiResponse<EosMemberDTO>>(apiResponse, HttpStatus.BAD_REQUEST);

			}
			user.setEmail(user.getLogin());

			Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(user.getEmail());
			if (existingUser.isPresent() && (!existingUser.get().getId().equals(eosMemberDTO.getId()))) {
				throw new EmailAlreadyUsedException();
			}
			existingUser = userRepository.findOneByLogin(user.getLogin().toLowerCase());
			if (existingUser.isPresent() && (!existingUser.get().getId().equals(eosMemberDTO.getId()))) {
				throw new LoginAlreadyUsedException();
			}

			User user2 = userRepository.findOne(user.getId());

			if (!user2.getEmail().equals(user.getEmail())) {
				mailService.sendEmailChangedMail(user, user2.getEmail());
			}

			userService.updateUser(user);
			Optional<EosMemberDTO> eosMemberDTO2 = eosMemberService.updateMember(eosMemberDTO);
			apiResponse.setStatus(true);
			if (eosMemberDTO2.isPresent()) {
				apiResponse.setData(eosMemberDTO2.get());
			}
			return new ResponseEntity<ApiResponse<EosMemberDTO>>(apiResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			log.error(e.getMessage());
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			return new ResponseEntity<ApiResponse<EosMemberDTO>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * GET /eos-members : get all the eosMembers.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         eosMembers in body
	 */
	@GetMapping("/admin/eos-members")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<ApiResponse<Page<EosMemberDTO>>> getAllEosMembers(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of EosMembers");

		ApiResponse<Page<EosMemberDTO>> apiResponse = new ApiResponse<Page<EosMemberDTO>>();
		HttpHeaders headers = null;
		try {
			Page<EosMemberDTO> page = eosMemberService.findAll(pageable);
			headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/events");
			apiResponse.setData(page);
			apiResponse.setStatus(true);
			apiResponse.setMessage("List of events");
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<Page<EosMemberDTO>>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<ApiResponse<Page<EosMemberDTO>>>(apiResponse, headers, HttpStatus.OK);
	}

	/**
	 * GET /eos-members/:id : get the "id" eosMember.
	 *
	 * @param id
	 *            the id of the eosMemberDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         eosMemberDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/admin/eos-members/{id}")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ApiResponse<EosMemberDTO> getEosMember(@PathVariable Long id) {
		log.debug("REST request to get EosMember : {}", id);
		ApiResponse<EosMemberDTO> apiResponse = new ApiResponse<>();
		try {
			EosMemberDTO eosMemberDTO = eosMemberService.findOne(id);
			if (eosMemberDTO == null) {
				apiResponse.setStatus(true);
				apiResponse.setMessage("Member not found");
			}
			apiResponse.setData(eosMemberDTO);
			apiResponse.setStatus(true);
		} catch (Exception e) {
			log.error(e.getMessage());
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
		}
		return apiResponse;
	}

	/**
	 * DELETE /eos-members/:id : delete the "id" eosMember.
	 *
	 * @param id
	 *            the id of the eosMemberDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/admin/eos-members/{id}")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<ApiResponse<Void>> deleteEosMember(@PathVariable Long id) {
		log.debug("REST request to delete EosMember : {}", id);
		ApiResponse<Void> apiResponse = new ApiResponse<>();
		try {
			eosMemberService.delete(id);
			userService.delete(id);
			apiResponse.setStatus(true);
			apiResponse.setMessage("Member deleted successfully");
			return new ResponseEntity<ApiResponse<Void>>(apiResponse, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage());
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			return new ResponseEntity<ApiResponse<Void>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/eos-members/register")
	@Timed
	public ApiResponse<Void> registerMember(@RequestBody KeyAndPasswordVM keyAndPassword) {

		ApiResponse<Void> apiResponse = new ApiResponse<>();
		try {
			if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
				throw new InvalidPasswordException();
			}
			Optional<User> user = userService.memberRegistration(keyAndPassword.getNewPassword(),
					keyAndPassword.getKey());

			if (!user.isPresent()) {
				throw new InternalServerErrorException("No user was found for this activation key");
			}
			mailService.sendSuccessEmail(user.get());
			apiResponse.setStatus(true);
			apiResponse.setMessage("Member register successfully");
		} catch (Exception e) {
			log.error(e.getMessage());
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
		}
		return apiResponse;
	}

	private static boolean checkPasswordLength(String password) {
		return !StringUtils.isEmpty(password) && password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH
				&& password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
	}

}
