package com.eos.web.rest.vm;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Size;

import com.eos.domain.Floor;
import com.eos.service.dto.SoundCodeDTO;
import com.eos.service.dto.UserDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * View Model extending the UserDTO, which is meant to be used in the user
 * management UI.
 */
public class ManagedUserVM extends UserDTO {

	public static final int PASSWORD_MIN_LENGTH = 4;

	public static final int PASSWORD_MAX_LENGTH = 100;

	@JsonIgnore
	@Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
	private String password;

	private ZonedDateTime fromDate;

	private ZonedDateTime toDate;

	private Set<Floor> floors = new HashSet<>();

	private SoundCodeDTO soundCode;

	private ZonedDateTime startTime;
	private ZonedDateTime endTime;

	public ManagedUserVM() {
		// Empty constructor needed for Jackson.
	}

	public ManagedUserVM(Long id, String login, String password, String firstName, String lastName, String email,
			boolean activated, String imageUrl, String langKey, String createdBy, Instant createdDate,
			String lastModifiedBy, Instant lastModifiedDate, Set<String> authorities, ZonedDateTime fromDate,

			ZonedDateTime toDate, Set<Floor> floors, SoundCodeDTO soundCode, ZonedDateTime startTime, ZonedDateTime endTime) {

		super(id, login, firstName, lastName, email, activated, imageUrl, langKey, createdBy, createdDate,
				lastModifiedBy, lastModifiedDate, authorities);
		this.password = password;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.floors = floors;
		this.soundCode = soundCode;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public ZonedDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(ZonedDateTime startTime) {
		this.startTime = startTime;
	}

	public ZonedDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(ZonedDateTime endTime) {
		this.endTime = endTime;
	}

	public String getPassword() {
		return password;
	}

	public ZonedDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(ZonedDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public ZonedDateTime getToDate() {
		return toDate;
	}

	public void setToDate(ZonedDateTime toDate) {
		this.toDate = toDate;
	}

	public Set<Floor> getFloors() {
		return floors;
	}

	public void setFloors(Set<Floor> floors) {
		this.floors = floors;
	}

	public SoundCodeDTO getSoundCode() {
		return soundCode;
	}

	public void setSoundCode(SoundCodeDTO soundCode) {
		this.soundCode = soundCode;
	}

	@Override
	public String toString() {
		return "ManagedUserVM{" + "} " + super.toString();
	}
}
