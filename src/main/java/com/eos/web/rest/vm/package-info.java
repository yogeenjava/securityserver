/**
 * View Models used by Spring MVC REST controllers.
 */
package com.eos.web.rest.vm;
