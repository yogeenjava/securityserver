package com.eos.web.rest.vm;

public class EmailVm {

	private String email;

	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		return "EmailVm [email=" + email + "]";
	}
}
