package com.eos.web.rest;

import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.eos.config.ApplicationProperties;
import com.eos.security.jwt.JWTConfigurer;
import com.eos.security.jwt.TokenProvider;
import com.eos.service.UserService;
import com.eos.utils.ApiResponse;
import com.eos.web.rest.util.SoundCodeUtil;
import com.eos.web.rest.vm.LoginVM;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

	private final Logger log = LoggerFactory.getLogger(UserJWTController.class);

	private final TokenProvider tokenProvider;

	private final AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;

	@Autowired
	private SoundCodeUtil soundCodeUtilService;

	@Autowired
	private ApplicationProperties applicationProperties;

	public UserJWTController(TokenProvider tokenProvider, AuthenticationManager authenticationManager) {
		this.tokenProvider = tokenProvider;
		this.authenticationManager = authenticationManager;
	}

	@PostMapping("/authenticate")
	@Timed
	public ApiResponse<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM, HttpServletRequest request,
			HttpServletResponse response) throws UnknownHostException {

		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
				loginVM.getUsername(), loginVM.getPassword());
		ApiResponse<JWTToken> apiResponse = new ApiResponse<JWTToken>();
		try {
			Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
			String jwt = tokenProvider.createToken(authentication, rememberMe);
			response.addHeader(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
			// return ResponseEntity.ok(new JWTToken(jwt));

			apiResponse.setStatus(true);

//			log.info("generate sound");
			com.eos.domain.User principal = userService.getUserWithAuthorities();
//			soundCodeUtilService.generateSoundFile("member", principal.getId());

			String downloadLink = applicationProperties.getUrl() + "/api/sound/download?soundFor=member&id="
					+ principal.getId();
			apiResponse.setData(new JWTToken(jwt, downloadLink));
		} catch (AuthenticationException ae) {
			log.trace("Authentication exception trace: {}", ae);
			// return new
			// ResponseEntity<>(Collections.singletonMap("AuthenticationException",
			// ae.getLocalizedMessage()), HttpStatus.UNAUTHORIZED);
			apiResponse.setStatus(false);
			apiResponse.setMessage("Failed to sign in! Please check your credentials and try again.");
		}
		return apiResponse;
	}

	/**
	 * Object to return as body in JWT Authentication.
	 */
	static class JWTToken {

		private String idToken;

		private String downloadLink;

		JWTToken(String idToken) {
			this.idToken = idToken;
		}

		JWTToken(String idToken, String downloadLink) {
			this.idToken = idToken;
			this.downloadLink = downloadLink;
		}

		@JsonProperty("id_token")
		String getIdToken() {
			return idToken;
		}

		void setIdToken(String idToken) {
			this.idToken = idToken;
		}

		public String getDownloadLink() {
			return downloadLink;
		}

		public void setDownloadLink(String downloadLink) {
			this.downloadLink = downloadLink;
		}
	}
}
