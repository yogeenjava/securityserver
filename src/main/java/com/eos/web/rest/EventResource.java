package com.eos.web.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.codahale.metrics.annotation.Timed;
import com.eos.config.ApplicationProperties;
import com.eos.domain.SoundCode;
import com.eos.security.jwt.JWTConfigurer;
import com.eos.security.jwt.TokenProvider;
import com.eos.service.EventGuestCsvService;
import com.eos.service.EventService;
import com.eos.service.SoundCodeService;
import com.eos.service.dto.EventDTO;
import com.eos.service.dto.EventGuestCsvDTO;
import com.eos.service.dto.EventGuestDTO;
import com.eos.service.dto.SoundCodeDTO;
import com.eos.utils.ApiResponse;
import com.eos.utils.AppUtils;
import com.eos.web.rest.UserJWTController.JWTToken;
import com.eos.web.rest.errors.BadRequestAlertException;
import com.eos.web.rest.util.PaginationUtil;
import com.eos.web.rest.util.SoundCodeUtil;
import com.google.common.io.Files;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Event.
 */
@RestController
@RequestMapping("/api")
public class EventResource {

	private final Logger log = LoggerFactory.getLogger(EventResource.class);

	private static final String ENTITY_NAME = "event";

	private final EventService eventService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private EventGuestCsvService eventGuestCsvService;

	@Autowired
	private AppUtils appUtilsService;

	@Autowired
	private SoundCodeService soundCodeService;

	@Autowired
	private SoundCodeUtil soundCodeUtilService;

	@Autowired
	private ApplicationProperties applicationProperties;

	@Autowired
	private TokenProvider tokenProvider;

	public EventResource(EventService eventService) {
		this.eventService = eventService;
	}

	/**
	 * POST /events : Create a new event.
	 *
	 * @param eventDTO
	 *            the eventDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new eventDTO, or with status 400 (Bad Request) if the event has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/admin/events")
	@Timed
	public ResponseEntity<ApiResponse<EventDTO>> createEvent(@Valid @RequestBody EventDTO eventDTO)
			throws URISyntaxException {
		log.debug("REST request to save Event : {}", eventDTO);
		ApiResponse<EventDTO> apiResponse = new ApiResponse<EventDTO>();
		try {
			if (eventDTO.getId() != null) {
				throw new BadRequestAlertException("A new event cannot already have an ID", ENTITY_NAME, "idexists");
			}

			if (eventDTO.getFromDate().isBefore(ZonedDateTime.now().minusDays(1))
					|| eventDTO.getToDate().isBefore(ZonedDateTime.now())) {
				throw new Exception("Event date should be future date");
			}
			String licenseNo = appUtilsService.getLicenseno();
			String uid = appUtilsService.getSoundCodeUid();
			SoundCode sc = soundCodeService.findOneByLicenseNoAndUid(licenseNo, uid);
			while (sc != null) {
				licenseNo = appUtilsService.getLicenseno();
				uid = appUtilsService.getSoundCodeUid();
				sc = soundCodeService.findOneByLicenseNoAndUid(licenseNo, uid);
			}
			SoundCodeDTO soundDto = new SoundCodeDTO();
			soundDto.setLicenseKey(licenseNo);
			soundDto.setUid(uid);
			eventDTO.setSoundCode(soundDto);
			EventDTO result = eventService.save(eventDTO);
			apiResponse.setData(result);
			apiResponse.setStatus(true);
			return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse, HttpStatus.OK);
		}
	}

	@PutMapping("/admin/events")
	@Timed
	public ResponseEntity<ApiResponse<EventDTO>> updateEvent(@Valid @RequestBody EventDTO eventDTO)
			throws URISyntaxException {
		log.debug("REST request to save Event : {}", eventDTO);
		ApiResponse<EventDTO> apiResponse = new ApiResponse<EventDTO>();
		try {

			if (eventDTO.getToDate().isBefore(ZonedDateTime.now())) {
				throw new Exception("Event To date should be future date");
			}

			if (eventDTO.getFile() != null && eventDTO.getFileName() != null) {
				byte[] decodedBytes = Base64.getDecoder().decode(eventDTO.getFile());
				String decodedFile = new String(decodedBytes, "UTF16");

				String[] data = decodedFile.split(System.getProperty("line.separator"));
				List<String> header = new ArrayList<>();
				List<EventGuestDTO> list = new ArrayList<>();
				int i;
				for (i = 0; i < data.length; i++) {
					if (data[i] != null) {
						String[] split2 = data[i].split("\"");
						for (int j = 0; j < split2.length; j++) {
							header.add(split2[j].trim().toLowerCase());
						}
						break;
					}
				}

				log.info(header.toString());
				if (header.stream().filter(o -> o.trim().equals("email")).findFirst().isPresent()) {
					int index = header.indexOf("email");
					for (i = i + 1; i < data.length; i++) {
						if (data[i] != null) {
							String[] split2 = data[i].split("\"");
							for (int j = 0; j < split2.length; j++) {
								if (j == index) {
									EventGuestDTO guest = new EventGuestDTO();
									guest.setEmail(split2[j]);
									list.add(guest);
								}
							}
						}
					}
				} else {
					apiResponse.setStatus(false);
					apiResponse
							.setMessage("Event can not be updated as Guest emails are not available in the CSV file.");
					return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse, HttpStatus.BAD_REQUEST);
				}
				eventDTO.setEventGuest(list);
			}

			EventDTO result = eventService.update(eventDTO);
			apiResponse.setData(result);
			apiResponse.setStatus(true);
			return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse, HttpStatus.OK);
		}
	}

	/**
	 * PUT /events : Updates an existing event.
	 *
	 * @param eventDTO
	 *            the eventDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         eventDTO, or with status 400 (Bad Request) if the eventDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the eventDTO
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	// @SuppressWarnings("rawtypes")
	// @PostMapping("/admin/events/update")
	// @Timed
	// public ResponseEntity<ApiResponse<EventDTO>>
	// updateEvent(HttpServletRequest req) {
	// ApiResponse<EventDTO> apiResponse = new ApiResponse<EventDTO>();
	// try {
	// Long id = null;
	// MultipartHttpServletRequest multipartRequest =
	// (MultipartHttpServletRequest) req;
	// Set set = multipartRequest.getFileMap().entrySet();
	//
	// if (req.getParameter("id") != null) {
	// id = Long.parseLong(req.getParameter("id"));
	// String eventName = req.getParameter("eventName");
	// String description = req.getParameter("description");
	// String fromDate = req.getParameter("eventDate");
	// ZonedDateTime dateTime = null;
	// if (fromDate != null) {
	// dateTime = ZonedDateTime.parse(fromDate);
	// }
	// // String toDate = req.getParameter("toDate");
	// String webUrl = req.getParameter("webUrl");
	// EventDTO eventDto = new EventDTO();
	// eventDto.setId(id);
	// eventDto.setEventName(eventName);
	// eventDto.setDescription(description);
	// eventDto.setWebUrl(webUrl);
	// eventDto.setLicenceKey("test");
	// eventDto.setFromDate(dateTime);
	// Iterator i = set.iterator();
	// while (i.hasNext()) {
	// Map.Entry me = (Map.Entry) i.next();
	// MultipartFile file = (MultipartFile) me.getValue();
	// if (file.getSize() > 0) {
	// String fileName = file.getOriginalFilename();
	// String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
	// log.info("ext = " + ext);
	// if (ext.equals("csv")) {
	// List<EventGuestDTO> list = csvReader.csvToBean(file.getInputStream());
	// eventDto.setEventGuest(list);
	// } else {
	// apiResponse.setStatus(false);
	// apiResponse.setMessage("File format not valid. Please upload csv file.");
	// return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse,
	// HttpStatus.BAD_REQUEST);
	// }
	// }
	// }
	// if (eventDto.getFromDate().isBefore(ZonedDateTime.now().minusDays(1))) {
	// throw new Exception("Event date should be future date");
	// }
	// EventDTO result = eventService.update(eventDto);
	// apiResponse.setData(result);
	// apiResponse.setStatus(true);
	// return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse,
	// HttpStatus.CREATED);
	// } else {
	// apiResponse.setStatus(false);
	// apiResponse.setMessage("id is required field.");
	// return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse,
	// HttpStatus.BAD_REQUEST);
	// }
	// } catch (Exception e) {
	// apiResponse.setStatus(false);
	// apiResponse.setMessage(e.getMessage());
	// log.error(e.getMessage(), e);
	// return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse,
	// HttpStatus.INTERNAL_SERVER_ERROR);
	// }
	// }

	/**
	 * GET /events : get all the events.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of events in
	 *         body
	 */
	@GetMapping("/events")
	@Timed
	public ResponseEntity<ApiResponse<Page<EventDTO>>> getAllEvents(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Events");
		ApiResponse<Page<EventDTO>> apiResponse = new ApiResponse<Page<EventDTO>>();
		HttpHeaders headers = null;
		try {
			Page<EventDTO> page = eventService.findAll(pageable);
			headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/events");
			apiResponse.setData(page);
			apiResponse.setStatus(true);
			apiResponse.setMessage("List of events");
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<Page<EventDTO>>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ApiResponse<Page<EventDTO>>>(apiResponse, headers, HttpStatus.OK);
	}

	@GetMapping("/admin/all-events")
	@Timed
	public ResponseEntity<ApiResponse<Page<EventDTO>>> getAllEvent(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Events");
		ApiResponse<Page<EventDTO>> apiResponse = new ApiResponse<Page<EventDTO>>();
		HttpHeaders headers = null;
		try {
			Page<EventDTO> page = eventService.findAllEvent(pageable);
			headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/events");
			apiResponse.setData(page);
			apiResponse.setStatus(true);
			apiResponse.setMessage("List of events");
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<Page<EventDTO>>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ApiResponse<Page<EventDTO>>>(apiResponse, headers, HttpStatus.OK);
	}

	/**
	 * GET /events/:id : get the "id" event.
	 *
	 * @param id
	 *            the id of the eventDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         eventDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/admin/events/{id}")
	@Timed
	public ResponseEntity<ApiResponse<EventDTO>> getEvent(@PathVariable Long id) {
		log.debug("REST request to get Event : {}", id);
		ApiResponse<EventDTO> apiResponse = new ApiResponse<EventDTO>();
		try {
			EventDTO eventDTO = eventService.findOne(id);
			apiResponse.setData(eventDTO);
			apiResponse.setStatus(true);
			apiResponse.setMessage("event details");
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<EventDTO>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(apiResponse));
	}

	@PostMapping("/events/valid_guest")
	@Timed
	public ResponseEntity<ApiResponse<Map<String, Object>>> validateGuest(@RequestBody EventGuestDTO eventGuestDto,
			HttpServletRequest req) {
		Map<String, Object> map = new HashMap<>();
		ApiResponse<Map<String, Object>> apiResponse = new ApiResponse<Map<String, Object>>();
		try {
			Boolean isValidGuest = eventService.isValidEventGuest(eventGuestDto);
			apiResponse.setStatus(isValidGuest);
			if (isValidGuest) {
				apiResponse.setMessage("Guest is valid for event");
				// soundCodeUtilService.generateSoundFile("guest",
				// eventGuestDto.getId());

				String ip = req.getRemoteHost();
				if (ip.equalsIgnoreCase("0:0:0:0:0:0:0:1")) {
					InetAddress inetAddress = InetAddress.getLocalHost();
					String ipAddress = inetAddress.getHostAddress();
					ip = ipAddress;
				}

				// UsernamePasswordAuthenticationToken authenticationToken = new
				// UsernamePasswordAuthenticationToken("yogeen.loriya@peaas.co",
				// "12345678");
				// // ApiResponse<JWTToken> apiResponse = new
				// // ApiResponse<JWTToken>();
				// Authentication authentication =
				// this.authenticationManager.authenticate(authenticationToken);
				// SecurityContextHolder.getContext().setAuthentication(authentication);
				String jwt = tokenProvider.createTokenForGuest(eventGuestDto.getId(), eventGuestDto.getEmail());
				// response.addHeader(JWTConfigurer.AUTHORIZATION_HEADER,
				// "Bearer " + jwt);
				// return ResponseEntity.ok(new JWTToken(jwt));

				log.info("ip = = >" + ip);
				String downloadLink = applicationProperties.getUrl() + "/api/sound/download?soundFor=guest&id="
						+ eventGuestDto.getId();
				map.put("downloadLink", downloadLink);
				map.put("id_token", jwt);
				apiResponse.setData(map);
				return new ResponseEntity<ApiResponse<Map<String, Object>>>(apiResponse, HttpStatus.OK);
			} else {
				apiResponse.setMessage(
						"The entry pass (keys) will be available by the beginning of the event if not available at the moment.");
				return new ResponseEntity<ApiResponse<Map<String, Object>>>(apiResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<Map<String, Object>>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * DELETE /events/:id : delete the "id" event.
	 *
	 * @param id
	 *            the id of the eventDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/admin/events/{id}")
	@Timed
	public ResponseEntity<ApiResponse<Void>> deleteEvent(@PathVariable Long id) {
		log.debug("REST request to delete Event : {}", id);
		ApiResponse<Void> apiResponse = new ApiResponse<Void>();
		try {
			eventService.delete(id);
			apiResponse.setStatus(true);
			apiResponse.setMessage("event deleted successfully");
			return new ResponseEntity<ApiResponse<Void>>(apiResponse, HttpStatus.OK);
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<Void>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "admin/csv/download/{id}", method = RequestMethod.GET)
	public void downLoadEventGuestCSV(@PathVariable Long id, @RequestParam("Authorization") String authorization,
			HttpServletResponse response) throws IOException {

		try {
			Authentication authentication = tokenProvider.getAuthentication(authorization);

			if (authentication.isAuthenticated()) {
				EventGuestCsvDTO eventGuestCsvDTO = eventGuestCsvService.findOne(id);
				if (eventGuestCsvDTO != null) {

					String suffix = ".csv";
					String prefix = eventGuestCsvDTO.getFileName();
					File file = File.createTempFile(prefix, suffix);
					byte[] dataBytes = Base64.getDecoder().decode(eventGuestCsvDTO.getFileData().getBytes());

					Files.write(dataBytes, file);
					InputStream inputStream = null;

					try {
						response.setContentType("text/csv");
						// response.setHeader("Content-Disposition",
						// String.format("inline; filename=\"" + file.getName()
						// + "\""));
						response.setContentLength((int) file.length());
						inputStream = new BufferedInputStream(new FileInputStream(file));
						FileCopyUtils.copy(inputStream, response.getOutputStream());
					} catch (Exception e) {
						log.error(e.getMessage());
					} finally {
						try {
							file.deleteOnExit();
							inputStream.close();
						} catch (Exception e) {
							log.error(e.getMessage());
						}
					}
				}

			}
		} catch (Exception e) {
			log.error(e.toString());
		}
	}

}
