package com.eos.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.eos.service.FloorService;
import com.eos.service.dto.FloorDTO;
import com.eos.utils.ApiResponse;

/**
 * REST controller for managing Floor.
 */
@RestController
@RequestMapping("/api/admin")
public class FloorResource {

	private final Logger log = LoggerFactory.getLogger(FloorResource.class);

	@SuppressWarnings("unused")
	private static final String ENTITY_NAME = "floor";

	private final FloorService floorService;

	public FloorResource(FloorService floorService) {
		this.floorService = floorService;
	}

	/**
	 * POST /floors : Create a new floor.
	 *
	 * @param floorDTO
	 *            the floorDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         floorDTO, or with status 400 (Bad Request) if the floor has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/floors")
	@Timed
	public ResponseEntity<ApiResponse<FloorDTO>> createFloor(@RequestBody FloorDTO floorDTO) throws URISyntaxException {
		log.debug("REST request to save Floor : {}", floorDTO);
		ApiResponse<FloorDTO> apiResponse = new ApiResponse<FloorDTO>();
		try {
			if (floorDTO.getId() != null) {
				apiResponse.setStatus(false);
				apiResponse.setMessage("A new floor cannot already have an ID");
				return new ResponseEntity<ApiResponse<FloorDTO>>(apiResponse, HttpStatus.BAD_REQUEST);
			}
			FloorDTO floor = floorService.findOneWithNo(floorDTO.getFloorNo());
			if (floor != null) {
				apiResponse.setStatus(false);
				apiResponse.setMessage("Floor already exists");
				return new ResponseEntity<ApiResponse<FloorDTO>>(apiResponse, HttpStatus.BAD_REQUEST);
			}
			FloorDTO result = floorService.save(floorDTO);
			apiResponse.setData(result);
			apiResponse.setStatus(true);
			return new ResponseEntity<ApiResponse<FloorDTO>>(apiResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<FloorDTO>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * PUT /floors : Updates an existing floor.
	 *
	 * @param floorDTO
	 *            the floorDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         floorDTO, or with status 400 (Bad Request) if the floorDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the floorDTO
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/floors")
	@Timed
	public ResponseEntity<ApiResponse<FloorDTO>> updateFloor(@RequestBody FloorDTO floorDTO) throws URISyntaxException {
		log.debug("REST request to update Floor : {}", floorDTO);
		ApiResponse<FloorDTO> apiResponse = new ApiResponse<FloorDTO>();
		try {
			if (floorDTO.getId() == null) {
				return createFloor(floorDTO);
			}
			FloorDTO result = floorService.save(floorDTO);
			apiResponse.setData(result);
			apiResponse.setStatus(true);
			return new ResponseEntity<ApiResponse<FloorDTO>>(apiResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<FloorDTO>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * GET /floors : get all the floors.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of floors in
	 *         body
	 */
	@GetMapping("/floors")
	@Timed
	public ApiResponse<List<FloorDTO>> getAllFloors() {
		log.debug("REST request to get all Floors");
		ApiResponse<List<FloorDTO>> apiResponse = new ApiResponse<List<FloorDTO>>();
		try {
			List<FloorDTO> listFloor = floorService.findAll();
			if (listFloor.size() > 0) {
				apiResponse.setData(listFloor);
				apiResponse.setStatus(true);
				apiResponse.setMessage("List of floor");
			} else {
				apiResponse.setStatus(false);
				apiResponse.setMessage("Floors not found");
			}
		} catch (Exception e) {
			apiResponse.setStatus(false);
			log.error(e.getMessage(), e);
			apiResponse.setMessage(e.getMessage());
		}
		return apiResponse;
	}

	/**
	 * GET /floors/:id : get the "id" floor.
	 *
	 * @param id
	 *            the id of the floorDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the floorDTO,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/floors/{id}")
	@Timed
	public ApiResponse<FloorDTO> getFloor(@PathVariable Long id) {
		log.debug("REST request to get all Floors");
		ApiResponse<FloorDTO> apiResponse = new ApiResponse<FloorDTO>();
		try {
			FloorDTO floorDTO = floorService.findOne(id);
			if (floorDTO != null) {
				apiResponse.setData(floorDTO);
				apiResponse.setStatus(true);
			} else {
				apiResponse.setStatus(false);
				apiResponse.setMessage("Floor not found");
			}
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
		}
		return apiResponse;
	}

	/**
	 * DELETE /floors/:id : delete the "id" floor.
	 *
	 * @param id
	 *            the id of the floorDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/floors/{id}")
	@Timed
	public ResponseEntity<ApiResponse<Void>> deleteFloor(@PathVariable Long id) {
		log.debug("REST request to delete Floor : {}", id);
		ApiResponse<Void> apiResponse = new ApiResponse<Void>();
		try {
			Boolean status = floorService.delete(id);
			if (status) {
				apiResponse.setStatus(true);
				apiResponse.setMessage("Floor deleted");
				return new ResponseEntity<ApiResponse<Void>>(apiResponse, HttpStatus.OK);
			} else {
				apiResponse.setStatus(false);
				apiResponse.setMessage("Floor not found");
				return new ResponseEntity<ApiResponse<Void>>(apiResponse, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<Void>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
