
package com.eos.web.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.URLConnection;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eos.config.ApplicationProperties;
import com.eos.domain.SoundCode;
import com.eos.security.jwt.TokenProvider;
import com.eos.service.EosMemberService;
import com.eos.service.EventService;
import com.eos.service.SoundCodeService;
import com.eos.service.dto.EosMemberDTO;
import com.eos.service.dto.EventDTO;
import com.eos.service.dto.SoundCodeDTO;
import com.eos.utils.ApiResponse;
import com.eos.utils.DateTimeUtil;
import com.eos.web.rest.util.SoundCodeUtil;

import net.spy.memcached.MemcachedClient;

@RestController
@RequestMapping("/api")
public class SoundCodeResounce {

	private final Logger log = LoggerFactory.getLogger(SoundCodeResounce.class);

	@Autowired
	private SoundCodeService soundCodeService;

	@Autowired
	private EventService eventService;

	@Autowired
	private EosMemberService eosMemberservice;

	@Autowired
	private ApplicationProperties appProperties;

	@Autowired
	private DateTimeUtil dateTimeUtil;

	@Autowired
	private SoundCodeUtil soundCodeUtilService;

	@Autowired
	private TokenProvider tokenProvider;

	@RequestMapping(value = "sound/download", method = RequestMethod.GET)
	public void downLoadEventSound(@RequestParam("soundFor") String soundFor, @RequestParam("id") Long id,
			@RequestHeader("Authorization") String token, HttpServletResponse response) {

		try {
			if (tokenProvider.validateToken(token)) {
				String soundPath = appProperties.getSoundUploadPath();
				if (soundFor.equals("member")) {
					soundPath += "/members/" + id + ".wav";
					log.info("soundPath = " + soundPath);
					this.sendFile(response, soundPath, id, "member");
				} else if (soundFor.equals("guest")) {
					EventDTO eventDTO = eventService.findOne(id);
					if (eventDTO != null) {
						soundPath += "/events/" + id + ".wav";
						this.sendFile(response, soundPath, id, "guest");
					}
				} else {

				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	private void sendFile(HttpServletResponse response, String filePath, Long id, String soundFor) {
		InputStream inputStream = null;
		try {
			log.info(filePath);

			if (getSoundFromCache(filePath) != null) {
				FileCopyUtils.copy(getSoundFromCache(filePath), response.getOutputStream());
			} else {
				if ("member".equals(soundFor)) {
					soundCodeUtilService.generateSoundFile("member", id);
				} else if ("guest".equals(soundFor)) {
					soundCodeUtilService.generateSoundFile("guest", id);
				} else {

				}
				File file = new File(filePath);
				String mimeType = URLConnection.guessContentTypeFromName(file.getName());
				if (mimeType == null) {
					log.warn("mimetype is not detectable, will take default");
				}
				log.info("mimetype : " + mimeType);
				response.setContentType(mimeType);
				response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));
				response.setContentLength((int) file.length());
				inputStream = new BufferedInputStream(new FileInputStream(file));
				FileCopyUtils.copy(getSoundFromCache(filePath), response.getOutputStream());
				inputStream.close();
				soundCodeUtilService.deleteSoundFile(filePath);
			}

		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@RequestMapping(value = "sound/isValidSound", method = RequestMethod.POST)
	public ResponseEntity<ApiResponse<String>> isValidSound(@RequestBody SoundCodeDTO soundDto,
			HttpServletRequest request) {
		ApiResponse<String> apiResponse = new ApiResponse<>();
		try {

			String header = request.getHeader("Authorization");

			if (isUserAuthenticated(header)) {
				log.info(soundDto.getLicenseKey());
				SoundCode sc = soundCodeService.findOneByLicenseNoAndUid(soundDto.getLicenseKey(), soundDto.getUid());
				if (sc != null) {
					EosMemberDTO eosMemberDTO = eosMemberservice.findOneBySoundCodeAndFloor(sc, soundDto.getFloorNo());
					if (eosMemberDTO != null) {
						if (!eosMemberDTO.getUser().getActivated()
								|| eosMemberDTO.getFromDate().isAfter(ZonedDateTime.now())) {
							apiResponse.setStatus(false);
							apiResponse.setMessage("The User has not been activated yet.");
							apiResponse.setStatusCode(8002);
						} else if (eosMemberDTO.getToDate() != null
								&& eosMemberDTO.getToDate().isBefore(ZonedDateTime.now())) {
							apiResponse.setStatus(false);
							apiResponse.setMessage("The User has expired.");
							apiResponse.setStatusCode(8003);
						} else if (eosMemberDTO.getStartTime() != null && eosMemberDTO.getEndTime() != null
								&& !dateTimeUtil.isTimeBetween(eosMemberDTO.getStartTime().toLocalTime(),
										eosMemberDTO.getEndTime().toLocalTime())) {

							DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("hh:mm a");
							apiResponse.setStatus(false);
							apiResponse.setMessage("The user has the permission to access this floor from "
									+ eosMemberDTO.getStartTime().toLocalTime().format(dateTimeFormatter) + " to "
									+ eosMemberDTO.getEndTime().toLocalTime().format(dateTimeFormatter) + " only.");
							apiResponse.setStatusCode(8008);
						} else {
							apiResponse.setStatus(true);
							apiResponse.setMessage("Welcome " + eosMemberDTO.getUser().getFirstName() + " "
									+ eosMemberDTO.getUser().getLastName());
						}
					} else {
						EventDTO eventDTO = eventService.findOneBySoundCodeAndFloor(sc, soundDto.getFloorNo());
						if (eventDTO != null) {
							if (eventDTO.getFromDate().isAfter(ZonedDateTime.now())) {
								apiResponse.setStatus(false);
								apiResponse.setMessage("The Event has not started yet.");
								apiResponse.setStatusCode(8005);
							} else if (eventDTO.getToDate().isBefore(ZonedDateTime.now())) {
								apiResponse.setStatus(false);
								apiResponse.setMessage("The Event has ended.");
								apiResponse.setStatusCode(8006);
							} else if (eventDTO.getToDate() != null
									&& !dateTimeUtil.isTimeBetween(eventDTO.getFromDate().toLocalTime(),
											eventDTO.getToDate().toLocalTime())) {
								apiResponse.setStatus(false);
								DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("hh:mm a");
								apiResponse.setMessage("The user has the permission to access this floor from "
										+ eventDTO.getFromDate().toLocalTime().format(dateTimeFormatter) + " to "
										+ eventDTO.getToDate().toLocalTime().format(dateTimeFormatter) + " only.");
								apiResponse.setStatusCode(8008);
							} else {
								apiResponse.setStatus(true);
								apiResponse.setMessage("Welcome Guest");
							}

						} else {
							eosMemberDTO = eosMemberservice.findOneBySoundCode(sc);
							if (eosMemberDTO != null) {
								apiResponse.setStatus(false);
								apiResponse.setMessage("The User does not have permission to access this floor.");
								apiResponse.setStatusCode(8004);
							} else {
								eventDTO = eventService.findOneBySoundCode(sc);
								if (eventDTO != null) {
									apiResponse.setStatus(false);
									apiResponse.setMessage("For this event, access to this floor is prohibited.");
									apiResponse.setStatusCode(8007);
								} else {
									throw new Exception("The user or event does not exist.");
								}
							}
						}
					}
				} else {
					apiResponse.setStatus(false);
					apiResponse.setMessage("The user or event does not exist.");
					apiResponse.setStatusCode(8001);
				}
			} else {
				apiResponse.setStatus(false);
				apiResponse.setMessage("Unauthorized access to server");
				return new ResponseEntity<ApiResponse<String>>(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (

		Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage(e.getMessage());
			log.error(e.getMessage(), e);
			return new ResponseEntity<ApiResponse<String>>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ApiResponse<String>>(apiResponse, HttpStatus.OK);
	}

	private byte[] getSoundFromCache(String soundFilePath) {

		// File file = new File(soundFilePath);
		MemcachedClient c;
		try {

			c = new MemcachedClient(new InetSocketAddress("edgeof.4vkjsx.cfg.apne1.cache.amazonaws.com", 11211));

			log.info(" Memched Object >>>>" + c.get(soundFilePath));
			byte[] bytesArray = (byte[]) c.get(soundFilePath);
			return bytesArray;
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return null;
	}

	@SuppressWarnings("unused")
	private void validateSoundFile(byte[] orginalByteArray, byte[] cacheByteArray) {

		if (Arrays.equals(orginalByteArray, cacheByteArray)) {
			log.info("Array Matched");
		} else {
			log.info("Find some other way");
		}

	}

	private boolean isUserAuthenticated(String authString) {

		String decodedAuth = "";
		// We need to extract data before decoding it back to original string
		String[] authParts = authString.trim().split("\\s+");
		String authInfo = authParts[1];
		String token = appProperties.getAuthenticationToken();
		if (token.equals(authInfo.trim())) {
			return true;
		} else {
			return false;
		}
	}

}
