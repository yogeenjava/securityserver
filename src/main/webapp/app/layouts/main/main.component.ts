import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiLanguageHelper,LoaderService,LoaderState } from '../../shared';

@Component({
    selector: 'jhi-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.css'],
})
export class JhiMainComponent implements OnInit {
    private isLoader: boolean;
    private subscription: Subscription;
    constructor(
        private jhiLanguageHelper: JhiLanguageHelper,
        private router: Router,
        private loaderService: LoaderService
        
    ) {}

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title: string = (routeSnapshot.data && routeSnapshot.data['pageTitle']) ? routeSnapshot.data['pageTitle'] : 'eosApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }

    ngOnInit() {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.jhiLanguageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
            }
        });
        this.subscription = this.loaderService.loaderState
            .subscribe((state: LoaderState) => {
            this.isLoader = state.show;
        });
    }
}
