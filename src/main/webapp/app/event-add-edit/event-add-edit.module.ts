import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EVENT_ADD_ROUTE,EVENT_EDIT_ROUTE , EventAddEditComponent } from './';
import { EosSharedModule } from '../shared';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';


@NgModule({
    imports: [
        RouterModule.forRoot([ EVENT_ADD_ROUTE, EVENT_EDIT_ROUTE], { useHash: true }),
        EosSharedModule,
        DatepickerModule.forRoot(),
        TimepickerModule.forRoot()
    ],
    declarations: [
        EventAddEditComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosEventAddEditModule {}
