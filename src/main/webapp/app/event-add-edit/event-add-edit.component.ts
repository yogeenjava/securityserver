import { Component, ElementRef, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { JhiAlertService, JhiEventManager, JhiParseLinks, JhiDataUtils } from 'ng-jhipster';
import { ResponseWrapper, ITEMS_PER_PAGE, LoaderService } from '../shared';
import { DomSanitizer } from '@angular/platform-browser';
import { Principal, ConfirmModalComponent, ConfirmDialodModel, ConfirmDialogService } from '../shared';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService, Event } from '../entities/event';
import { FloorService, Floor } from '../entities/floor';
// import { EventGuestCsvService} from '../entities/event-guest-csv';
import { Subscription } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { SERVER_API_URL } from '../app.constants';


@Component({
    selector: 'app-event-add-edit',
    templateUrl: './event-add-edit.component.html',
    styleUrls: [
        'event-add-edit.component.css'
    ]
})
export class EventAddEditComponent implements OnInit {
    private resourceUrlcsv = SERVER_API_URL + 'api/admin/csv/download';
    private subscription: Subscription;
    event_id: any;
    floors: Floor[];
    isfloor: boolean;
    selectefloors = [];
    isEventRes: boolean;
    event: Event;
    showFromDatePicker: boolean;
    showToDatePicker: boolean;
    showStartTimePicker: any;
    showEndTimePicker: any;
    confirmmodel: ConfirmDialodModel;
    isFormSubmited: boolean;
    showDatePicker: boolean;
    date: any;
    endDate: any;
    starttime: any;
    endtime: any;
    eventSubscriber: Subscription;
    deleteCSVflag: number = 0;
    urlPattern: string = "^(http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$";
    // pattern:string = "^.+\.(xlsx|xls|csv)$";
    private user: Account;
    @ViewChild('eventCSVFile') eventCSVFile: ElementRef;

    constructor(
        private eventManager: JhiEventManager,
        private alertService: JhiAlertService,
        private confirmservice: ConfirmDialogService,
        private parseLinks: JhiParseLinks,
        private _sanitizer: DomSanitizer,
        private principal: Principal,
        private router: Router,
        private eventService: EventService,
        private dataUtils: JhiDataUtils,
        private route: ActivatedRoute,
        private elem: ElementRef,
        // private eventGuestcsvservice:EventGuestCsvService,
        private modalService: NgbModal,
        private loaderService: LoaderService,
        private floorservice: FloorService,
        private localStorage: LocalStorageService,
        private sessionStorage: SessionStorageService

    ) {
        // this.isEventRes = false;
        this.event = {};
        this.isFormSubmited = false;
        this.showFromDatePicker = false;
        this.showToDatePicker = false;
        this.showStartTimePicker = false;
        this.showEndTimePicker = false;
        this.confirmmodel = {};
        this.floors = [];
        this.isfloor = false;
        this.event.eventFloors = [];
        this.selectefloors = [];
    }
    // releaseAllPicker(event) {
    //     if(event) {
    //         event.stopPropagation();
    //     }
    //     this.showFromDatePicker = false;
    //     this.showToDatePicker = false;
    //     this.showStartTimePicker = false;
    //     this.showEndTimePicker = false;
    // }
    ngOnInit() {
        this.eventSubscriber = this.eventManager.subscribe('releaseAllPicker', (response) => {
            if (event) {
                event.stopPropagation();
            }
            this.showFromDatePicker = false;
            this.showToDatePicker = false;
            this.showStartTimePicker = false;
            this.showEndTimePicker = false;
        });
        this.loaderService.show();
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.event_id = params['id'];
                this.principal.identity().then((account) => {
                    this.user = account;

                });
            }
        });
        this.isEventRes = true;
        this.getFloor();
        this.loaderService.hide();
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }
    getFloor() {
        this.floors = [];
        this.loaderService.show();
        this.floorservice.query().subscribe(
            (res: ResponseWrapper) => {

                let data: any = res.json;
                for (let i = 0; i < data.length; i++) {
                    data[i].checked = false;
                    this.floors.push(data[i]);
                }
                let isAdmin = this.principal.hasAnyAuthorityDirect(["ROLE_ADMIN"]);
                if (isAdmin && (this.event_id)) {
                    this.getEvent();
                    this.loaderService.hide();
                } else {
                    this.loaderService.hide();
                    // this.router.navigate(['/login']);
                }
                if (!this.event.fromDate) {
                    this.starttime = new Date();
                    this.starttime.setHours(0o0);
                    this.starttime.setMinutes(0o0);
                }
                if (!this.event.toDate) {
                    this.endtime = new Date();
                    this.endtime.setHours(23);
                    this.endtime.setMinutes(59);
                }
                this.loaderService.hide();
            }, (res: ResponseWrapper) => {
                if(res.status == 400){
                    this.loaderService.hide();
                    this.redirectErrorPage();
                }
                this.isfloor = true;
                let error: any = res.json;
                this.alertService.error(error, null, null);
                this.loaderService.hide();
            }
        );
    }

    getEvent() {
        this.loaderService.show();
        this.eventService.find(this.event_id).subscribe((event) => {
            this.loaderService.hide();
            if (event.eventName === null) {
                this.alertService.error("no event found", null, null);
            }
            else {
                this.event = event;
                for (let i = 0; i < this.floors.length; i++) {
                    let data: any = this.floors[i];

                    for (let j = 0; j < this.event.eventFloors.length; j++) {
                        if (this.floors[i].id === this.event.eventFloors[j].id) {
                            data.checked = true;
                        }
                    }
                    this.floors[i] = data;
                }
                this.event.fromDate = event.fromDate;
                this.event.toDate = event.toDate;
                this.starttime = event.fromDate;
                this.endtime = event.toDate;
                this.isEventRes = true;
            }
        }, (err: any) => {
            
        }
        );
    }
    isFloorValid() {
        let data: any = this.floors;
        let count: number = 0;
        for (let i = 0; i < data.length; i++) {
            if (data[i].checked == true) {
                count++;
            }
        }
        if (count == 0) {
            return false;
        } else {
            return true;
        }
    }
    setFromDate() {
        if (this.event.fromDate && this.starttime) {
            this.date = new Date(this.event.fromDate);
            let start_time = new Date(this.starttime);
            this.event.fromDate.setHours(start_time.getHours());
            this.event.fromDate.setMinutes(start_time.getMinutes());
        } else {
            this.event.fromDate.setMinutes(0o0);
            this.event.fromDate.setHours(0o0);
        }
    }
    setEndDate() {
        if (this.event.toDate && this.endtime) {
            let end_time = new Date(this.endtime);
            this.event.toDate.setHours(end_time.getHours());
            this.event.toDate.setMinutes(end_time.getMinutes());
        } else if (this.event.toDate && !this.endtime) {
            this.event.toDate.setMinutes(59);
            this.event.toDate.setHours(23);
        } else {
        }
    }
    save(form: any) {
        this.isFormSubmited = true;
        if (!this.isFloorValid()) { return; }
        if (!this.isDateValid()) { return; }
        if (!this.isValidDate() && !this.event_id) { return; }
        if (!this.isValidToDate() && this.event_id) { return; }
        if (!form.valid) { return; }
        // if(!this.isSelectedFromDate()){return;}
        if (!this.isDateTimeValid()) { return; }
        this.setFromDate();
        this.setEndDate();

        let data: any = this.floors;
        for (let i = 0; i < data.length; i++) {
            if (data[i].checked == true) {
                this.selectefloors.push(this.floors[i]);
            }
        }
        this.event.eventFloors = this.selectefloors;
        if (!this.event.id) {
            this.confirmmodel.name = this.event.eventName;
            this.confirmmodel.page = "an Event";
            this.confirmservice.setData(this.confirmmodel);
            const modalRef = this.modalService.open(ConfirmModalComponent, { size: 'lg', backdrop: 'static' });
            modalRef.result.then(() => {
            }, (reason) => {
                if (reason === true) {
                    this.loaderService.show();
                    this.eventService.create(this.event).subscribe(
                        (res: Event) => {
                            this.isFormSubmited = false;
                            if (res.status == true) {
                                this.loaderService.hide();
                                this.router.navigate(['/events']);
                                this.alertService.success("global.messages.eventadded.ewventaddsuccess", null, null);
                            }
                            else {
                                this.loaderService.hide();
                                if (res.message) {
                                    this.alertService.error("error.custom", { msg: res.message });
                                } else {
                                    this.alertService.error("error.400", null, null);
                                }
                            }
                        },
                        (err: Response) => {
                            this.loaderService.hide();
                            this.alertService.error("error.internalServerError", null, null);
                        }
                    );
                }
            });
        }
        else {
            this.loaderService.show();
            if (this.event_id) {
                this.event.id = this.event_id;
                let eventData = Object.assign({}, this.event);
                if (this.deleteCSVflag != 0) {
                    this.eventService.deletecsv(this.event_id);
                    this.deleteCSVflag = 1;
                }
                this.eventService.update(eventData).subscribe(
                    (res: Event) => {
                        if (res.status == true) {
                            this.loaderService.hide();
                            this.alertService.success("global.messages.eventupdate.eventupdatesuccess");
                            this.isFormSubmited = false;
                            this.router.navigate(['/events']);
                        }
                        else {
                            this.loaderService.hide();
                            if (res.message) {
                                this.alertService.error("error.custom", { msg: res.message });
                            } else {
                                this.alertService.error("error.400", null, null);
                            }
                        }
                    }, (res: any) => {
                        this.loaderService.hide();
                        if (res._body) {
                            let body = JSON.parse(res._body);
                            if (body.message) {
                                this.alertService.error("error.custom", { msg: body.message });
                            } else {
                                this.alertService.error("error.internalServerError", null, null);
                            }
                        } else {
                            this.alertService.error("error.internalServerError", null, null);
                        }
                    }
                );
            }
        }
    }
    setFileData(ev, field, isFile) {
        if (ev && ev.target.files && ev.target.files[0]) {
            const file = ev.target.files[0];
            if (!/^.+\.(csv)$/.test(file.name)) {
                this.alertService.error("global.messages.error.csvvalid", null, null);
                ev.target.value = "";
                return;
            }
            else {
                this.dataUtils.toBase64(file, (base64Data) => {
                    if (this.byteSize(base64Data)) {
                        let size = parseInt(this.byteSize(base64Data).replace("bytes", '').replace(/ /g, ''));
                        if (size > 10485760) {
                            this.alertService.error("global.messages.file.size", null, null);
                            return;
                        }
                    }
                    if (field == 'file') {
                        this.event.file = base64Data;
                        this.event.filetype = file.type;
                        this.event.fileName = file.name;
                    } else {

                    }
                });
            }
        }
    }
    isDateTimeValid() {
        if (this.event.fromDate && this.starttime
            && this.event.toDate && this.event) {

            this.date = new Date(this.event.fromDate);
            let start_time = new Date(this.starttime);
            this.date.setHours(start_time.getHours());
            this.date.setMinutes(start_time.getMinutes());

            this.endDate = new Date(this.event.toDate);
            let end_time = new Date(this.endtime);
            this.endDate.setHours(end_time.getHours());
            this.endDate.setMinutes(end_time.getMinutes());
            if (this.endDate <= this.date) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    cancel() {
        this.router.navigate(['/events']);
    }

    onDone() {


        if (this.showFromDatePicker) {
            setTimeout(() => {
                if (!this.event.toDate) {
                    this.event.toDate = new Date(this.event.fromDate);
                }

            }, 200)
        }
        this.showFromDatePicker = false;
        this.showToDatePicker = false;
        this.showStartTimePicker = false;
        this.showEndTimePicker = false;

    }
    isSelectedFromDate() {
        if (this.endtime && this.event.toDate === null) {
            return false;
        } else {
            return true;
        }
    }
    isValidToDate() {
        if (this.event.toDate) {
            let date = new Date();
            this.event.toDate.setMinutes(this.endtime.getMinutes());
            this.event.toDate.setHours(this.endtime.getHours());
            if (this.event.toDate < date) {
                return false;
            } else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    isDateValid() {
        if (this.event.fromDate && this.event.toDate) {
            let date = new Date(this.event.toDate);
            // date.setMinutes(59);
            // date.setHours(23);
            if (this.event.fromDate > date) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    isValidDate() {
        if (this.event.fromDate) {
            let date = new Date();
            date.setMilliseconds(0o0);
            date.setMinutes(0o0);
            date.setHours(0o0);
            date.setSeconds(0o0);
            if (this.event.fromDate < date) {
                return false;
            } else {
                return true;
            }
        }
        else {
            return false;
        }
    }

    trim(obj, field) {
        let value = obj[field];
        if (value.trim().length <= 0) {
            setTimeout(() => {
                obj[field] = '';
            })
        } else {
            setTimeout(() => {
                obj[field] = value.trimLeft();
            })
        }
    }
    downloadCsv(id: any) {
        this.loaderService.show();
        let totalItems: any;

        // this.eventService.downloadCSV(id).subscribe(
        // (res: ResponseWrapper) => {

        // let headers: any = res.headers;
        // totalItems = headers.get('content-disposition');
        // console.log(body);
        // var blob = new Blob([body._body], { type: 'application/csv'});
        // var blobURL = (window.URL).createObjectURL(blob);
        let url = `${this.resourceUrlcsv}/${id}`;
        this.loaderService.hide();
        var anchor = document.createElement("a");
        anchor.download = this.event.fileName;
        const token = this.localStorage.retrieve('authenticationToken') || this.sessionStorage.retrieve('authenticationToken');
        if (!!token) {
            url += "?Authorization=" + token;
            anchor.href = url;
            anchor.click();
        }
        else {
            this.alertService.error("error.400", null, null);
        }
        //     },
        //     (err: Response) => {
        //         this.loaderService.hide();
        //         let error: any = err.json;
        //         this.alertService.error(error);
        //     }
        // );
    }
    deleteCsv(id: any) {
        // this.loaderService.show();
        if (this.event.fileName != null) {
            delete this.event.fileName;
            delete this.event.file;
            delete this.event.filetype;
            this.deleteCSVflag = 1;
            this.eventCSVFile.nativeElement.value = "";
        }

    }

    redirectErrorPage() {
        this.router.navigate(['/error', 'bad_request']);
    }
}
