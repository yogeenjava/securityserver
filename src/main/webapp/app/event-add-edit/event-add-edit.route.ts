import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { EventAddEditComponent} from './';

export const EVENT_ADD_ROUTE: Route = {
    path: 'add-event',
    component: EventAddEditComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};

export const EVENT_EDIT_ROUTE: Route = {
    path: 'edit-event/:id',
    component: EventAddEditComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};