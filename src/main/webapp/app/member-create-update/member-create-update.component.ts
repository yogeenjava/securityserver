import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs/Rx';
import { Location } from '@angular/common';
import { EosMember, EosMemberService } from '../entities/eos-member';
import { BaseEntity, User, ConfirmModalComponent, ConfirmDialodModel, ConfirmDialogService } from '../shared';
import { Floor, FloorService } from '../entities/floor';
import { ResponseWrapper, Principal, Account, LoaderService } from '../shared';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { NgbActiveModal, NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
    selector: 'app-member-create-update',
    templateUrl: './member-create-update.component.html',
    styleUrls: ['./member-create-update.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class MemberCreateUpdateComponent implements OnInit {
    isFormSubmited: boolean;
    member: EosMember;
    floors: Floor[];
    showFromDatePicker: boolean;
    showToDatePicker: boolean;
    private subscription: Subscription;
    memberuser: User;
    confirmmodel: ConfirmDialodModel;
    isMemberes: boolean;
    selectefloors = [];
    member_id: any;
    showStartTimePicker: any;
    showEndTimePicker: any;
    check = [];
    private user: Account;
    isfloor: boolean;
    date: any;
    checkall: any;
    endDate: any;
    eventSubscriber: Subscription;
    constructor(
        private confirmservice: ConfirmDialogService,
        private router: Router,
        private alertService: JhiAlertService,
        private memberservice: EosMemberService,
        private floorservice: FloorService,
        private route: ActivatedRoute,
        private principal: Principal,
        private modalService: NgbModal,
        private loaderService: LoaderService,
        private eventManager: JhiEventManager,

    ) {
        this.member = {};
        this.memberuser = {};
        this.member.floors = [];
        this.showFromDatePicker = false;
        this.showToDatePicker = false;
        this.showStartTimePicker = false;
        this.showEndTimePicker = false;
        this.confirmmodel = {};
    }
    // releaseAllPicker(event) {
    //     console.log(event.target);
    //     if(event) {
    //         event.stopPropagation();
    //     }
    //     this.showFromDatePicker = false;
    //     this.showToDatePicker = false;
    //     this.showStartTimePicker = false;
    //     this.showEndTimePicker = false;
    // }
    ngOnInit() {
        this.eventSubscriber = this.eventManager.subscribe('releaseAllPicker', (response) => {
            if (event) {
                event.stopPropagation();
            }
            this.showFromDatePicker = false;
            this.showToDatePicker = false;
            this.showStartTimePicker = false;
            this.showEndTimePicker = false;
        });
        this.loaderService.show();
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.member_id = params['id'];
                this.principal.identity().then((account) => {
                    this.user = account;
                });
            }
        });
        this.isMemberes = true;
        this.getFloor();
        this.loaderService.hide();
    }
    clear() {
        window.history.back();
    }
    getMember() {
        this.loaderService.show();
        this.memberservice.find(this.member_id).subscribe((member) => {
            this.loaderService.hide();
            if (member.user == null) {
                this.alertService.error("no member found", null, null);
            }
            else {
                this.isMemberes = true;
                this.memberuser = member.user;
                this.member.floors = member.floors;
                for (let i = 0; i < this.floors.length; i++) {
                    let data: any = this.floors[i];
                    for (let j = 0; j < this.member.floors.length; j++) {
                        if (this.floors[i].id === this.member.floors[j].id) {
                            data.checked = true;
                        }
                    }
                    this.floors[i] = data;
                }
                this.member.fromDate = member.fromDate;
                this.member.toDate = member.toDate;
                this.member.startTime = member.startTime;
                this.member.endTime = member.endTime;

                let datacheck: any = this.floors;
                let count: number = 0;
                for (let i = 0; i < datacheck.length; i++) {
                    if (datacheck[i].checked == false) {
                        count++;
                    }
                }
                if (count == 0) {
                    this.checkall = true;
                } else {
                    this.checkall = false;
                }
            }
        }, (err: any) => {
            if (err.status == 400) {
                this.loaderService.hide();
                this.redirectErrorPage();
            }
        })
    }
    getFloor() {
        this.floors = [];
        this.loaderService.show();
        this.floorservice.query().subscribe(
            (res: ResponseWrapper) => {

                if (res.status == 400) {
                    this.redirectErrorPage();
                }

                let data: any = res.json;
                for (let i = 0; i < data.length; i++) {
                    data[i].checked = false;
                    this.floors.push(data[i]);
                }
                this.loaderService.hide();
                let isAdmin = this.principal.hasAnyAuthorityDirect(["ROLE_ADMIN"]);
                if (isAdmin && (this.member_id)) {
                    this.getMember();
                    this.loaderService.hide();
                } else {
                    //   this.router.navigate(['/login']);
                    this.loaderService.hide();
                }
            }, (res: ResponseWrapper) => {
                // let data: any = res.json;
                // console.log("no floor found"+res);
                // if(data)
                // {
                //     this.isfloor=true;
                //     console.log("no floor found");
                // }
                // else{
                if (res.status == 400) {
                    this.loaderService.hide();
                    this.redirectErrorPage();
                }
                this.isfloor = true;
                // }
                let error: any = res.json;
                this.alertService.error(error, null, null);
                this.loaderService.hide();
            }
        );
    }
    save(form: any) {
        this.isFormSubmited = true;
        if (!this.isFloorValid()) { return; }
        if (!this.isDateValid()) { return; }
        if (!this.isfromdate()) { return; }
        if (!this.isValidDate()) { return; }
        // if(!this.isSelectedFromDate()){return;}
        if (!this.isDateTimeValid()) { return; }

        if (this.member.fromDate && this.member.startTime) {
            this.date = new Date(this.member.fromDate);
            let start_time = new Date(this.member.startTime);
            this.member.fromDate.setHours(start_time.getHours());
            this.member.fromDate.setMinutes(start_time.getMinutes());
        } else {
            this.member.fromDate.setMinutes(0o0);
            this.member.fromDate.setHours(0o0);
        }
        if (this.member.toDate && this.member.endTime) {
            this.endDate = new Date(this.member.toDate);
            let end_time = new Date(this.member.endTime);
            this.member.toDate.setHours(end_time.getHours());
            this.member.toDate.setMinutes(end_time.getMinutes());
        } else if (this.member.toDate && !this.member.endTime) {
            this.member.toDate.setMinutes(59);
            this.member.toDate.setHours(23);
        } else {

        }
        if (!form.valid) { return; }
        let data: any = this.floors;
        for (let i = 0; i < data.length; i++) {
            if (data[i].checked == true) {
                this.selectefloors.push(this.floors[i]);
            }
        }
        this.member.floors = this.selectefloors;
        this.member.user = this.memberuser;
        if (!this.member_id) {
            this.confirmmodel.name = this.memberuser.firstName;
            this.confirmmodel.lastname = this.memberuser.lastName;
            this.confirmmodel.page = "a Member";
            this.confirmservice.setData(this.confirmmodel);
            const modalRef = this.modalService.open(ConfirmModalComponent, { size: 'lg', backdrop: 'static' });
            modalRef.result.then(() => {
            }, (reason) => {
                if (reason === true) {
                    this.loaderService.show();
                    let memberData = Object.assign({}, this.member);
                    this.memberservice.create(memberData).subscribe(
                        (res: EosMember) => {
                            if (res.status == true) {
                                this.loaderService.hide();
                                this.alertService.success("global.messages.memberadded.memberaddsuccess");
                                this.isFormSubmited = false;
                                this.member = res;
                                this.router.navigate(['/members']);
                            }
                            else {
                                this.loaderService.hide();
                                if (res.message) {
                                    this.alertService.error("error.custom", { msg: res.message });
                                } else {
                                    this.alertService.error("error.400", null, null);
                                }
                            }
                        }, (res: any) => {
                            if (res._body) {
                                let body = JSON.parse(res._body);
                                if (body.message) {
                                    this.alertService.error("error.custom", { msg: body.message });
                                } else {
                                    this.alertService.error("error.internalServerError", null, null);
                                }
                            } else {
                                this.alertService.error("error.internalServerError", null, null);
                            }
                            this.loaderService.hide();
                        }
                    );
                }
            });
        }
        else {
            this.loaderService.show();
            if (this.member_id) {
                this.member.id = this.member_id;
                let memberData = Object.assign({}, this.member);
                this.memberservice.update(memberData).subscribe(
                    (res: EosMember) => {
                        if (res.status == true) {
                            this.loaderService.hide();
                            this.alertService.success("global.messages.memberupdate.memberupdatesuccess");
                            this.isFormSubmited = false;
                            // this._location.back();
                            this.router.navigate(['/members']);
                        }
                        else {
                            this.loaderService.hide();
                            if (res.message) {
                                this.alertService.error("error.custom", { msg: res.message });
                            } else {
                                this.alertService.error("error.400", null, null);
                            }
                        }
                    }, (res: any) => {
                        this.loaderService.hide();
                        if (res._body) {
                            let body = JSON.parse(res._body);
                            if (body.message) {
                                this.alertService.error("error.custom", { msg: body.message });
                            } else {
                                this.alertService.error("error.internalServerError", null, null);
                            }
                        } else {
                            this.alertService.error("error.internalServerError", null, null);
                        }
                    }
                );
            }
        }
    }
    // isSelectedFromDate()
    // {
    //     if(this.endtime && this.member.toDate === null){
    //         return false;
    //     }else{
    //         return true;
    //     }
    // }
    onSelectall(event) {
        let data: any = this.floors;
        if (this.checkall === true) {
            for (let i = 0; i < data.length; i++) {
                data[i].checked = true;
            }
        }
        else {
            for (let i = 0; i < data.length; i++) {
                data[i].checked = false;
            }
        }

    }
    onAllChange($event) {
        let data: any = this.floors;
        let count: number = 0;
        for (let i = 0; i < data.length; i++) {
            if (data[i].checked == false) {
                count++;
            }
        }
        if (count == 0) {
            this.checkall = true;
        } else {
            this.checkall = false;
        }
    }
    isDateTimeValid() {
        if (this.member.fromDate && this.member.startTime
            && this.member.toDate && this.member.endTime) {

            this.date = new Date(this.member.fromDate);
            let start_time = new Date(this.member.startTime);
            this.date.setHours(start_time.getHours());
            this.date.setMinutes(start_time.getMinutes());

            this.endDate = new Date(this.member.toDate);
            let end_time = new Date(this.member.endTime);
            this.endDate.setHours(end_time.getHours());
            this.endDate.setMinutes(end_time.getMinutes());
            if (this.endDate <= this.date) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }
    isFloorValid() {
        let data: any = this.floors;
        let count: number = 0;
        for (let i = 0; i < data.length; i++) {
            if (data[i].checked == true) {
                count++;
            }
        }
        if (count == 0) {
            return false;
        } else {
            return true;
        }
    }
    isfromdate() {
        if (this.member.fromDate == null) {
            return false;

        }
        else {
            return true;
        }
    }
    isValidDate() {
        if (this.member.toDate) {
            let date = new Date();
            date.setMilliseconds(0o0);
            date.setMinutes(0o0);
            date.setHours(0o0);
            date.setSeconds(0o0);
            if (this.member.toDate < date) {
                return false;
            } else {
                return true;
            }
        }
        else {
            return true;
        }
    }

    isDateValid() {
        if (this.member.fromDate && this.member.toDate) {
            this.member.toDate.setMinutes(59);
            this.member.toDate.setHours(23);
            if (this.member.fromDate > this.member.toDate) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    trim(obj, field) {
        let value = obj[field];
        if (value.trim().length <= 0) {
            setTimeout(() => {
                obj[field] = '';
            })
        } else {
            setTimeout(() => {
                obj[field] = value.trimLeft();
            })
        }
    }
    onDone() {
        if (this.showFromDatePicker && !this.member_id && !this.member.startTime && !this.member.endTime) {
            setTimeout(() => {
                this.member.fromDate = this.member.fromDate;
                this.member.startTime = new Date();
                this.member.startTime.setHours(0o0);
                this.member.startTime.setMinutes(0o0);
                this.member.endTime = new Date();
                this.member.endTime.setHours(23);
                this.member.endTime.setMinutes(59);
            }, 200)
        }
        this.showFromDatePicker = false;
        this.showToDatePicker = false;
        this.showStartTimePicker = false;
        this.showEndTimePicker = false;
    }

    redirectErrorPage() {
        this.router.navigate(['/error', 'bad_request']);
    }
}
