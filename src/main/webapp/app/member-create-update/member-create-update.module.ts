import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EosSharedModule } from '../shared';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';
import { MemberCreateUpdateComponent } from './';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
@NgModule({
    imports: [
        EosSharedModule,        
        DatepickerModule.forRoot(),
        TimepickerModule.forRoot()
    ],
    declarations: [
        MemberCreateUpdateComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MemberCreateUpdateModule {}
