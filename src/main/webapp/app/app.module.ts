import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { EosSharedModule, UserRouteAccessService } from './shared';
import { EosHomeModule } from './home/home.module';
import { EosAdminModule } from './admin/admin.module';
import { EosAccountModule } from './account/account.module';
import { EosEntityModule } from './entities/entity.module';
import { EosViewEventsModule} from './view-events/view-events.module';
import { EosEventAddEditModule} from './event-add-edit/event-add-edit.module';
import { EosViewMembversModule } from './view-member/view-member.module';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MemberCreateUpdateModule } from './member-create-update/member-create-update.module';
import { SideMenuModule } from './side-menu';
import { HttpClientModule} from '@angular/common/http'

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent, 
   
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        EosSharedModule,
        EosHomeModule,
        SideMenuModule,
        EosAdminModule,
        EosAccountModule,
        EosEntityModule,
        EosViewEventsModule,
        EosViewMembversModule,
        EosEventAddEditModule,
        MemberCreateUpdateModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        NgbActiveModal,
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class EosAppModule {}
