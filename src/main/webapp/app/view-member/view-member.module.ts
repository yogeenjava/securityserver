import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VIEW_MEMBERS_ROUTE, ViewMembersComponent } from './';
import { EosSharedModule } from '../shared';

@NgModule({
    imports: [
        RouterModule.forRoot([ VIEW_MEMBERS_ROUTE ], { useHash: true }),
        EosSharedModule
    ],
    declarations: [
        ViewMembersComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosViewMembversModule {}
