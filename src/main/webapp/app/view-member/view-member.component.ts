import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { JhiAlertService, JhiParseLinks, JhiEventManager } from 'ng-jhipster';
import { ResponseWrapper, ITEMS_PER_PAGE } from '../shared';
import { DomSanitizer } from '@angular/platform-browser';
import { Principal, LoaderService } from '../shared';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { EosMemberService, EosMember } from '../entities/eos-member';
import { Subscription } from 'rxjs/Rx';
import { SideMenuModel } from '../entities/side-menu/side-menu.model';
import { SideMenuService } from '../entities/side-menu';
@Component({
    selector: 'app-view-member',
    templateUrl: './view-member.component.html',
    styleUrls: [
        'view-member.component.css'
    ]
})
export class ViewMembersComponent implements OnInit {
    isRes: boolean;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    reverse: any;
    totalItems: number;
    private isAdmin: boolean;
    members: EosMember[];
    eventSubscriber: Subscription;
    sidemenu: SideMenuModel;
    pages: Array<number>;
    isFirst: boolean;
    isLast: boolean;
    first: number;
    last: number;

    constructor(
        private alertService: JhiAlertService,
        private parseLinks: JhiParseLinks,
        private _sanitizer: DomSanitizer,
        private principal: Principal,
        private router: Router,
        private memberService: EosMemberService,
        private sidemenuservice: SideMenuService,
        private eventManager: JhiEventManager,
        private loaderService: LoaderService,

    ) {
        this.isRes = false;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.pages = [1];
        this.links = {
            last: 0
        };
        this.predicate = 'user.firstName';
        this.reverse = false;
        this.members = [];
        this.sidemenu = {};
        this.isFirst = true;
        this.isLast = false;
        this.first = 1;
        this.last = 5;
    }

    ngOnInit() {
        this.setMenu();
        this.getMember();
        this.registerChangeInEosMembers();
    }
    setMenu() {

        this.sidemenu.selectedmenu = "MEMBERS";
        this.sidemenuservice.setMenu(this.sidemenu);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEosMembers() {

        this.eventSubscriber = this.eventManager.subscribe('eosMemberListModification', (response) => {
            //     if (this.members && this.members.length > 0) {
            //         for (let i = 0; i < this.members.length; i++) {
            //             if (response && response.content && response.content == this.members[i].id) {
            //                 this.members.splice(i, 1);
            //                 break;
            //             }
            //         }
            //     }
            this.members = [];
            this.getMember();
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    getMember() {

        this.loaderService.show();
        this.memberService.query({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {
                let data: any = res.json;
                let headers: any = res.headers;
                this.isRes = true;
                //this.links = this.parseLinks.parse(headers.get('link'));
                this.pages = new Array(headers.get('page'));
                this.totalItems = headers.get('X-Total-Count');
                if (this.page == 0) {
                    this.isFirst = true;
                }
                else {
                    this.isFirst = false;
                }
                if (this.page == this.pages.length - 1) {
                    this.isLast = true;
                }
                else {
                    this.isLast = false;
                }
                for (let i = 0; i < data.length; i++) {
                    this.members.push(data[i]);
                }
                if (this.page > 0 && this.members.length <= 0) {
                    this.page = this.page - 1;
                    this.getMember();
                }
                this.loaderService.hide();
            },
            (err: ResponseWrapper) => {
                if (err.status == 400) {
                    this.loaderService.hide();
                    this.redirectErrorPage();
                }
                this.loaderService.hide();
                let error: any = err.json;
                this.alertService.error(error);
            }
            );
    }
    setPage(i, event: any) {
        this.page = i;
        this.members = [];
        this.getMember();
    }
    onNext(): void {
        if (this.page !== this.pages.length - 1) {
            this.page++;
            this.members = [];
            this.getMember();
        }
        this.last = (~~(this.page / 5)) * 5 + 5;
        this.first = (~~(this.page / 5)) * 5 + 1;

    }

    onPrev(): void {
        if (this.page > 0) {
            this.page--;
            this.members = [];
            this.getMember();
        }
        this.last = (~~(this.page / 5)) * 5 + 5;
        this.first = (~~(this.page / 5)) * 5 + 1;

    }
    editMember(id: any) {
        this.router.navigate(['/edit-member/' + id]);
    }

    redirectErrorPage() {
        this.router.navigate(['/error', 'bad_request']);
    }

}
