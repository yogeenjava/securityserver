import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { ViewMembersComponent } from './';
import { SideMenuComponent} from '../side-menu'

export const VIEW_MEMBERS_ROUTE: Route = {
    path: 'memberlist',
    component: ViewMembersComponent,
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'home.title'
    },
    children: [
        {
            path: '', component: SideMenuComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'home'
            },
            canActivate: [UserRouteAccessService]
        },
    ]
};
