import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
    EosSharedLibsModule,
    EosSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    JhiLoginModalComponent,
    Principal,
    HasAnyAuthorityDirective,
} from './';
import { ConfirmModalComponent } from './confirm-dialog/confirm-dialog.component';
import { ConfirmDialogService } from './confirm-dialog/confirm-dialog.service';
import { LoaderService } from './loader/loader.service';

@NgModule({
    imports: [
        EosSharedLibsModule,
        EosSharedCommonModule
    ],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        ConfirmModalComponent,
    ],
    providers: [
        LoginService,
        LoginModalService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        ConfirmDialogService,
        AuthServerProvider,
        UserService,
        DatePipe,
        LoaderService,
    ],
    entryComponents: [
        JhiLoginModalComponent,
        ConfirmModalComponent],
    exports: [
        EosSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        ConfirmModalComponent,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class EosSharedModule {}
