import { Injectable } from '@angular/core';
import { ConfirmDialodModel } from './confirm-dialog.model';
@Injectable()
export class ConfirmDialogService {
    confirmdialog :ConfirmDialodModel
    constructor() {
        this.confirmdialog={};
    }

    setData(confirmdialog : ConfirmDialodModel){
        this.confirmdialog=confirmdialog;        
    }
    getData(){
        return this.confirmdialog;     
    }
}
