import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialodModel} from './confirm-dialog.model';
import { ConfirmDialogService } from './confirm-dialog.service';
@Component({
    selector: 'app-confirm-modal',
    templateUrl: './confirm-dialog.component.html'
})
export class ConfirmModalComponent implements OnInit {
    confirrmmodel :ConfirmDialodModel;
    
    constructor(
        public activeModal: NgbActiveModal,
        private confirmservice: ConfirmDialogService,
    ) {}
    ngOnInit() {
       this.confirrmmodel = this.confirmservice.getData();
    }
    clear() {
        this.activeModal.dismiss(false);
    }
    confirm() {
        this.activeModal.dismiss(true);
    }
}