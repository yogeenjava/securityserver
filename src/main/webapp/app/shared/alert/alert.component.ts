import { Component, OnDestroy, OnInit } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-alert',
    template: `
    <div class="alerts" role="alert">
        <div *ngFor="let alert of alerts | slice:alerts.length - 1" [ngClass]="{\'alert.position\': true, \'toast\': alert.toast}">
            <ngb-alert *ngIf="alert && alert.type && alert.msg" [type]="alert.type" (close)="alert.close(alerts)">
                <p style="text-align: center;" [innerHTML]="alert.msg"></p>
            </ngb-alert>
        </div>
    </div>`
})
export class JhiAlertComponent implements OnInit, OnDestroy {
    alerts: any[];

    constructor(private alertService: JhiAlertService) { }

    ngOnInit() {
        this.alerts = this.alertService.get();
    }

    ngOnDestroy() {
        this.alerts = [];
    }

}
