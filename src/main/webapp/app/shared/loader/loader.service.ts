import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { LoaderState } from './loader';
@Injectable()
export class LoaderService {
    private loaderSubject = new Subject<LoaderState>();
    private loaderCalledcount: number;
    loaderState = this.loaderSubject.asObservable();

    constructor() { 
        this.loaderCalledcount = 0;
    }
    show() {
        if(this.loaderCalledcount==0) {
            this.loaderSubject.next(<LoaderState>{show: true});
        }
        this.loaderCalledcount ++ ;
    }
    hide() {
        if(this.loaderCalledcount > 0){
            this.loaderCalledcount--;
        }
        if(this.loaderCalledcount==0) {
            this.loaderSubject.next(<LoaderState>{show: false});
        }
    }
}