import { BaseEntity } from './../../shared';

export class EventGuest implements BaseEntity {
    constructor(
        public id?: number,
        public email?: string,
        public eventId?: number,
    ) {
    }
}
