import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EosSharedModule } from '../../shared';
import {
    EventGuestService,
    EventGuestPopupService,
    EventGuestComponent,
    EventGuestDetailComponent,
    EventGuestDialogComponent,
    EventGuestPopupComponent,
    EventGuestDeletePopupComponent,
    EventGuestDeleteDialogComponent,
    eventGuestRoute,
    eventGuestPopupRoute,
} from './';

const ENTITY_STATES = [
    ...eventGuestRoute,
    ...eventGuestPopupRoute,
];

@NgModule({
    imports: [
        EosSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EventGuestComponent,
        EventGuestDetailComponent,
        EventGuestDialogComponent,
        EventGuestDeleteDialogComponent,
        EventGuestPopupComponent,
        EventGuestDeletePopupComponent,
    ],
    entryComponents: [
        EventGuestComponent,
        EventGuestDialogComponent,
        EventGuestPopupComponent,
        EventGuestDeleteDialogComponent,
        EventGuestDeletePopupComponent,
    ],
    providers: [
        EventGuestService,
        EventGuestPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosEventGuestModule {}
