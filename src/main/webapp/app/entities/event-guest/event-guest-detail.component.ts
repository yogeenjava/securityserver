import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { EventGuest } from './event-guest.model';
import { EventGuestService } from './event-guest.service';

@Component({
    selector: 'jhi-event-guest-detail',
    templateUrl: './event-guest-detail.component.html'
})
export class EventGuestDetailComponent implements OnInit, OnDestroy {

    eventGuest: EventGuest;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private eventGuestService: EventGuestService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEventGuests();
    }

    load(id) {
        this.eventGuestService.find(id).subscribe((eventGuest) => {
            this.eventGuest = eventGuest;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEventGuests() {
        this.eventSubscriber = this.eventManager.subscribe(
            'eventGuestListModification',
            (response) => this.load(this.eventGuest.id)
        );
    }
}
