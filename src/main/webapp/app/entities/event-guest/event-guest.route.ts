import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { EventGuestComponent } from './event-guest.component';
import { EventGuestDetailComponent } from './event-guest-detail.component';
import { EventGuestPopupComponent } from './event-guest-dialog.component';
import { EventGuestDeletePopupComponent } from './event-guest-delete-dialog.component';

export const eventGuestRoute: Routes = [
    {
        path: 'event-guest',
        component: EventGuestComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'event-guest/:id',
        component: EventGuestDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const eventGuestPopupRoute: Routes = [
    {
        path: 'event-guest-new',
        component: EventGuestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-guest/:id/edit',
        component: EventGuestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-guest/:id/delete',
        component: EventGuestDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
