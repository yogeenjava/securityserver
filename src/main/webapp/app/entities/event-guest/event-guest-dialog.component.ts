import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { EventGuest } from './event-guest.model';
import { EventGuestPopupService } from './event-guest-popup.service';
import { EventGuestService } from './event-guest.service';
import { Event, EventService } from '../event';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-event-guest-dialog',
    templateUrl: './event-guest-dialog.component.html'
})
export class EventGuestDialogComponent implements OnInit {

    eventGuest: EventGuest;
    isSaving: boolean;

    events: Event[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private eventGuestService: EventGuestService,
        private eventService: EventService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.eventService.query()
            .subscribe((res: ResponseWrapper) => { this.events = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.eventGuest.id !== undefined) {
            this.subscribeToSaveResponse(
                this.eventGuestService.update(this.eventGuest));
        } else {
            this.subscribeToSaveResponse(
                this.eventGuestService.create(this.eventGuest));
        }
    }

    private subscribeToSaveResponse(result: Observable<EventGuest>) {
        result.subscribe((res: EventGuest) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: EventGuest) {
        this.eventManager.broadcast({ name: 'eventGuestListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackEventById(index: number, item: Event) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-event-guest-popup',
    template: ''
})
export class EventGuestPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventGuestPopupService: EventGuestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.eventGuestPopupService
                    .open(EventGuestDialogComponent as Component, params['id']);
            } else {
                this.eventGuestPopupService
                    .open(EventGuestDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
