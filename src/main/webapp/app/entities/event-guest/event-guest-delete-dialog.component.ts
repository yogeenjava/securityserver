import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { EventGuest } from './event-guest.model';
import { EventGuestPopupService } from './event-guest-popup.service';
import { EventGuestService } from './event-guest.service';

@Component({
    selector: 'jhi-event-guest-delete-dialog',
    templateUrl: './event-guest-delete-dialog.component.html'
})
export class EventGuestDeleteDialogComponent {

    eventGuest: EventGuest;

    constructor(
        private eventGuestService: EventGuestService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.eventGuestService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'eventGuestListModification',
                content: 'Deleted an eventGuest'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-event-guest-delete-popup',
    template: ''
})
export class EventGuestDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventGuestPopupService: EventGuestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.eventGuestPopupService
                .open(EventGuestDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
