import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { EventGuest } from './event-guest.model';
import { EventGuestService } from './event-guest.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-event-guest',
    templateUrl: './event-guest.component.html'
})
export class EventGuestComponent implements OnInit, OnDestroy {
eventGuests: EventGuest[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private eventGuestService: EventGuestService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.eventGuestService.query().subscribe(
            (res: ResponseWrapper) => {
                this.eventGuests = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEventGuests();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: EventGuest) {
        return item.id;
    }
    registerChangeInEventGuests() {
        this.eventSubscriber = this.eventManager.subscribe('eventGuestListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
