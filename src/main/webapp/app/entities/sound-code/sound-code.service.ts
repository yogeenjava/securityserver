import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { SoundCode } from './sound-code.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SoundCodeService {

    private resourceUrl = SERVER_API_URL + 'api/admin/sound-codes';

    constructor(private http: Http) { }

    create(soundCode: SoundCode): Observable<SoundCode> {
        const copy = this.convert(soundCode);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(soundCode: SoundCode): Observable<SoundCode> {
        const copy = this.convert(soundCode);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<SoundCode> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to SoundCode.
     */
    private convertItemFromServer(json: any): SoundCode {
        const entity: SoundCode = Object.assign(new SoundCode(), json);
        return entity;
    }

    /**
     * Convert a SoundCode to a JSON which can be sent to the server.
     */
    private convert(soundCode: SoundCode): SoundCode {
        const copy: SoundCode = Object.assign({}, soundCode);
        return copy;
    }
}
