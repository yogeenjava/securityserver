import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SoundCodeComponent } from './sound-code.component';
import { SoundCodeDetailComponent } from './sound-code-detail.component';
import { SoundCodePopupComponent } from './sound-code-dialog.component';
import { SoundCodeDeletePopupComponent } from './sound-code-delete-dialog.component';

export const soundCodeRoute: Routes = [
    {
        path: 'sound-code',
        component: SoundCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.soundCode.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sound-code/:id',
        component: SoundCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.soundCode.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const soundCodePopupRoute: Routes = [
    {
        path: 'sound-code-new',
        component: SoundCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.soundCode.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sound-code/:id/edit',
        component: SoundCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.soundCode.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sound-code/:id/delete',
        component: SoundCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.soundCode.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
