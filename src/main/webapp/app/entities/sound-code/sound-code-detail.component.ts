import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SoundCode } from './sound-code.model';
import { SoundCodeService } from './sound-code.service';

@Component({
    selector: 'jhi-sound-code-detail',
    templateUrl: './sound-code-detail.component.html'
})
export class SoundCodeDetailComponent implements OnInit, OnDestroy {

    soundCode: SoundCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private soundCodeService: SoundCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSoundCodes();
    }

    load(id) {
        this.soundCodeService.find(id).subscribe((soundCode) => {
            this.soundCode = soundCode;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSoundCodes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'soundCodeListModification',
            (response) => this.load(this.soundCode.id)
        );
    }
}
