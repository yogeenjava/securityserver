import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { SoundCode } from './sound-code.model';
import { SoundCodeService } from './sound-code.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-sound-code',
    templateUrl: './sound-code.component.html'
})
export class SoundCodeComponent implements OnInit, OnDestroy {
soundCodes: SoundCode[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private soundCodeService: SoundCodeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.soundCodeService.query().subscribe(
            (res: ResponseWrapper) => {
                this.soundCodes = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSoundCodes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SoundCode) {
        return item.id;
    }
    registerChangeInSoundCodes() {
        this.eventSubscriber = this.eventManager.subscribe('soundCodeListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
