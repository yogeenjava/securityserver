import { BaseEntity } from './../../shared';

export class SoundCode implements BaseEntity {
    constructor(
        public id?: number,
        public licenseNo?: string,
        public uid?: string,
    ) {
    }
}
