import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EosSharedModule } from '../../shared';
import {
    SoundCodeService,
    SoundCodePopupService,
    SoundCodeComponent,
    SoundCodeDetailComponent,
    SoundCodeDialogComponent,
    SoundCodePopupComponent,
    SoundCodeDeletePopupComponent,
    SoundCodeDeleteDialogComponent,
    soundCodeRoute,
    soundCodePopupRoute,
} from './';

const ENTITY_STATES = [
    ...soundCodeRoute,
    ...soundCodePopupRoute,
];

@NgModule({
    imports: [
        EosSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SoundCodeComponent,
        SoundCodeDetailComponent,
        SoundCodeDialogComponent,
        SoundCodeDeleteDialogComponent,
        SoundCodePopupComponent,
        SoundCodeDeletePopupComponent,
    ],
    entryComponents: [
        SoundCodeComponent,
        SoundCodeDialogComponent,
        SoundCodePopupComponent,
        SoundCodeDeleteDialogComponent,
        SoundCodeDeletePopupComponent,
    ],
    providers: [
        SoundCodeService,
        SoundCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosSoundCodeModule {}
