import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SoundCode } from './sound-code.model';
import { SoundCodePopupService } from './sound-code-popup.service';
import { SoundCodeService } from './sound-code.service';

@Component({
    selector: 'jhi-sound-code-delete-dialog',
    templateUrl: './sound-code-delete-dialog.component.html'
})
export class SoundCodeDeleteDialogComponent {

    soundCode: SoundCode;

    constructor(
        private soundCodeService: SoundCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.soundCodeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'soundCodeListModification',
                content: 'Deleted an soundCode'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sound-code-delete-popup',
    template: ''
})
export class SoundCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private soundCodePopupService: SoundCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.soundCodePopupService
                .open(SoundCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
