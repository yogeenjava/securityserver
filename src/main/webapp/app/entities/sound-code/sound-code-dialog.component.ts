import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SoundCode } from './sound-code.model';
import { SoundCodePopupService } from './sound-code-popup.service';
import { SoundCodeService } from './sound-code.service';

@Component({
    selector: 'jhi-sound-code-dialog',
    templateUrl: './sound-code-dialog.component.html'
})
export class SoundCodeDialogComponent implements OnInit {

    soundCode: SoundCode;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private soundCodeService: SoundCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.soundCode.id !== undefined) {
            this.subscribeToSaveResponse(
                this.soundCodeService.update(this.soundCode));
        } else {
            this.subscribeToSaveResponse(
                this.soundCodeService.create(this.soundCode));
        }
    }

    private subscribeToSaveResponse(result: Observable<SoundCode>) {
        result.subscribe((res: SoundCode) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: SoundCode) {
        this.eventManager.broadcast({ name: 'soundCodeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-sound-code-popup',
    template: ''
})
export class SoundCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private soundCodePopupService: SoundCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.soundCodePopupService
                    .open(SoundCodeDialogComponent as Component, params['id']);
            } else {
                this.soundCodePopupService
                    .open(SoundCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
