import { BaseEntity } from './../../shared';

export class Member implements BaseEntity {
    constructor(
        public id?: number,
        public firstname?: string,
        public lastname?: string,
        public email?: string,
        public floor?: BaseEntity[],
        public fromdate?: any,
        public todate?: any,       
        public isActive?: boolean,
    ) {
        this.isActive = false;
    }
}
