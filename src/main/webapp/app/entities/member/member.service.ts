import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { Member } from './member.model';
import { ResponseWrapper, createRequestOption, Principal } from '../../shared';

@Injectable()
export class MemberService {

    private resourceUrl = 'api/eos-members';

    constructor(private http: Http, private dateUtils: JhiDateUtils, private principal: Principal) { }

    create(member: Member): Observable<Member> {
        // const copy = this.convert(project);
        const copy = member;
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    // update(member: Member): Observable<Member> {
    //     // const copy = this.convert(project);
    //     const copy = Member;
    //     return this.http.put(this.resourceUrl, copy).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }
    // delete(id: number): Observable<Response> {
    //     return this.http.delete(`${this.resourceUrl}/${id}`);
    // }
    private convertItemFromServer(entity: any) {
        entity.creationDateTime = this.dateUtils
            .convertDateTimeFromServer(entity.creationDateTime);
    }

}
