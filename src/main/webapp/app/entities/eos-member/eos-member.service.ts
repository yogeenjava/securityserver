import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { EosMember } from './eos-member.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class EosMemberService {

    private resourceUrl = SERVER_API_URL + 'api/admin/eos-members';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(eosMember: EosMember): Observable<EosMember> {
        //const copy = this.convert(eosMember);
        const copy = eosMember;
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(eosMember: EosMember): Observable<EosMember> {
        // const copy = this.convert(eosMember);
        const copy = eosMember;
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<EosMember> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse.data);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        // debugger;
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponseToData(res));
    }

    delete(id: number): Observable<Response> { 
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }
    
    private convertResponseToData(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        const page=jsonResponse.data.totalPages;
        for (let i = 0; i < jsonResponse.data.content.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse.data.content[i]));
        }
        res.headers.append("page",page);
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to EosMember.
     */
    private convertItemFromServer(json: any): EosMember {
        const entity: EosMember = Object.assign(new EosMember(), json);
        entity.fromDate = this.dateUtils
            .convertDateTimeFromServer(json.fromDate);
        entity.toDate = this.dateUtils
            .convertDateTimeFromServer(json.toDate);
        return entity;
    }

    /**
     * Convert a EosMember to a JSON which can be sent to the server.
     */
    private convert(eosMember: EosMember): EosMember {
        const copy: EosMember = Object.assign({}, eosMember);

        copy.fromDate = this.dateUtils.toDate(eosMember.fromDate);

        copy.toDate = this.dateUtils.toDate(eosMember.toDate);
        return copy;
    }
}
