import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EosSharedModule } from '../../shared';
import {
    EosMemberService,
    EosMemberPopupService,
    EosMemberComponent,
    EosMemberDetailComponent,
    EosMemberDialogComponent,
    EosMemberPopupComponent,
    EosMemberDeletePopupComponent,
    EosMemberDeleteDialogComponent,
    eosMemberRoute,
    eosMemberPopupRoute,
} from './';

const ENTITY_STATES = [
    ...eosMemberRoute,
    ...eosMemberPopupRoute,
];

@NgModule({
    imports: [
        EosSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EosMemberComponent,
        EosMemberDetailComponent,
        EosMemberDialogComponent,
        EosMemberDeleteDialogComponent,
        EosMemberPopupComponent,
        EosMemberDeletePopupComponent,
    ],
    entryComponents: [
        EosMemberComponent,
        EosMemberDialogComponent,
        EosMemberPopupComponent,
        EosMemberDeleteDialogComponent,
        EosMemberDeletePopupComponent,
    ],
    providers: [
        EosMemberService,
        EosMemberPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosEosMemberModule {}
