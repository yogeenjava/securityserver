import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { EosMember } from './eos-member.model';
import { EosMemberPopupService } from './eos-member-popup.service';
import { EosMemberService } from './eos-member.service';
import { Floor, FloorService } from '../floor';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-eos-member-dialog',
    templateUrl: './eos-member-dialog.component.html'
})
export class EosMemberDialogComponent implements OnInit {

    eosMember: EosMember;
    isSaving: boolean;

    floors: Floor[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private eosMemberService: EosMemberService,
        private floorService: FloorService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.floorService.query()
            .subscribe((res: ResponseWrapper) => { this.floors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.eosMember.id !== undefined) {
            this.subscribeToSaveResponse(
                this.eosMemberService.update(this.eosMember));
        } else {
            this.subscribeToSaveResponse(
                this.eosMemberService.create(this.eosMember));
        }
    }

    private subscribeToSaveResponse(result: Observable<EosMember>) {
        result.subscribe((res: EosMember) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: EosMember) {
        this.eventManager.broadcast({ name: 'eosMemberListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackFloorById(index: number, item: Floor) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-eos-member-popup',
    template: ''
})
export class EosMemberPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eosMemberPopupService: EosMemberPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.eosMemberPopupService
                    .open(EosMemberDialogComponent as Component, params['id']);
            } else {
                this.eosMemberPopupService
                    .open(EosMemberDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
