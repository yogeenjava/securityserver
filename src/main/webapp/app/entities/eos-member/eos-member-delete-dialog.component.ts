import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService } from '../../shared';
import { EosMember } from './eos-member.model';
import { EosMemberPopupService } from './eos-member-popup.service';
import { EosMemberService } from './eos-member.service';

@Component({
    selector: 'jhi-eos-member-delete-dialog',
    templateUrl: './eos-member-delete-dialog.component.html'
})

export class EosMemberDeleteDialogComponent {

    eosMember: EosMember;
    id: Number

    constructor(
        private eosMemberService: EosMemberService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private alertService: JhiAlertService,
        private loaderService: LoaderService,
        
    ) {
      
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.loaderService.show();
        this.eosMemberService.delete(id).subscribe((response) => {
            this.loaderService.hide();
            this.alertService.success("global.messages.memberdelete.memberdeletesuccess");
            this.eventManager.broadcast({
                name: 'eosMemberListModification',
                content: id
            });
            this.activeModal.dismiss(true);
            this.loaderService.hide();
        });
    }
}

@Component({
    selector: 'jhi-eos-member-delete-popup',
    template: ''
})
export class EosMemberDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;
    constructor(
        private route: ActivatedRoute,
        private eosMemberPopupService: EosMemberPopupService
    ) {}
    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.eosMemberPopupService
                .open(EosMemberDeleteDialogComponent as Component, params['id']);
        });
    }
    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
