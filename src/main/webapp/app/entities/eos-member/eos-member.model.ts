import { BaseEntity, User } from './../../shared';

export class EosMember implements BaseEntity {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public login?: string,
        public floors?: BaseEntity[],
        public fromDate?: any,
        public toDate?: any,
        public isActive?: boolean,
        public licenceKey?: string,
        public user?: User,
        public start_date?: any,
        public startTime?: any,
        public end_date?: any,
        public endTime?: any,
        public status?:boolean,
        public message?:string,
    ) {
    }
}
