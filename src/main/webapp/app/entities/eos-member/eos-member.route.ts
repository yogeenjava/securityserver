import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { EosMemberComponent } from './eos-member.component';
import { EosMemberDetailComponent } from './eos-member-detail.component';
import { EosMemberPopupComponent } from './eos-member-dialog.component';
import { EosMemberDeletePopupComponent } from './eos-member-delete-dialog.component';

export const eosMemberRoute: Routes = [
    {
        path: 'eos-member',
        component: EosMemberComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'eosApp.eosMember.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'eos-member/:id',
        component: EosMemberDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'eosApp.eosMember.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const eosMemberPopupRoute: Routes = [
    {
        path: 'eos-member-new',
        component: EosMemberPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'eosApp.eosMember.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'eos-member/:id/edit',
        component: EosMemberPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'eosApp.eosMember.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'eos-member/:id/delete',
        component: EosMemberDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'eosApp.eosMember.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
