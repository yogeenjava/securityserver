import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { EosMember } from './eos-member.model';
import { EosMemberService } from './eos-member.service';

@Component({
    selector: 'jhi-eos-member-detail',
    templateUrl: './eos-member-detail.component.html'
})
export class EosMemberDetailComponent implements OnInit, OnDestroy {

    eosMember: EosMember;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private eosMemberService: EosMemberService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEosMembers();
    }

    load(id) {
        this.eosMemberService.find(id).subscribe((eosMember) => {
            this.eosMember = eosMember;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEosMembers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'eosMemberListModification',
            (response) => this.load(this.eosMember.id)
        );
    }
}
