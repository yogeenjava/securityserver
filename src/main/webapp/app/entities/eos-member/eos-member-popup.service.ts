import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { EosMember } from './eos-member.model';
import { EosMemberService } from './eos-member.service';

@Injectable()
export class EosMemberPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private eosMemberService: EosMemberService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        // debugger;
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }
            if (id) {
                
        
                this.eosMemberService.find(id).subscribe((eosMember) => {
                    eosMember.fromDate = this.datePipe
                        .transform(eosMember.fromDate, 'yyyy-MM-ddTHH:mm:ss');
                    eosMember.toDate = this.datePipe
                        .transform(eosMember.toDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.eosMemberModalRef(component, eosMember);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.eosMemberModalRef(component, new EosMember());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    eosMemberModalRef(component: Component, eosMember: EosMember): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.eosMember = eosMember;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
