import { Injectable } from '@angular/core';
import { Http, Response,RequestOptions,Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Event } from './event.model';
import { ResponseWrapper, createRequestOption ,} from '../../shared';

@Injectable()
export class EventService {

    private resourceUrl = SERVER_API_URL + 'api/admin/events';
    private csvurl = SERVER_API_URL + 'api/admin/csv/download';
    private resourceUrlcsv = SERVER_API_URL + 'api/admin/event-guest-csvs';
    
    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(event: Event): Observable<Event> {
        // const copy = this.convert(event);
        const copy = event;        
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(event:any): Observable<Event> {
        // const copy = this.convert(event);
        const copy = event;        
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Event> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromData(jsonResponse.data);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get('api/admin/all-events', options)
            .map((res: Response) => this.convertResponseToData(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }
    deletecsv(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrlcsv}/${id}`);
    }
    // downloadCSV(id: number){
    //     return this.http.get(`${this.csvurl}/${id}`);
    // }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse.content[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponseToData(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        const page=jsonResponse.data.totalPages;
        for (let i = 0; i < jsonResponse.data.content.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse.data.content[i]));
        }
        res.headers.append("page",page);
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Event.
     */
    private convertItemFromServer(json: any): Event {
        const entity: Event = Object.assign(new Event(), json);
        entity.fromDate = this.dateUtils
            .convertDateTimeFromServer(json.fromDate);
        entity.toDate = this.dateUtils
            .convertDateTimeFromServer(json.toDate);
        return entity;
    }


    private convertItemFromData(json: any): Event {
        const entity: Event = Object.assign(new Event(), json);
        
        entity.fromDate = this.dateUtils
            .convertDateTimeFromServer(json.fromDate);
        entity.toDate = this.dateUtils
            .convertDateTimeFromServer(json.toDate);
        return entity;
    }


    /**
     * Convert a Event to a JSON which can be sent to the server.
     */
    private convert(event: Event): Event {
        const copy: Event = Object.assign({}, event);

        copy.fromDate = this.dateUtils.toDate(event.fromDate);

        copy.toDate = this.dateUtils.toDate(event.toDate);
        return copy;
    }
}
