import { BaseEntity } from './../../shared';

export class Event implements BaseEntity {
    constructor(
        public id?: number,
        public eventName?: string,
        public description?: string,
        public fromDate?: any,
        public toDate?: any,
        public eventFloors?: BaseEntity[],
        public eventDate?: any,
        public webUrl?: string,
        public file?:File,
        public filetype?:string,
        public fileName?:string,
        public status?:boolean,
        public message?:string,
    ) {
    }
}
