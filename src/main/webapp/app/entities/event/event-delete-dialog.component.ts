import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager , JhiAlertService } from 'ng-jhipster';
import { LoaderService } from '../../shared';
import { Event } from './event.model';
import { EventPopupService } from './event-popup.service';
import { EventService } from './event.service';

@Component({
    selector: 'jhi-event-delete-dialog',
    templateUrl: './event-delete-dialog.component.html'
})
export class EventDeleteDialogComponent {

    event: Event;
    id : number;
    constructor(
        private eventService: EventService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private alertService: JhiAlertService,
        private loaderService: LoaderService,
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.loaderService.show();
        this.eventService.delete(id).subscribe((response) => {
            this.loaderService.hide();
            this.alertService.success("global.messages.eventdelete.eventdeletesuccess");
            this.eventManager.broadcast({
                name: 'eventListModification',
                content: id
            });
            this.activeModal.dismiss(true);
            this.loaderService.hide();
        });
    }
}

@Component({
    selector: 'jhi-event-delete-popup',
    template: ''
})
export class EventDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;
    constructor(
        private route: ActivatedRoute,
        private eventPopupService: EventPopupService
    ) {}
    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.eventPopupService
                .open(EventDeleteDialogComponent as Component, params['id']);
        });
    }
    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
