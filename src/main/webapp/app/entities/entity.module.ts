import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { EosFloorModule } from './floor/floor.module';

import { EosEosMemberModule } from './eos-member/eos-member.module';

import { EosEventModule } from './event/event.module';
import { EosEventGuestModule } from './event-guest/event-guest.module';
import { MemberModule } from './member/member.module';
import { SidemenuModule } from './side-menu/sidemenu.module';

import { EosSoundCodeModule } from './sound-code/sound-code.module';

import { EosEventGuestCsvModule } from './event-guest-csv/event-guest-csv.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        EosFloorModule,
        MemberModule,
        EosEosMemberModule,
        EosEventModule,
        EosEventGuestModule,
        SidemenuModule,
        EosSoundCodeModule,
        EosEventGuestCsvModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosEntityModule {}
