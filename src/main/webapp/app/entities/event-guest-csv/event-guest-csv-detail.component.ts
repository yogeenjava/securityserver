import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { EventGuestCsv } from './event-guest-csv.model';
import { EventGuestCsvService } from './event-guest-csv.service';

@Component({
    selector: 'jhi-event-guest-csv-detail',
    templateUrl: './event-guest-csv-detail.component.html'
})
export class EventGuestCsvDetailComponent implements OnInit, OnDestroy {

    eventGuestCsv: EventGuestCsv;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private eventGuestCsvService: EventGuestCsvService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEventGuestCsvs();
    }

    load(id) {
        this.eventGuestCsvService.find(id).subscribe((eventGuestCsv) => {
            this.eventGuestCsv = eventGuestCsv;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEventGuestCsvs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'eventGuestCsvListModification',
            (response) => this.load(this.eventGuestCsv.id)
        );
    }
}
