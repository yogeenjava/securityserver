import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { EventGuestCsv } from './event-guest-csv.model';
import { EventGuestCsvService } from './event-guest-csv.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-event-guest-csv',
    templateUrl: './event-guest-csv.component.html'
})
export class EventGuestCsvComponent implements OnInit, OnDestroy {
eventGuestCsvs: EventGuestCsv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private eventGuestCsvService: EventGuestCsvService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.eventGuestCsvService.query().subscribe(
            (res: ResponseWrapper) => {
                this.eventGuestCsvs = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEventGuestCsvs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: EventGuestCsv) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    registerChangeInEventGuestCsvs() {
        this.eventSubscriber = this.eventManager.subscribe('eventGuestCsvListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
