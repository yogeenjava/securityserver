import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { EventGuestCsv } from './event-guest-csv.model';
import { EventGuestCsvPopupService } from './event-guest-csv-popup.service';
import { EventGuestCsvService } from './event-guest-csv.service';

@Component({
    selector: 'jhi-event-guest-csv-dialog',
    templateUrl: './event-guest-csv-dialog.component.html'
})
export class EventGuestCsvDialogComponent implements OnInit {

    eventGuestCsv: EventGuestCsv;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private eventGuestCsvService: EventGuestCsvService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.eventGuestCsv.id !== undefined) {
            this.subscribeToSaveResponse(
                this.eventGuestCsvService.update(this.eventGuestCsv));
        } else {
            this.subscribeToSaveResponse(
                this.eventGuestCsvService.create(this.eventGuestCsv));
        }
    }

    private subscribeToSaveResponse(result: Observable<EventGuestCsv>) {
        result.subscribe((res: EventGuestCsv) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: EventGuestCsv) {
        this.eventManager.broadcast({ name: 'eventGuestCsvListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-event-guest-csv-popup',
    template: ''
})
export class EventGuestCsvPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventGuestCsvPopupService: EventGuestCsvPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.eventGuestCsvPopupService
                    .open(EventGuestCsvDialogComponent as Component, params['id']);
            } else {
                this.eventGuestCsvPopupService
                    .open(EventGuestCsvDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
