import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { EventGuestCsvComponent } from './event-guest-csv.component';
import { EventGuestCsvDetailComponent } from './event-guest-csv-detail.component';
import { EventGuestCsvPopupComponent } from './event-guest-csv-dialog.component';
import { EventGuestCsvDeletePopupComponent } from './event-guest-csv-delete-dialog.component';

export const eventGuestCsvRoute: Routes = [
    {
        path: 'event-guest-csv',
        component: EventGuestCsvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuestCsv.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'event-guest-csv/:id',
        component: EventGuestCsvDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuestCsv.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const eventGuestCsvPopupRoute: Routes = [
    {
        path: 'event-guest-csv-new',
        component: EventGuestCsvPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuestCsv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-guest-csv/:id/edit',
        component: EventGuestCsvPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuestCsv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-guest-csv/:id/delete',
        component: EventGuestCsvDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.eventGuestCsv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
