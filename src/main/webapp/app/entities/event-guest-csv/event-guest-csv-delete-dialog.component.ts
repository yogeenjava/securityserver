import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { EventGuestCsv } from './event-guest-csv.model';
import { EventGuestCsvPopupService } from './event-guest-csv-popup.service';
import { EventGuestCsvService } from './event-guest-csv.service';

@Component({
    selector: 'jhi-event-guest-csv-delete-dialog',
    templateUrl: './event-guest-csv-delete-dialog.component.html'
})
export class EventGuestCsvDeleteDialogComponent {

    eventGuestCsv: EventGuestCsv;

    constructor(
        private eventGuestCsvService: EventGuestCsvService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.eventGuestCsvService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'eventGuestCsvListModification',
                content: 'Deleted an eventGuestCsv'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-event-guest-csv-delete-popup',
    template: ''
})
export class EventGuestCsvDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventGuestCsvPopupService: EventGuestCsvPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.eventGuestCsvPopupService
                .open(EventGuestCsvDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
