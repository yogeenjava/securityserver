import { BaseEntity } from './../../shared';

export class EventGuestCsv implements BaseEntity {
    constructor(
        public id?: number,
        public fileName?: string,
        public fileData?: any,
    ) {
    }
}
