import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EosSharedModule } from '../../shared';
import {
    EventGuestCsvService,
    EventGuestCsvPopupService,
    EventGuestCsvComponent,
    EventGuestCsvDetailComponent,
    EventGuestCsvDialogComponent,
    EventGuestCsvPopupComponent,
    EventGuestCsvDeletePopupComponent,
    EventGuestCsvDeleteDialogComponent,
    eventGuestCsvRoute,
    eventGuestCsvPopupRoute,
} from './';

const ENTITY_STATES = [
    ...eventGuestCsvRoute,
    ...eventGuestCsvPopupRoute,
];

@NgModule({
    imports: [
        EosSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EventGuestCsvComponent,
        EventGuestCsvDetailComponent,
        EventGuestCsvDialogComponent,
        EventGuestCsvDeleteDialogComponent,
        EventGuestCsvPopupComponent,
        EventGuestCsvDeletePopupComponent,
    ],
    entryComponents: [
        EventGuestCsvComponent,
        EventGuestCsvDialogComponent,
        EventGuestCsvPopupComponent,
        EventGuestCsvDeleteDialogComponent,
        EventGuestCsvDeletePopupComponent,
    ],
    providers: [
        EventGuestCsvService,
        EventGuestCsvPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosEventGuestCsvModule {}
