import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventGuestCsv } from './event-guest-csv.model';
import { EventGuestCsvService } from './event-guest-csv.service';

@Injectable()
export class EventGuestCsvPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private eventGuestCsvService: EventGuestCsvService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.eventGuestCsvService.find(id).subscribe((eventGuestCsv) => {
                    this.ngbModalRef = this.eventGuestCsvModalRef(component, eventGuestCsv);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.eventGuestCsvModalRef(component, new EventGuestCsv());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    eventGuestCsvModalRef(component: Component, eventGuestCsv: EventGuestCsv): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.eventGuestCsv = eventGuestCsv;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
