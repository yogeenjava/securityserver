import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EosSharedModule } from '../../shared';
import {
    FloorService,
    FloorPopupService,
    FloorComponent,
    FloorDetailComponent,
    FloorDialogComponent,
    FloorPopupComponent,
    FloorDeletePopupComponent,
    FloorDeleteDialogComponent,
    floorRoute,
    floorPopupRoute,
} from './';

const ENTITY_STATES = [
    ...floorRoute,
    ...floorPopupRoute,
];

@NgModule({
    imports: [
        EosSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        FloorComponent,
        FloorDetailComponent,
        FloorDialogComponent,
        FloorDeleteDialogComponent,
        FloorPopupComponent,
        FloorDeletePopupComponent,
    ],
    entryComponents: [
        FloorComponent,
        FloorDialogComponent,
        FloorPopupComponent,
        FloorDeleteDialogComponent,
        FloorDeletePopupComponent,
    ],
    providers: [
        FloorService,
        FloorPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosFloorModule {}
