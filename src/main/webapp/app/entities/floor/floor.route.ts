import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { FloorComponent } from './floor.component';
import { FloorDetailComponent } from './floor-detail.component';
import { FloorPopupComponent } from './floor-dialog.component';
import { FloorDeletePopupComponent } from './floor-delete-dialog.component';

export const floorRoute: Routes = [
    {
        path: 'floor',
        component: FloorComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.floor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'floor/:id',
        component: FloorDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.floor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const floorPopupRoute: Routes = [
    {
        path: 'floor-new',
        component: FloorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.floor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'floor/:id/edit',
        component: FloorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.floor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'floor/:id/delete',
        component: FloorDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'eosApp.floor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
