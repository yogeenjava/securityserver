import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Floor } from './floor.model';
import { FloorPopupService } from './floor-popup.service';
import { FloorService } from './floor.service';

@Component({
    selector: 'jhi-floor-dialog',
    templateUrl: './floor-dialog.component.html'
})
export class FloorDialogComponent implements OnInit {

    floor: Floor;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private floorService: FloorService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.floor.id !== undefined) {
            this.subscribeToSaveResponse(
                this.floorService.update(this.floor));
        } else {
            this.subscribeToSaveResponse(
                this.floorService.create(this.floor));
        }
    }

    private subscribeToSaveResponse(result: Observable<Floor>) {
        result.subscribe((res: Floor) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Floor) {
        this.eventManager.broadcast({ name: 'floorListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-floor-popup',
    template: ''
})
export class FloorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private floorPopupService: FloorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.floorPopupService
                    .open(FloorDialogComponent as Component, params['id']);
            } else {
                this.floorPopupService
                    .open(FloorDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
