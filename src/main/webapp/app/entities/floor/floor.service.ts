import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Floor } from './floor.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class FloorService {

    private resourceUrl = SERVER_API_URL + 'api/admin/floors';

    constructor(private http: Http) { }

    create(floor: Floor): Observable<Floor> {
        const copy = this.convert(floor);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(floor: Floor): Observable<Floor> {
        const copy = this.convert(floor);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Floor> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        // debugger;

        if( res && res.json()){
            const jsonResponse = res.json().data;
            const result = [];
            for (let i = 0; i < jsonResponse.length; i++) {
                result.push(this.convertItemFromServer(jsonResponse[i]));
            }
            return new ResponseWrapper(res.headers, result, res.status);
        }
        else{
            alert("No json data found");
            return;
        }
    }

    /**
     * Convert a returned JSON object to Floor.
     */
    private convertItemFromServer(json: any): Floor {
        const entity: Floor = Object.assign(new Floor(), json);
        return entity;
    }

    /**
     * Convert a Floor to a JSON which can be sent to the server.
     */
    private convert(floor: Floor): Floor {
        const copy: Floor = Object.assign({}, floor);
        return copy;
    }
}
