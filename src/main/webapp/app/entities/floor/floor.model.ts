import { BaseEntity } from './../../shared';

export class Floor implements BaseEntity {
    constructor(
        public id?: number,
        public floorNo?: string,
    ) {
    }
}
