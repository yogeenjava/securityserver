import { Injectable } from '@angular/core';
import { SideMenuModel } from './side-menu.model';
@Injectable()
export class SideMenuService {
    sidemenu :SideMenuModel
    constructor() {
        this.sidemenu={};
    }

    setMenu(sidemenu : SideMenuModel){
        this.sidemenu.selectedmenu=sidemenu.selectedmenu;        
    }
    getMenu(){
        return this.sidemenu.selectedmenu;     
    }
}
