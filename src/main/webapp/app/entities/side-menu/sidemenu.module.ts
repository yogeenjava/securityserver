import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EosSharedModule } from '../../shared';
import { SideMenuService} from './';

@NgModule({
    imports: [
        EosSharedModule
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    providers: [
        SideMenuService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SidemenuModule {}
