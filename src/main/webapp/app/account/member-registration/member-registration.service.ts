import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { ResponseWrapper, createRequestOption } from '../../shared';
@Injectable()
export class MemberRegistrationService {

    constructor(private http: Http) {}

    save(keyAndPassword: any): Observable<any> {
        return this.http.post(SERVER_API_URL + 'api/eos-members/register', keyAndPassword).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse.status;
        });
    }
}
