import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { MemberRegistrationComponent } from './member-registration.component';

export const MemberRegistrationRoute: Route = {
    path: 'activate',
    component: MemberRegistrationComponent,
    data: {
        authorities: [],
        pageTitle: 'global.menu.memberregistration.title'
    },
    canActivate: [UserRouteAccessService]
};
