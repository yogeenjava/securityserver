import { Injectable } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';
import { Observable } from 'rxjs/Rx';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';
import { ResponseWrapper } from '../../shared';
import { Http, Response } from '@angular/http';
import { JhiDateUtils } from 'ng-jhipster';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'

import { SERVER_API_URL } from '../../app.constants';


@Injectable()
export class LoginService {

    constructor(
        private languageService: JhiLanguageService,
        private principal: Principal,
        private authServerProvider: AuthServerProvider,
        private http: Http,
        private dateUtils: JhiDateUtils
        , private httpClient: HttpClient
    ) { }

    login(credentials, callback?) {
        const cb = callback || function () { };
        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe((data) => {
                this.principal.identity(true).then((account) => {
                    resolve(data);
                });
                if (data === false) {
                    reject("false");
                }
                else {
                    return cb();
                }

            }, (err) => {
                this.logout();
                reject(err);
                return cb(err);
            });
        });
    }
    loginWithToken(jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe);
    }

    logout() {
        this.authServerProvider.logout().subscribe();
        this.principal.authenticate(null);
    }

    getIps(req?: any): Observable<ResponseWrapper> {
        // const options = createRequestOption(req);
        return this.http.get(SERVER_API_URL + 'api/admin/authenticate_ip')
            .map((res: Response) => {
                const status = res.json();
                return status.status;
            });
    }
}
