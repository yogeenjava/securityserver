import { Component, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { JhiEventManager, JhiAlertService, } from 'ng-jhipster';
import { Principal, Account, LoaderService } from '../../shared';
import { LoginService } from './login.service';
import { StateStorageService } from '../../shared/auth/state-storage.service';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

import { UserService } from '../../shared/user/user.service'
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],

})
export class LoginComponent {
    authenticationError: boolean;
    isFormSubmited: boolean;
    password: string;
    rememberMe: boolean;
    username: string;
    credentials: any;
    error: string;
    account: Account;
    checkuser: boolean;
    private clientIp: string;
    authorized: boolean;
    private ipAddress: string;
    constructor(
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private stateStorageService: StateStorageService,
        private elementRef: ElementRef,
        private alertService: JhiAlertService,
        private renderer: Renderer,
        private router: Router,
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private loaderService: LoaderService,
        private userService: UserService

    ) {
        this.authorized = false;
        this.credentials = {};
        this.clientIp = "";
    }

    ngOnInit() {
        this.loginService.getIps().subscribe((res) => {
            let status: any = res;
            this.authorized = status;
        },
            (err) => {
                this.authorized = false ;
                this.redirectErrorPage();
            }
        );

        let body = document.getElementsByTagName('body')[0];
        body.classList.add('body-color');

    }

    ngOnDestroy() {
        let body = document.getElementsByTagName('body')[0];
        body.classList.remove("body-color");
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#email'), 'focus', []);
    }

    cancel() {
        this.credentials = {
            username: null,
            password: null,
            rememberMe: true
        };
        this.authenticationError = false;
        this.activeModal.dismiss('cancel');
    }

    login(form: any) {

        this.isFormSubmited = true;
        if (!form.valid) { return; }

        this.loaderService.show();
        this.loginService.login({
            username: this.username,
            password: this.password,
            rememberMe: this.rememberMe
        }).then(() => {
            this.loaderService.hide();
            this.authenticationError = false;
            this.activeModal.dismiss('login success');
            this.principal.identity().then((account) => {
                for (let i = 0; i < account.authorities.length; i++) {
                    if (account.authorities[i] !== "ROLE_ADMIN") {
                        this.checkuser = true;
                    }
                    else {
                        this.checkuser = false;
                    }
                }
                if (this.checkuser === true) {
                    this.alertService.error("login.messages.error.authentication", null, null);
                }
                this.isFormSubmited = true;
                this.account = account;
                this.loaderService.hide();
                this.router.navigate(['members']);
            });
            this.eventManager.broadcast({
                name: 'authenticationSuccess',
                content: 'Sending Authentication Success'
            });
            // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // // since login is succesful, go to stored previousState and clear previousState
            const redirect = this.stateStorageService.getUrl();
            if (redirect) {
                this.stateStorageService.storeUrl(null);
                this.router.navigate([redirect]);
            }
        }, (err: any) => {
            this.loaderService.hide();
            let body = JSON.parse(err._body);
            let error = body.AuthenticationException;
            this.alertService.error("login.messages.error.authentication", null, null);
        }).catch(() => {
            this.loaderService.hide();
            this.authenticationError = true;
            this.alertService.error("login.messages.error.authentication", null, null);



        });
    }
    requestResetPassword() {
        this.activeModal.dismiss('to state requestReset');
        this.router.navigate(['/reset', 'request']);
    }

    redirectErrorPage() {
        this.activeModal.dismiss('to state requestReset');
        this.router.navigate(['/error','bad_request']);
    }
}
