import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EosSharedModule } from '../shared';

import {
    Register,
    LOGIN_ROUTE,
    ActivateService,
    PasswordService,
    MemberRegistrationService,
    PasswordResetInitService,
    PasswordResetFinishService,
    PasswordStrengthBarComponent,
    RegisterComponent,
    ActivateComponent,
    MemberRegistrationComponent,
    LoginComponent,
    ErrorComponent,
    PasswordComponent,
    PasswordResetInitComponent,
    PasswordResetFinishComponent,
    SettingsComponent,
    LoginService,
    accountState
} from './';

@NgModule({
    imports: [
        EosSharedModule,
        RouterModule.forRoot(accountState, { useHash: true })
    ],
    declarations: [
        ActivateComponent,
        RegisterComponent,
        LoginComponent,
        ErrorComponent,
        PasswordComponent,
        PasswordStrengthBarComponent,
        PasswordResetInitComponent,
        PasswordResetFinishComponent,
        MemberRegistrationComponent,
        SettingsComponent
    ],
    providers: [
        Register,
        ActivateService,
        MemberRegistrationService,
        LoginService,
        PasswordService,
        PasswordResetInitService,
        PasswordResetFinishService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosAccountModule {}
