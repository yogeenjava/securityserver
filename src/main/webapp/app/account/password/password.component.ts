import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Principal, LoaderService } from '../../shared';
import { PasswordService } from './password.service';
import { Router } from '@angular/router';
import { StateStorageService } from '../../shared/auth/state-storage.service';
import { JhiAlertService } from 'ng-jhipster';
@Component({
    selector: 'jhi-password',
    templateUrl: './password.component.html'
})
export class PasswordComponent implements OnInit {
    doNotMatch: string;
    error: string;
    success: string;
    account: any;
    password: string;
    confirmPassword: string;
    isFormSubmited: boolean;
    oldpassword: string;
    constructor(
        private passwordService: PasswordService,
        private principal: Principal,
        private router: Router,
        private stateStorageService: StateStorageService,
        private alertService: JhiAlertService,
        private loaderService: LoaderService,

    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
    }

    changePassword(form: any) {
        this.isFormSubmited = true;
        if (!this.checkpassword()) { return; }
        if (!this.checkOldPassword()) { return; }
        if (!this.checkforspace()) { return; }
        if (!form.valid) { return; }
        this.doNotMatch = null;
        this.loaderService.show();
        this.passwordService.save({ oldPassword: this.oldpassword, newPassword: this.password })
            .subscribe((data) => {
                if (data == true) {
                    this.loaderService.hide();
                    this.error = null;
                    this.success = 'OK';
                    this.isFormSubmited = false;
                    this.alertService.success("global.messages.password.passwordsuccess");
                    window.history.back();

                } else {
                    this.loaderService.hide();
                    this.isFormSubmited = false;
                    this.success = null;
                    this.alertService.error("error.wrongpassword", null, null);
                    this.error = 'ERROR';
                }
            }, (error) => {
                this.loaderService.hide();
                this.isFormSubmited = false;
                if (error) {
                    let body = JSON.parse(error._body);
                    if (body.message) {
                        this.alertService.error(body.message, null, null);
                    } else {
                        this.alertService.error("error.internalServerError", null, null);
                    }
                } else {
                    this.success = null;
                    this.error = 'ERROR';
                }
            });

    }
    checkpassword() {
        if (this.password !== this.confirmPassword) {
            return false;
        } else {
            return true;
        }
    }

    checkOldPassword() {
        if (this.password == this.oldpassword) {
            return false;
        } else {
            return true;
        }
    }

    gotoback() {
        // const redirect = this.stateStorageService.getUrl();
        // if (redirect) {
        // this.stateStorageService.storeUrl(null);
        window.history.back();
        // }
    }
    checkforspace() {
        if (this.password != null) {
            let value = this.password;
            if (value.trim().length <= 0) {
                return false;
            } else {
                return true;
            }
        }
    }
}
