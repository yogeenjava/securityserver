import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

@Injectable()
export class PasswordService {

    constructor(private http: Http) {}

    save(newPassword: any): Observable<any> {
        return this.http.post(SERVER_API_URL + 'api/admin/account/change-password', newPassword).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse.status;
        });
    }
}