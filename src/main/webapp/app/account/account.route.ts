import { Routes } from '@angular/router';

import {
    activateRoute,
    // passwordRoute,
    passwordResetFinishRoute,
    passwordResetInitRoute,
    registerRoute,
    // MemberRegistrationRoute,
    // LOGIN_ROUTE,
    settingsRoute
} from './';
import {LOGIN_ROUTE} from './login/login.route';
import {ERROR_ROUTE } from './error/error.route'
import {MemberRegistrationRoute} from './member-registration/member-registration.route';
const ACCOUNT_ROUTES = [
    activateRoute,
    // passwordRoute,
    passwordResetFinishRoute,
    passwordResetInitRoute,
    MemberRegistrationRoute,
    LOGIN_ROUTE,
    ERROR_ROUTE,
    registerRoute,
    settingsRoute
];

export const accountState: Routes = [{
    path: '',
    children: ACCOUNT_ROUTES
}];
