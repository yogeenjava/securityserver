import { Component, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { JhiEventManager, JhiAlertService, } from 'ng-jhipster';
import { Principal, Account, LoaderService } from '../../shared';
import { StateStorageService } from '../../shared/auth/state-storage.service';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Title} from '@angular/platform-browser';

@Component({
    selector: 'jhi-error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.css'],

})
export class ErrorComponent {

    constructor(private loaderService: LoaderService, private titleService: Title) {

    }

    ngOnInit() {
        this.titleService.setTitle("Error 400 page");
        this.loaderService.hide();
        let body = document.getElementsByTagName('body')[0];
        body.classList.add('body-color');
    }

    ngOnDestroy() {
        let body = document.getElementsByTagName('body')[0];
        body.classList.remove("body-color");
    }

}
