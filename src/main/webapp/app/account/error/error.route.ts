import { Route } from '@angular/router';
import { UserRouteAccessService } from '../../shared';

import { ErrorComponent } from './error.component';

export const ERROR_ROUTE: Route = {
    path: 'error/bad_request',
    component: ErrorComponent,
    data: {
        authorities: [],
        pageTitle: 'global.menu.account.error'
    }   
};
