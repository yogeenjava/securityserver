import { Component, OnInit, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { JhiEventManager, JhiAlertService, } from 'ng-jhipster';
import { PasswordResetInitService } from './password-reset-init.service';
import { EMAIL_NOT_FOUND_TYPE, LoaderService, ResponseWrapper } from '../../../shared';
import { Router } from '@angular/router';
import { ForgetPassword } from '../../../entities/account';
import { LoginService } from '../../login/login.service'

import { UserService } from '../../../shared/user/user.service'


@Component({
    selector: 'jhi-password-reset-init',
    templateUrl: './password-reset-init.component.html'
})
export class PasswordResetInitComponent implements OnInit, AfterViewInit {
    error: string;
    errorEmailNotExists: string;
    forgetpassword: ForgetPassword;
    success: string;
    isFormSubmited: boolean;
    private clientIp: string;
    authorized: boolean;
    private ipAddress: string;
    constructor(
        private passwordResetInitService: PasswordResetInitService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private router: Router,
        private alertService: JhiAlertService,
        private loaderService: LoaderService,
        private loginService: LoginService,
        private userService: UserService
    ) {
        this.authorized = false;
    }


    ngOnDestroy() {
        let body = document.getElementsByTagName('body')[0];
        body.classList.remove("body-color");
    }
    ngOnInit() {
        this.loginService.getIps().subscribe((res) => {
            let status: any = res;
            this.authorized = status;
        },
            (err) => {
                this.authorized = false;
                this.redirectErrorPage();
            }
        );
        this.forgetpassword = {};
        let body = document.getElementsByTagName('body')[0];
        body.classList.add('body-color');
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#email'), 'focus', []);
    }

    requestReset(form: any) {
        this.isFormSubmited = true;
        if (!form.valid) { return; }
        this.error = null;
        this.errorEmailNotExists = null;
        this.loaderService.show();
        this.passwordResetInitService.save(this.forgetpassword)
            .subscribe((data) => {
                // this.success = 'OK';
                if (data == false) {
                    this.loaderService.hide();
                    this.success = null;
                    this.errorEmailNotExists = 'ERROR';
                    this.alertService.error("reset.request.messages.notfound", null, null);
                    // this.error = 'ERROR';
                } else {
                    this.loaderService.hide();
                    this.success = 'OK';
                    this.isFormSubmited = true;
                    this.alertService.success("reset.request.messages.success", );

                }
            }, (response) => {
                this.loaderService.hide();
                this.success = null;
                if (response.status === 400 && response.json().type === EMAIL_NOT_FOUND_TYPE) {
                    this.alertService.error("reset.request.messages.notfound", null, null);
                    // this.errorEmailNotExists = 'ERROR';
                } else {
                    // this.error = 'ERROR';
                    this.alertService.error("reset.request.messages.notfound", null, null);

                }
            });
    }
    backtologin() {
        this.router.navigate(['login']);
    }

    redirectErrorPage() {
        this.router.navigate(['/error','bad_request']);
    }
}
