import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../../app.constants';
import { ResponseWrapper, createRequestOption } from '../../../shared';
import { ForgetPassword } from '../../../entities/account';
@Injectable()
export class PasswordResetInitService {

    constructor(private http: Http) {}

    save(forgetpassword:ForgetPassword): Observable<any> {
        const copy = this.convert(forgetpassword);
        return this.http.post(SERVER_API_URL + 'api/admin/account/reset-password/init/validateEmail', forgetpassword).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse.status;
        });
    }
    private convert(forgetpassword: ForgetPassword): ForgetPassword {
        const copy: ForgetPassword = Object.assign({}, forgetpassword);
        return copy;
    }
}
