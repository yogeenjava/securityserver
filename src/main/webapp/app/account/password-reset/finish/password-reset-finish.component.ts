import { Component, OnInit, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { PasswordResetFinishService } from './password-reset-finish.service';
import { LoginModalService,LoaderService } from '../../../shared';
import { Router } from '@angular/router';

@Component({
    selector: 'jhi-password-reset-finish',
    templateUrl: './password-reset-finish.component.html'
})
export class PasswordResetFinishComponent implements OnInit, AfterViewInit {
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    keyMissing: boolean;
    resetAccount: any;
    success: string;
    modalRef: NgbModalRef;
    key: string;
    isFormSubmited :boolean;
    constructor(
        private passwordResetFinishService: PasswordResetFinishService,
        private loginModalService: LoginModalService,
        private route: ActivatedRoute,
        private elementRef: ElementRef, private renderer: Renderer,
        private router: Router,
        private alertService: JhiAlertService,
        private loaderService: LoaderService,
        
    ) {
        
    }

    ngOnInit() {
        this.route.queryParams.subscribe((params) => {
            this.key = params['key'];
        });
        this.resetAccount = {};
        this.keyMissing = !this.key;
    }

    ngAfterViewInit() {
        if (this.elementRef.nativeElement.querySelector('#password') != null) {
          this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#password'), 'focus', []);
        }
    }
    checkforspace() {
        if(this.resetAccount.password !=null){
            let value =this.resetAccount.password ;
            if(value.trim().length <= 0) {
                return false;
            } else {            
                return true;   
            }
        }
    }
    checkpassword() {
        if (this.resetAccount.password !== this.confirmPassword) {
            return false;
        } else {
            return true;
        }
    }
    finishReset(form:any) {
        this.isFormSubmited = true;
        if(!this.checkforspace()){return;}
        if (!this.checkpassword()) { return; }
        if (!form.valid) { return; }
        this.doNotMatch = null;
        this.error = null;
        this.loaderService.show();
        this.passwordResetFinishService.save({key: this.key, newPassword: this.resetAccount.password})
        .subscribe((data) => {  
            if(data == false)  {
                this.loaderService.hide();            
                this.success = null;
                this.error = 'ERROR';
                this.alertService.error("reset.finish.messages.errorreset", null, null);
            } else{    
                this.loaderService.hide();     
                this.success = 'OK';
            }
        }, (error) => {
            this.loaderService.hide();
            if (error) {
                let body = JSON.parse(error._body);
                if (body.message) {
                    this.alertService.error(body.message, null, null);
                } else {
                    this.alertService.error("error.internalServerError", null, null);
                }
            } else {
                this.success = null;
                this.error = 'ERROR';
            }
        });
    }
    // login() {
    //     this.router.navigate(['login']);
    // }
}
