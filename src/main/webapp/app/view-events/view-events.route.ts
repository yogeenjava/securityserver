import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { ViewEventsComponent } from './';
import { SideMenuComponent} from '../side-menu'

export const VIEW_EVENTS_ROUTE: Route = {
    path: 'events',
    component: ViewEventsComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    },
    children: [
        {
            path: '', component: SideMenuComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'home'
            },
            canActivate: [UserRouteAccessService]
        },
    ]
};