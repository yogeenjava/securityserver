import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VIEW_EVENTS_ROUTE, ViewEventsComponent } from './';
import { EosSharedModule } from '../shared';

@NgModule({
    imports: [
        RouterModule.forRoot([ VIEW_EVENTS_ROUTE ], { useHash: true }),
        EosSharedModule
    ],
    declarations: [
        ViewEventsComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EosViewEventsModule {}
