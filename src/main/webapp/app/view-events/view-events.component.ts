import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { JhiAlertService, JhiParseLinks, JhiEventManager } from 'ng-jhipster';
import { ResponseWrapper, ITEMS_PER_PAGE } from '../shared';
import { DomSanitizer } from '@angular/platform-browser';
import { Principal, LoaderService } from '../shared';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { EventService, Event } from '../entities/event'
import { Subscription } from 'rxjs/Rx';
import { SideMenuModel } from '../entities/side-menu/side-menu.model';
import { SideMenuService } from '../entities/side-menu';

@Component({
    selector: 'app-view-events',
    templateUrl: './view-events.component.html',
    styleUrls: [
        'view-events.component.css'
    ]
})
export class ViewEventsComponent implements OnInit {
    isRes: boolean;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    reverse: any;
    totalItems: number;
    private isAdmin: boolean;
    events: Event[];
    eventSubscriber: Subscription;
    sidemenu: SideMenuModel;
    pages: Array<number>;
    isFirst: boolean;
    isLast: boolean;
    first: number;
    last: number;
    constructor(
        private alertService: JhiAlertService,
        private parseLinks: JhiParseLinks,
        private _sanitizer: DomSanitizer,
        private principal: Principal,
        private router: Router,
        private eventService: EventService,
        private sidemenuservice: SideMenuService,
        private eventManager: JhiEventManager,
        private loaderService: LoaderService,
        // private pagerService: PagerService,

    ) {
        this.isRes = false;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.first = 1;
        this.last = 5;
        this.predicate = 'activated';
        this.reverse = false;
        this.events = [];
        this.sidemenu = {};
        this.isFirst = true;
        this.isLast = false;
        this.pages = [1];

    }

    ngOnInit() {
        this.setMenu();
        this.getEvents();
        this.registerChangeInEvents();
    }
    setMenu() {

        this.sidemenu.selectedmenu = "EVENTS";
        this.sidemenuservice.setMenu(this.sidemenu);
    }
    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEvents() {
        this.eventSubscriber = this.eventManager.subscribe('eventListModification', (response) => {
            this.events = [];
            this.getEvents();
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'fromDate') {
            result.push('fromDate');
        }
        return result;
    }
    getEvents() {

        this.loaderService.show();
        this.eventService.query({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => {
                let data: any = res.json;
                let headers: any = res.headers;
                this.isRes = true;
                this.links = this.parseLinks.parse(headers.get('link'));
                this.totalItems = headers.get('X-Total-Count');
                this.pages = new Array(headers.get('page'));
                if (this.page == 0) {
                    this.isFirst = true;
                }
                else {
                    this.isFirst = false;
                }
                if (this.page == this.pages.length - 1) {
                    this.isLast = true;
                }
                else {
                    this.isLast = false;
                }
                for (let i = 0; i < data.length; i++) {
                    this.events.push(data[i]);
                }
                if (this.page > 0 && this.events.length <= 0) {
                    this.page = this.page - 1;
                    this.getEvents();
                }

                this.loaderService.hide();
            },
            (err: ResponseWrapper) => {
                if (err.status == 400) {
                    this.loaderService.hide();
                    this.redirectErrorPage();
                }
                this.loaderService.hide();
                let error: any = err.json;
                this.alertService.error(error);
            }
            );
    }
    setPage(i, event: any) {
        this.page = i;
        this.events = [];
        this.getEvents();
    }
    onNext(): void {
        if (this.page !== this.pages.length - 1) {
            this.page++;
            this.events = [];
            this.getEvents();
        }
        this.last = (~~(this.page / 5)) * 5 + 5;
        this.first = (~~(this.page / 5)) * 5 + 1;
    }

    onPrev(): void {
        if (this.page > 0) {
            this.page--;
            this.events = [];
            this.getEvents();
        }
        this.last = (~~(this.page / 5)) * 5 + 5;
        this.first = (~~(this.page / 5)) * 5 + 1;
    }


    editEvent(id: any) {
        this.loaderService.show();
        this.router.navigate(['/edit-event/' + id]);
    }

    deleteEvent(id: any, index: any) {
        this.eventService.delete(id).subscribe(
            (res: ResponseWrapper) => {
                this.events.splice(index, 1);
                this.alertService.success("eosApp.event.deleted", id, null);
            },
            (err: Response) => {
                let error: any = err.json;
                this.alertService.error(error);
            }
        );
    }

    redirectErrorPage() {
        this.router.navigate(['/error', 'bad_request']);
    }
}
