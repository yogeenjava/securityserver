import { Route } from '@angular/router';
import { UserRouteAccessService } from '../shared';
import { ViewMembersComponent } from '../view-member';
import { SideMenuComponent} from '../side-menu'
import { ViewEventsComponent }from '../view-events';
export const SIDEMENU_ROUTE: Route = {
    path: '', component: SideMenuComponent,
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'home.events'
    },
    children: [
        {
            path: 'events', component: ViewEventsComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'home.events'
            },
            canActivate: [UserRouteAccessService]
        },
        {
            path: 'members', component: ViewMembersComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'home.members'
            },
            canActivate: [UserRouteAccessService],
        },
    ]
};     
   
  