import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EosSharedModule } from '../shared';
import { SideMenuComponent } from './';

@NgModule({
    imports: [
        EosSharedModule,
    ],
    declarations: [
        SideMenuComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SideMenuModule {}
