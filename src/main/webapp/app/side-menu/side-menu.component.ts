import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager,JhiAlertService } from 'ng-jhipster';
import { Router,ActivatedRoute } from '@angular/router';
import { Account, LoginModalService, Principal ,ResponseWrapper } from '../shared';
import { LoginService } from '../account/login/login.service';
import { SideMenuModel } from '../entities/side-menu/side-menu.model';
import { SideMenuService } from '../entities/side-menu';

@Component({
    selector: 'jhi-sidemenu',
    templateUrl: './side-menu.component.html',
    styleUrls: [
        'side-menu.css'
    ]
    
})
export class SideMenuComponent implements OnInit {
    sidemenu :SideMenuModel;
    constructor(
        private router: Router,
        private sidemenuservice: SideMenuService,
    ){
        this.sidemenu={};
    }
    ngOnInit() {
        this.sidemenu.selectedmenu = this.sidemenuservice.getMenu();
    }
    goToAddMember() {
        this.router.navigate(['members']);
        // this.router.navigate(['/']);
    }
    gotoEvents(){
        this.router.navigate(['events']);        
    }
}
