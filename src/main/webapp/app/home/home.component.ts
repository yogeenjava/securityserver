import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { Account, LoginModalService, Principal, ResponseWrapper } from '../shared';
import { LoginService } from '../account/login/login.service';
import { UserService } from '../shared/user/user.service';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.css'
    ]

})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    code: string;
    showAdminMenu: boolean;
    authorized: boolean;
    private clientIP: string;
    private ipAddress: string;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private router: Router,
        private route: ActivatedRoute,
        private loginService: LoginService,
        private alertService: JhiAlertService,
        private userService: UserService
    ) {
        this.authorized = false;
        this.loginService.getIps().subscribe((res) => {
            let status: any = res;
            this.authorized = status;
            this.getAccount();
        },
            (err) => {
                this.authorized = false;
                this.redirectErrorPage();
            }
        );
        this.code = null;
        this.showAdminMenu = false;
    }
    ngOnInit() {
    }

    getAccount() {
        this.principal.identity().then((account) => {
            this.account = account;
            if (!this.account) {
                this.router.navigate(['/login']);
                return;
            } else {
                if (this.router && this.router.url && this.router.url == '/') {
                    this.router.navigate(['members']);
                }
                this.route.queryParams.subscribe((params) => {
                    this.code = params['code'];
                    if (this.code) {
                        this.router.navigate(['members']);
                    }
                });
            }
        });
    }


    isAuthenticated() {
        return this.principal.isAuthenticated();
    }
    releaseAllPicker($event) {
        this.eventManager.broadcast({
            name: 'releaseAllPicker',
            content: { $event: event }
        })
    }
    login() {
        this.router.navigate(['login']);
    }
    logout() {
        this.loginService.logout();
        this.router.navigate(['login']);
    }
    goToHome() {
        this.router.navigate(['members']);
    }
    changepassword() {
        this.router.navigate(['changepassword']);
    }

    redirectErrorPage() {
        this.router.navigate(['/error', 'bad_request']);
    }
}
