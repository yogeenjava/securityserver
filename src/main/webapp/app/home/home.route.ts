import { Route } from '@angular/router';
import { MemberCreateUpdateComponent } from '../member-create-update';
import { UserRouteAccessService } from '../shared';
import { ViewMembersComponent } from '../view-member';
import { HomeComponent } from './';
import { SideMenuComponent} from '../side-menu'
import { PasswordComponent } from '../account/password/password.component';
import { ViewEventsComponent }from '../view-events';
import { EventAddEditComponent} from '../event-add-edit';
export const HOME_ROUTE: Route = {
    path: '',
    component: HomeComponent,
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'home.title'
    },
    children: [
        {
            path: 'member-create', component: MemberCreateUpdateComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'home.addmember'
            },
            canActivate: [UserRouteAccessService]
        },
        {
            path: 'add-event',
            component: EventAddEditComponent,
            data: {
                authorities: [],
                pageTitle: 'home.addevent'
            },
            canActivate: [UserRouteAccessService]
        },
        {
            path: 'edit-event/:id',
            component: EventAddEditComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'home.editevent'
            },
            canActivate: [UserRouteAccessService]
        },        
        {
            path: 'changepassword',
            component: PasswordComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'global.menu.account.password'
            },
            canActivate: [UserRouteAccessService]
        },
        {
            path: 'members', component: ViewMembersComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'home.members'
            },
            children: [
                {
                    path: '', component: SideMenuComponent,
                    data: {
                        authorities: ['ROLE_ADMIN'],
                        pageTitle: 'home.members'
                    },
                    canActivate: [UserRouteAccessService]
                },
            ]
        },
        {
            path: 'events', component: ViewEventsComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'home.events'
            },
            children: [
                {
                    path: '', component: SideMenuComponent,
                    data: {
                        authorities: ['ROLE_ADMIN'],
                        pageTitle: 'home.events'
                    },
                    canActivate: [UserRouteAccessService]
                },
            ]
        },
        {
            path: 'edit-member/:id', component: MemberCreateUpdateComponent,
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'home.editmember'
            },
            canActivate: [UserRouteAccessService]
        },
        // {
        //     path: '', component: SideMenuComponent,
        //     data: {
        //         authorities: ['ROLE_ADMIN'],
        //         pageTitle: 'home.events'
        //     },
        //     children: [
        //         {
        //             path: 'events', component: ViewEventsComponent,
        //             data: {
        //                 authorities: ['ROLE_ADMIN'],
        //                 pageTitle: 'home.events'
        //             },
        //             canActivate: [UserRouteAccessService]
        //         },
        //         {
        //             path: 'members', component: ViewMembersComponent,
        //             data: {
        //                 authorities: ['ROLE_ADMIN'],
        //                 pageTitle: 'home.members'
        //             },
        //             canActivate: [UserRouteAccessService],
        //         },
        //     ]
        // },     
    ],
    
};
