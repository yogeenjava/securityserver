/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { EosTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SoundCodeDetailComponent } from '../../../../../../main/webapp/app/entities/sound-code/sound-code-detail.component';
import { SoundCodeService } from '../../../../../../main/webapp/app/entities/sound-code/sound-code.service';
import { SoundCode } from '../../../../../../main/webapp/app/entities/sound-code/sound-code.model';

describe('Component Tests', () => {

    describe('SoundCode Management Detail Component', () => {
        let comp: SoundCodeDetailComponent;
        let fixture: ComponentFixture<SoundCodeDetailComponent>;
        let service: SoundCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [EosTestModule],
                declarations: [SoundCodeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SoundCodeService,
                    JhiEventManager
                ]
            }).overrideTemplate(SoundCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SoundCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SoundCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new SoundCode(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.soundCode).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
