/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { EosTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { EosMemberDetailComponent } from '../../../../../../main/webapp/app/entities/eos-member/eos-member-detail.component';
import { EosMemberService } from '../../../../../../main/webapp/app/entities/eos-member/eos-member.service';
import { EosMember } from '../../../../../../main/webapp/app/entities/eos-member/eos-member.model';

describe('Component Tests', () => {

    describe('EosMember Management Detail Component', () => {
        let comp: EosMemberDetailComponent;
        let fixture: ComponentFixture<EosMemberDetailComponent>;
        let service: EosMemberService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [EosTestModule],
                declarations: [EosMemberDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    EosMemberService,
                    JhiEventManager
                ]
            }).overrideTemplate(EosMemberDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EosMemberDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EosMemberService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new EosMember(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.eosMember).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
