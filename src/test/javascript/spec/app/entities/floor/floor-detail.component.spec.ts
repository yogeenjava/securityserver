/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { EosTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { FloorDetailComponent } from '../../../../../../main/webapp/app/entities/floor/floor-detail.component';
import { FloorService } from '../../../../../../main/webapp/app/entities/floor/floor.service';
import { Floor } from '../../../../../../main/webapp/app/entities/floor/floor.model';

describe('Component Tests', () => {

    describe('Floor Management Detail Component', () => {
        let comp: FloorDetailComponent;
        let fixture: ComponentFixture<FloorDetailComponent>;
        let service: FloorService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [EosTestModule],
                declarations: [FloorDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    FloorService,
                    JhiEventManager
                ]
            }).overrideTemplate(FloorDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(FloorDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FloorService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Floor(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.floor).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
