/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { EosTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { EventGuestDetailComponent } from '../../../../../../main/webapp/app/entities/event-guest/event-guest-detail.component';
import { EventGuestService } from '../../../../../../main/webapp/app/entities/event-guest/event-guest.service';
import { EventGuest } from '../../../../../../main/webapp/app/entities/event-guest/event-guest.model';

describe('Component Tests', () => {

    describe('EventGuest Management Detail Component', () => {
        let comp: EventGuestDetailComponent;
        let fixture: ComponentFixture<EventGuestDetailComponent>;
        let service: EventGuestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [EosTestModule],
                declarations: [EventGuestDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    EventGuestService,
                    JhiEventManager
                ]
            }).overrideTemplate(EventGuestDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EventGuestDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EventGuestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new EventGuest(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.eventGuest).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
