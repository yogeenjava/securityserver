/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { EosTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { EventGuestCsvDetailComponent } from '../../../../../../main/webapp/app/entities/event-guest-csv/event-guest-csv-detail.component';
import { EventGuestCsvService } from '../../../../../../main/webapp/app/entities/event-guest-csv/event-guest-csv.service';
import { EventGuestCsv } from '../../../../../../main/webapp/app/entities/event-guest-csv/event-guest-csv.model';

describe('Component Tests', () => {

    describe('EventGuestCsv Management Detail Component', () => {
        let comp: EventGuestCsvDetailComponent;
        let fixture: ComponentFixture<EventGuestCsvDetailComponent>;
        let service: EventGuestCsvService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [EosTestModule],
                declarations: [EventGuestCsvDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    EventGuestCsvService,
                    JhiEventManager
                ]
            }).overrideTemplate(EventGuestCsvDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EventGuestCsvDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EventGuestCsvService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new EventGuestCsv(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.eventGuestCsv).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
