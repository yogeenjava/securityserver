//package com.eos.web.rest;
//
//import static com.eos.web.rest.TestUtil.sameInstant;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.time.Instant;
//import java.time.ZoneId;
//import java.time.ZoneOffset;
//import java.time.ZonedDateTime;
//import java.util.List;
//
//import javax.persistence.EntityManager;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.eos.EosApp;
//import com.eos.domain.EosMember;
//import com.eos.repository.EosMemberRepository;
//import com.eos.service.EosMemberService;
//import com.eos.service.dto.EosMemberDTO;
//import com.eos.service.mapper.EosMemberMapper;
//import com.eos.web.rest.errors.ExceptionTranslator;
//
///**
// * Test class for the EosMemberResource REST controller.
// *
// * @see EosMemberResource
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = EosApp.class)
//public class EosMemberResourceIntTest {
//
//    private static final ZonedDateTime DEFAULT_FROM_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
//    private static final ZonedDateTime UPDATED_FROM_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
//
//    private static final ZonedDateTime DEFAULT_TO_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
//    private static final ZonedDateTime UPDATED_TO_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
//
//    @Autowired
//    private EosMemberRepository eosMemberRepository;
//
//    @Autowired
//    private EosMemberMapper eosMemberMapper;
//
//    @Autowired
//    private EosMemberService eosMemberService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    private MockMvc restEosMemberMockMvc;
//
//    private EosMember eosMember;
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//		final EosMemberResource eosMemberResource = new EosMemberResource(eosMemberService, null, null, null, null, null);
//        this.restEosMemberMockMvc = MockMvcBuilders.standaloneSetup(eosMemberResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setMessageConverters(jacksonMessageConverter).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static EosMember createEntity(EntityManager em) {
//        EosMember eosMember = new EosMember()
//            .fromDate(DEFAULT_FROM_DATE)
//            .toDate(DEFAULT_TO_DATE);
//        return eosMember;
//    }
//
//    @Before
//    public void initTest() {
//        eosMember = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createEosMember() throws Exception {
//        int databaseSizeBeforeCreate = eosMemberRepository.findAll().size();
//
//        // Create the EosMember
//        EosMemberDTO eosMemberDTO = eosMemberMapper.toDto(eosMember);
//        restEosMemberMockMvc.perform(post("/api/eos-members")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(eosMemberDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the EosMember in the database
//        List<EosMember> eosMemberList = eosMemberRepository.findAll();
//        assertThat(eosMemberList).hasSize(databaseSizeBeforeCreate + 1);
//        EosMember testEosMember = eosMemberList.get(eosMemberList.size() - 1);
//        assertThat(testEosMember.getFromDate()).isEqualTo(DEFAULT_FROM_DATE);
//        assertThat(testEosMember.getToDate()).isEqualTo(DEFAULT_TO_DATE);
//    }
//
//    @Test
//    @Transactional
//    public void createEosMemberWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = eosMemberRepository.findAll().size();
//
//        // Create the EosMember with an existing ID
//        eosMember.setId(1L);
//        EosMemberDTO eosMemberDTO = eosMemberMapper.toDto(eosMember);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restEosMemberMockMvc.perform(post("/api/eos-members")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(eosMemberDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the EosMember in the database
//        List<EosMember> eosMemberList = eosMemberRepository.findAll();
//        assertThat(eosMemberList).hasSize(databaseSizeBeforeCreate);
//    }
//
//    @Test
//    @Transactional
//    public void getAllEosMembers() throws Exception {
//        // Initialize the database
//        eosMemberRepository.saveAndFlush(eosMember);
//
//        // Get all the eosMemberList
//        restEosMemberMockMvc.perform(get("/api/eos-members?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(eosMember.getId().intValue())))
//            .andExpect(jsonPath("$.[*].fromDate").value(hasItem(sameInstant(DEFAULT_FROM_DATE))))
//            .andExpect(jsonPath("$.[*].toDate").value(hasItem(sameInstant(DEFAULT_TO_DATE))));
//    }
//
//    @Test
//    @Transactional
//    public void getEosMember() throws Exception {
//        // Initialize the database
//        eosMemberRepository.saveAndFlush(eosMember);
//
//        // Get the eosMember
//        restEosMemberMockMvc.perform(get("/api/eos-members/{id}", eosMember.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(eosMember.getId().intValue()))
//            .andExpect(jsonPath("$.fromDate").value(sameInstant(DEFAULT_FROM_DATE)))
//            .andExpect(jsonPath("$.toDate").value(sameInstant(DEFAULT_TO_DATE)));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingEosMember() throws Exception {
//        // Get the eosMember
//        restEosMemberMockMvc.perform(get("/api/eos-members/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateEosMember() throws Exception {
//        // Initialize the database
//        eosMemberRepository.saveAndFlush(eosMember);
//        int databaseSizeBeforeUpdate = eosMemberRepository.findAll().size();
//
//        // Update the eosMember
//        EosMember updatedEosMember = eosMemberRepository.findOne(eosMember.getId());
//        updatedEosMember
//            .fromDate(UPDATED_FROM_DATE)
//            .toDate(UPDATED_TO_DATE);
//        EosMemberDTO eosMemberDTO = eosMemberMapper.toDto(updatedEosMember);
//
//        restEosMemberMockMvc.perform(put("/api/eos-members")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(eosMemberDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the EosMember in the database
//        List<EosMember> eosMemberList = eosMemberRepository.findAll();
//        assertThat(eosMemberList).hasSize(databaseSizeBeforeUpdate);
//        EosMember testEosMember = eosMemberList.get(eosMemberList.size() - 1);
//        assertThat(testEosMember.getFromDate()).isEqualTo(UPDATED_FROM_DATE);
//        assertThat(testEosMember.getToDate()).isEqualTo(UPDATED_TO_DATE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingEosMember() throws Exception {
//        int databaseSizeBeforeUpdate = eosMemberRepository.findAll().size();
//
//        // Create the EosMember
//        EosMemberDTO eosMemberDTO = eosMemberMapper.toDto(eosMember);
//
//        // If the entity doesn't have an ID, it will be created instead of just being updated
//        restEosMemberMockMvc.perform(put("/api/eos-members")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(eosMemberDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the EosMember in the database
//        List<EosMember> eosMemberList = eosMemberRepository.findAll();
//        assertThat(eosMemberList).hasSize(databaseSizeBeforeUpdate + 1);
//    }
//
//    @Test
//    @Transactional
//    public void deleteEosMember() throws Exception {
//        // Initialize the database
//        eosMemberRepository.saveAndFlush(eosMember);
//        int databaseSizeBeforeDelete = eosMemberRepository.findAll().size();
//
//        // Get the eosMember
//        restEosMemberMockMvc.perform(delete("/api/eos-members/{id}", eosMember.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isOk());
//
//        // Validate the database is empty
//        List<EosMember> eosMemberList = eosMemberRepository.findAll();
//        assertThat(eosMemberList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(EosMember.class);
//        EosMember eosMember1 = new EosMember();
//        eosMember1.setId(1L);
//        EosMember eosMember2 = new EosMember();
//        eosMember2.setId(eosMember1.getId());
//        assertThat(eosMember1).isEqualTo(eosMember2);
//        eosMember2.setId(2L);
//        assertThat(eosMember1).isNotEqualTo(eosMember2);
//        eosMember1.setId(null);
//        assertThat(eosMember1).isNotEqualTo(eosMember2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(EosMemberDTO.class);
//        EosMemberDTO eosMemberDTO1 = new EosMemberDTO();
//        eosMemberDTO1.setId(1L);
//        EosMemberDTO eosMemberDTO2 = new EosMemberDTO();
//        assertThat(eosMemberDTO1).isNotEqualTo(eosMemberDTO2);
//        eosMemberDTO2.setId(eosMemberDTO1.getId());
//        assertThat(eosMemberDTO1).isEqualTo(eosMemberDTO2);
//        eosMemberDTO2.setId(2L);
//        assertThat(eosMemberDTO1).isNotEqualTo(eosMemberDTO2);
//        eosMemberDTO1.setId(null);
//        assertThat(eosMemberDTO1).isNotEqualTo(eosMemberDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(eosMemberMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(eosMemberMapper.fromId(null)).isNull();
//    }
//}
