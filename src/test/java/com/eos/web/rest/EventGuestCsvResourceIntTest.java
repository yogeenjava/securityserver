package com.eos.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.eos.EosApp;
import com.eos.domain.EventGuestCsv;
import com.eos.repository.EventGuestCsvRepository;
import com.eos.service.EventGuestCsvService;
import com.eos.service.dto.EventGuestCsvDTO;
import com.eos.service.mapper.EventGuestCsvMapper;
import com.eos.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the EventGuestCsvResource REST controller.
 *
 * @see EventGuestCsvResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EosApp.class)
public class EventGuestCsvResourceIntTest {

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_DATA = "AAAAAAAAAA";
    private static final String UPDATED_FILE_DATA = "BBBBBBBBBB";

    @Autowired
    private EventGuestCsvRepository eventGuestCsvRepository;

    @Autowired
    private EventGuestCsvMapper eventGuestCsvMapper;

    @Autowired
    private EventGuestCsvService eventGuestCsvService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEventGuestCsvMockMvc;

    private EventGuestCsv eventGuestCsv;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EventGuestCsvResource eventGuestCsvResource = new EventGuestCsvResource(eventGuestCsvService);
        this.restEventGuestCsvMockMvc = MockMvcBuilders.standaloneSetup(eventGuestCsvResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventGuestCsv createEntity(EntityManager em) {
        EventGuestCsv eventGuestCsv = new EventGuestCsv()
            .fileName(DEFAULT_FILE_NAME)
            .fileData(DEFAULT_FILE_DATA);
        return eventGuestCsv;
    }

    @Before
    public void initTest() {
        eventGuestCsv = createEntity(em);
    }

    @Test
    @Transactional
    public void createEventGuestCsv() throws Exception {
        int databaseSizeBeforeCreate = eventGuestCsvRepository.findAll().size();

        // Create the EventGuestCsv
        EventGuestCsvDTO eventGuestCsvDTO = eventGuestCsvMapper.toDto(eventGuestCsv);
        restEventGuestCsvMockMvc.perform(post("/api/event-guest-csvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventGuestCsvDTO)))
            .andExpect(status().isCreated());

        // Validate the EventGuestCsv in the database
        List<EventGuestCsv> eventGuestCsvList = eventGuestCsvRepository.findAll();
        assertThat(eventGuestCsvList).hasSize(databaseSizeBeforeCreate + 1);
        EventGuestCsv testEventGuestCsv = eventGuestCsvList.get(eventGuestCsvList.size() - 1);
        assertThat(testEventGuestCsv.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testEventGuestCsv.getFileData()).isEqualTo(DEFAULT_FILE_DATA);
    }

    @Test
    @Transactional
    public void createEventGuestCsvWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = eventGuestCsvRepository.findAll().size();

        // Create the EventGuestCsv with an existing ID
        eventGuestCsv.setId(1L);
        EventGuestCsvDTO eventGuestCsvDTO = eventGuestCsvMapper.toDto(eventGuestCsv);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEventGuestCsvMockMvc.perform(post("/api/event-guest-csvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventGuestCsvDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EventGuestCsv in the database
        List<EventGuestCsv> eventGuestCsvList = eventGuestCsvRepository.findAll();
        assertThat(eventGuestCsvList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFileNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventGuestCsvRepository.findAll().size();
        // set the field null
        eventGuestCsv.setFileName(null);

        // Create the EventGuestCsv, which fails.
        EventGuestCsvDTO eventGuestCsvDTO = eventGuestCsvMapper.toDto(eventGuestCsv);

        restEventGuestCsvMockMvc.perform(post("/api/event-guest-csvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventGuestCsvDTO)))
            .andExpect(status().isBadRequest());

        List<EventGuestCsv> eventGuestCsvList = eventGuestCsvRepository.findAll();
        assertThat(eventGuestCsvList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFileDataIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventGuestCsvRepository.findAll().size();
        // set the field null
        eventGuestCsv.setFileData(null);

        // Create the EventGuestCsv, which fails.
        EventGuestCsvDTO eventGuestCsvDTO = eventGuestCsvMapper.toDto(eventGuestCsv);

        restEventGuestCsvMockMvc.perform(post("/api/event-guest-csvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventGuestCsvDTO)))
            .andExpect(status().isBadRequest());

        List<EventGuestCsv> eventGuestCsvList = eventGuestCsvRepository.findAll();
        assertThat(eventGuestCsvList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEventGuestCsvs() throws Exception {
        // Initialize the database
        eventGuestCsvRepository.saveAndFlush(eventGuestCsv);

        // Get all the eventGuestCsvList
        restEventGuestCsvMockMvc.perform(get("/api/event-guest-csvs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eventGuestCsv.getId().intValue())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
            .andExpect(jsonPath("$.[*].fileData").value(hasItem(DEFAULT_FILE_DATA.toString())));
    }

    @Test
    @Transactional
    public void getEventGuestCsv() throws Exception {
        // Initialize the database
        eventGuestCsvRepository.saveAndFlush(eventGuestCsv);

        // Get the eventGuestCsv
        restEventGuestCsvMockMvc.perform(get("/api/event-guest-csvs/{id}", eventGuestCsv.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(eventGuestCsv.getId().intValue()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()))
            .andExpect(jsonPath("$.fileData").value(DEFAULT_FILE_DATA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEventGuestCsv() throws Exception {
        // Get the eventGuestCsv
        restEventGuestCsvMockMvc.perform(get("/api/event-guest-csvs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEventGuestCsv() throws Exception {
        // Initialize the database
        eventGuestCsvRepository.saveAndFlush(eventGuestCsv);
        int databaseSizeBeforeUpdate = eventGuestCsvRepository.findAll().size();

        // Update the eventGuestCsv
        EventGuestCsv updatedEventGuestCsv = eventGuestCsvRepository.findOne(eventGuestCsv.getId());
        updatedEventGuestCsv
            .fileName(UPDATED_FILE_NAME)
            .fileData(UPDATED_FILE_DATA);
        EventGuestCsvDTO eventGuestCsvDTO = eventGuestCsvMapper.toDto(updatedEventGuestCsv);

        restEventGuestCsvMockMvc.perform(put("/api/event-guest-csvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventGuestCsvDTO)))
            .andExpect(status().isOk());

        // Validate the EventGuestCsv in the database
        List<EventGuestCsv> eventGuestCsvList = eventGuestCsvRepository.findAll();
        assertThat(eventGuestCsvList).hasSize(databaseSizeBeforeUpdate);
        EventGuestCsv testEventGuestCsv = eventGuestCsvList.get(eventGuestCsvList.size() - 1);
        assertThat(testEventGuestCsv.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testEventGuestCsv.getFileData()).isEqualTo(UPDATED_FILE_DATA);
    }

    @Test
    @Transactional
    public void updateNonExistingEventGuestCsv() throws Exception {
        int databaseSizeBeforeUpdate = eventGuestCsvRepository.findAll().size();

        // Create the EventGuestCsv
        EventGuestCsvDTO eventGuestCsvDTO = eventGuestCsvMapper.toDto(eventGuestCsv);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEventGuestCsvMockMvc.perform(put("/api/event-guest-csvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventGuestCsvDTO)))
            .andExpect(status().isCreated());

        // Validate the EventGuestCsv in the database
        List<EventGuestCsv> eventGuestCsvList = eventGuestCsvRepository.findAll();
        assertThat(eventGuestCsvList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEventGuestCsv() throws Exception {
        // Initialize the database
        eventGuestCsvRepository.saveAndFlush(eventGuestCsv);
        int databaseSizeBeforeDelete = eventGuestCsvRepository.findAll().size();

        // Get the eventGuestCsv
        restEventGuestCsvMockMvc.perform(delete("/api/event-guest-csvs/{id}", eventGuestCsv.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EventGuestCsv> eventGuestCsvList = eventGuestCsvRepository.findAll();
        assertThat(eventGuestCsvList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventGuestCsv.class);
        EventGuestCsv eventGuestCsv1 = new EventGuestCsv();
        eventGuestCsv1.setId(1L);
        EventGuestCsv eventGuestCsv2 = new EventGuestCsv();
        eventGuestCsv2.setId(eventGuestCsv1.getId());
        assertThat(eventGuestCsv1).isEqualTo(eventGuestCsv2);
        eventGuestCsv2.setId(2L);
        assertThat(eventGuestCsv1).isNotEqualTo(eventGuestCsv2);
        eventGuestCsv1.setId(null);
        assertThat(eventGuestCsv1).isNotEqualTo(eventGuestCsv2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventGuestCsvDTO.class);
        EventGuestCsvDTO eventGuestCsvDTO1 = new EventGuestCsvDTO();
        eventGuestCsvDTO1.setId(1L);
        EventGuestCsvDTO eventGuestCsvDTO2 = new EventGuestCsvDTO();
        assertThat(eventGuestCsvDTO1).isNotEqualTo(eventGuestCsvDTO2);
        eventGuestCsvDTO2.setId(eventGuestCsvDTO1.getId());
        assertThat(eventGuestCsvDTO1).isEqualTo(eventGuestCsvDTO2);
        eventGuestCsvDTO2.setId(2L);
        assertThat(eventGuestCsvDTO1).isNotEqualTo(eventGuestCsvDTO2);
        eventGuestCsvDTO1.setId(null);
        assertThat(eventGuestCsvDTO1).isNotEqualTo(eventGuestCsvDTO2);
    }

}
